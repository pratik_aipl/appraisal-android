package com.federal.appraisal.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.federal.appraisal.R;
import com.federal.appraisal.fragment.JewelleryFragment;
import com.federal.appraisal.ws.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import static com.federal.appraisal.adapter.EngagementRingsAdapter.handleSamplingAndRotationBitmap;

/**
 * Created by abc on 11/28/2017.
 */

public class JewelleryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    private List<String> imageList;
    JewelleryFragment jewelleryFragment;

    public JewelleryAdapter(Context mContext, List<String> imageList, JewelleryFragment jewelleryFragment) {
        this.mContext = mContext;
        this.imageList = imageList;
        this.jewelleryFragment = jewelleryFragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case 0:
                View v1 = inflater.inflate(R.layout.list_item_add_image, parent, false);
                viewHolder = new ViewHolderAddImage(v1);
                break;

            case 1:
                View v2 = inflater.inflate(R.layout.list_item_images, parent, false);
                viewHolder = new ViewHolderImages(v2);
                break;
        }

        return viewHolder;
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return (null != imageList ? imageList.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public class ViewHolderAddImage extends RecyclerView.ViewHolder {

        ViewHolderAddImage(View v) {
            super(v);
        }
    }

    public class ViewHolderImages extends RecyclerView.ViewHolder {

        ImageView ivImage, ivCancel;

        ViewHolderImages(View v) {
            super(v);
            ivImage = v.findViewById(R.id.ivImage);
            ivCancel = v.findViewById(R.id.ivCancel);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderAddImage viewHolderAddImage = (ViewHolderAddImage) holder;
                bindAddImageHolder(viewHolderAddImage, position);
                break;
            case 1:
                ViewHolderImages holderImages = (ViewHolderImages) holder;
                bindImagesHolder(holderImages, position);
                break;
        }
    }

    private void bindImagesHolder(ViewHolderImages holder, final int position) {
        File imgFile = new File(imageList.get(position));

        if (imgFile.exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            /*try {
                holder.ivImage.setImageBitmap(handleSamplingAndRotationBitmap(mContext, Uri.fromFile(imgFile)));
            } catch (IOException e) {
                e.printStackTrace();
            }*/

            try {
                Bitmap bitmap = handleSamplingAndRotationBitmap(mContext, Uri.fromFile(imgFile));

                try (FileOutputStream out = new FileOutputStream(imgFile)) {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
                    // PNG is a lossless format, the compression factor (100) is ignored
                } catch (IOException e) {
                    e.printStackTrace();
                }

                holder.ivImage.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        holder.ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageList.remove(position);
                notifyDataSetChanged();
            }
        });
    }


    private void bindAddImageHolder(ViewHolderAddImage holder, int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.checkPermission(mContext)) {
                    jewelleryFragment.selectImage();
                }
            }
        });
    }
}
