package com.federal.appraisal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.federal.appraisal.R;
import com.federal.appraisal.fragment.CommentsFragment;
import com.federal.appraisal.others.App;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by abc on 11/28/2017.
 */

public class CommentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    CommentsFragment commentsFragment;
    private List<HashMap<String, String>> commentList;

    public CommentsAdapter(Context mContext, List<HashMap<String, String>> commentList, CommentsFragment commentsFragment) {
        this.mContext = mContext;
        this.commentList = commentList;
        this.commentsFragment = commentsFragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case 0:
                View v1 = inflater.inflate(R.layout.list_item_comment_admin, parent, false);
                viewHolder = new ViewHolderAdmin(v1);
                break;

            case 1:
                View v2 = inflater.inflate(R.layout.list_item_comment_user, parent, false);
                viewHolder = new ViewHolderUser(v2);
                break;

            case 2:
                View v3 = inflater.inflate(R.layout.list_item_image_admin, parent, false);
                viewHolder = new ViewHolderImageAdmin(v3);
                break;

            case 3:
                View v4 = inflater.inflate(R.layout.list_item_image_user, parent, false);
                viewHolder = new ViewHolderImageUser(v4);
                break;
        }

        return viewHolder;
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return (null != commentList ? commentList.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        try {
            if (commentList.get(position).get("LoginID").equalsIgnoreCase(App.user.getUserID())) {
                if (commentList.get(position).get("CommentType").equalsIgnoreCase("T"))
                    return 1;
                else
                    return 3;
            } else {
                if (commentList.get(position).get("CommentType").equalsIgnoreCase("T"))
                    return 0;
                else
                    return 2;
            }
        }catch (NullPointerException e){
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderAdmin viewHolderAdmin = (ViewHolderAdmin) holder;
                bindAdminHolder(viewHolderAdmin, position);
                break;
            case 1:
                ViewHolderUser holderImages = (ViewHolderUser) holder;
                bindUserHolder(holderImages, position);
                break;
            case 2:
                ViewHolderImageAdmin viewHolderImageAdmin = (ViewHolderImageAdmin) holder;
                bindImageAdminHolder(viewHolderImageAdmin, position);
                break;
            case 3:
                ViewHolderImageUser viewHolderImageUser = (ViewHolderImageUser) holder;
                bindImageUserHolder(viewHolderImageUser, position);
                break;
        }
    }

    private void bindUserHolder(ViewHolderUser holder, final int position) {
        holder.tvComment.setText(commentList.get(position).get("Comment"));
        holder.tvUserName.setText(App.user.getName());
        holder.tvTime.setText(getFormattedTime(commentList.get(position).get("CommentedDate")));
    }

    private void bindAdminHolder(ViewHolderAdmin holder, int position) {
        holder.tvComment.setText(commentList.get(position).get("Comment"));
        holder.tvUserName.setText("Admin");
        holder.tvTime.setText(getFormattedTime(commentList.get(position).get("CommentedDate")));
    }

    private void bindImageUserHolder(ViewHolderImageUser holder, final int position) {
        holder.tvUserName.setText(App.user.getName());
        holder.tvTime.setText(getFormattedTime(commentList.get(position).get("CommentedDate")));

        System.out.println("imagePath==" + commentList.get(position).get("ImageURL"));
        Picasso.with(mContext).load(commentList.get(position).get("ImageURL")).into(holder.ivImage);
    }

    private void bindImageAdminHolder(ViewHolderImageAdmin holder, int position) {
        holder.tvUserName.setText("Admin");
        holder.tvTime.setText(getFormattedTime(commentList.get(position).get("CommentedDate")));
        System.out.println("imagePath==" + commentList.get(position).get("ImageURL"));
        Picasso.with(mContext).load(commentList.get(position).get("ImageURL")).into(holder.ivImage);
    }

    public String getFormattedTime(String date) {
        /// iphone  ///  2017-02-23T14:21:16.GMT
        /// android  /// 2018-04-12 12:53:53

        try {
            Log.i("DATE", "IN Android FORMAT");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            SimpleDateFormat newFormat = new SimpleDateFormat("hh:mm a");
            Date currentDate = new Date();

            currentDate = format.parse(date);
            return newFormat.format(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public class ViewHolderAdmin extends RecyclerView.ViewHolder {

        public TextView tvComment, tvUserName, tvTime;

        ViewHolderAdmin(View v) {
            super(v);

            tvComment = v.findViewById(R.id.tvComment);
            tvUserName = v.findViewById(R.id.tvUserName);
            tvTime = v.findViewById(R.id.tvTime);
        }
    }

    public class ViewHolderImageAdmin extends RecyclerView.ViewHolder {

        public TextView tvUserName, tvTime;
        public ImageView ivImage;

        ViewHolderImageAdmin(View v) {
            super(v);

            ivImage = v.findViewById(R.id.ivImage);
            tvUserName = v.findViewById(R.id.tvUserName);
            tvTime = v.findViewById(R.id.tvTime);
        }
    }

    public class ViewHolderUser extends RecyclerView.ViewHolder {

        public TextView tvComment, tvUserName, tvTime;

        ViewHolderUser(View v) {
            super(v);

            tvComment = v.findViewById(R.id.tvComment);
            tvUserName = v.findViewById(R.id.tvUserName);
            tvTime = v.findViewById(R.id.tvTime);
        }
    }

    public class ViewHolderImageUser extends RecyclerView.ViewHolder {

        public TextView tvUserName, tvTime;
        public ImageView ivImage;

        ViewHolderImageUser(View v) {
            super(v);

            ivImage = v.findViewById(R.id.ivImage);
            tvUserName = v.findViewById(R.id.tvUserName);
            tvTime = v.findViewById(R.id.tvTime);
        }
    }
}
