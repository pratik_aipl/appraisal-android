package com.federal.appraisal.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.federal.appraisal.R;
import com.federal.appraisal.activity.RemoveCard;
import com.federal.appraisal.activity.SavedCardpay;
import com.federal.appraisal.animation.MyBounceInterpolator;
import com.federal.appraisal.model.SavedCardListModel;

import java.util.List;


public class SavedCardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<SavedCardListModel> savedCardListModels;
    SavedCardpay savedCardpay;
    RemoveCard removeCard;

    public SavedCardAdapter(Context context, List<SavedCardListModel> savedCardListModels, SavedCardpay savedCardpay, RemoveCard removeCard) {
        this.context = context;
        this.savedCardListModels = savedCardListModels;
        this.savedCardpay = savedCardpay;
        this.removeCard = removeCard;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.savedcard_rowiteam, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holderIn, final int position) {

        final ViewHolder holder = (ViewHolder) holderIn;
        SavedCardListModel savedCardListModel = savedCardListModels.get(position);

        holder.tvCardName.setText(savedCardListModel.getCardHolderName());
        holder.tvCardNo.setText("xxxxxxxxxxxx" + savedCardListModel.getLastfourdigit());
        holder.tv_expdate.setText(savedCardListModel.getExp_month() + "/" + savedCardListModel.getExp_year());
        holder.tv_type.setText(savedCardListModel.getBrand());

        holder.btnPay.setOnClickListener(view -> {
            final Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);
            MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
            myAnim.setInterpolator(interpolator);
            holder.btnPay.startAnimation(myAnim);

            savedCardpay.onKeyPay(savedCardListModel.getCustomerID(), savedCardListModel.getUserStripreID());
            ;

            // CardDetailsFragment.tabDetails.getTabAt(1).setText("Pay").select();


        });
        holder.btnremove.setOnClickListener(view -> {
            final Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);
            MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
            myAnim.setInterpolator(interpolator);
            holder.btnremove.startAnimation(myAnim);

            removeCard.onKeyRemove(savedCardListModel.getUserStripreID(), position);

        });
    }

    @Override
    public int getItemCount() {

        return savedCardListModels.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvCardName, tvCardNo, tv_expdate, tv_type;
        Button btnPay, btnremove;

        public ViewHolder(View v) {
            super(v);

            tvCardName = v.findViewById(R.id.tv_holdername);
            tvCardNo = v.findViewById(R.id.tv_cardno);
            btnPay = v.findViewById(R.id.btn_pay);
            btnremove = v.findViewById(R.id.btn_remove);
            tv_expdate = v.findViewById(R.id.tv_expdate);
            tv_type = v.findViewById(R.id.tv_type);


        }
    }

}
