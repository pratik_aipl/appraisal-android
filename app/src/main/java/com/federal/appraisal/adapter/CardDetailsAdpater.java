package com.federal.appraisal.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class CardDetailsAdpater extends FragmentStatePagerAdapter {

    List<Fragment> fragments = new ArrayList<>();
    List<String> tabname = new ArrayList<>();

    public CardDetailsAdpater(FragmentManager fm) {
        super(fm);
    }


    public void addFragment(Fragment fragment, String name) {
        fragments.add(fragment);
        tabname.add(name);
    }

    /*  public void setFragments(Fragment fragment, int pos) {
          fragments.set(pos, fragment);
      }
  */
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabname.get(position);
    }
}
