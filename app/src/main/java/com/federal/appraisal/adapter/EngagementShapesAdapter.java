package com.federal.appraisal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.federal.appraisal.R;
import com.federal.appraisal.fragment.EngagementRingsFragment;
import com.federal.appraisal.others.PicassoTrustAll;
import com.federal.appraisal.ws.MyConstants;
import com.squareup.picasso.Callback;

import java.util.HashMap;
import java.util.List;

/**
 * Created by abc on 11/28/2017.
 */

public class EngagementShapesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    EngagementRingsFragment engagementRingsFragment;
    int mSelectedItem = -1;
    private List<HashMap<String, String>> shapeList;

    public EngagementShapesAdapter(Context mContext, List<HashMap<String, String>> shapeList, EngagementRingsFragment engagementRingsFragment) {
        this.mContext = mContext;
        this.shapeList = shapeList;
        this.engagementRingsFragment = engagementRingsFragment;
        mSelectedItem = 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View v2 = inflater.inflate(R.layout.list_item_engagement_shape, parent, false);
        viewHolder = new ViewHolderImages(v2);

        return viewHolder;
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return (null != shapeList ? shapeList.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolderImages holderImages = (ViewHolderImages) holder;
        bindImagesHolder(holderImages, position);
    }

    private void bindImagesHolder(final ViewHolderImages holder, final int position) {

      //  String imagePath = MyConstants.MAIN_URL + shapeList.get(position).get("ShapeImagePath") + shapeList.get(position).get("ShapeImageName");

        try {
            PicassoTrustAll.getInstance(mContext)
                    .load(shapeList.get(position).get("ShapeImagePath"))
                    .error(R.drawable.no_img)
                    .into(holder.ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            /*if (position == 0){
                                mSelectedItem = 0;
                                notifyDataSetChanged();
                            }*/
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.tvShapeName.setText(shapeList.get(position).get("ShapeName"));

        if (position == mSelectedItem) {
            // set your color
            holder.ivImage.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
        } else {
            holder.ivImage.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
        }

        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int) v.getTag();
                MyConstants.shapeId = shapeList.get(pos).get("ShapeID");
                MyConstants.shapePos = pos;

                mSelectedItem = pos;
                notifyDataSetChanged();

            }
        });

    }

    public class ViewHolderImages extends RecyclerView.ViewHolder {

        public ImageView ivImage;
        public TextView tvShapeName;

        ViewHolderImages(View v) {
            super(v);
            ivImage = v.findViewById(R.id.ivImage);
            tvShapeName = v.findViewById(R.id.tvShapeName);
        }
    }

}
