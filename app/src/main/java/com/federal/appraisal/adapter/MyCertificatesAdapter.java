package com.federal.appraisal.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.fragment.MyCertificatesFragment;
import com.federal.appraisal.others.Internet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.federal.appraisal.activity.HomeActivity.TAG_COMMENTS;

/**
 * Created by abc on 11/28/2017.
 */

public class MyCertificatesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    MyCertificatesFragment myCertificatesFragment;
    private List<HashMap<String, String>> certificateList;
    private List<HashMap<String, String>> searchList;
    private static final String TAG = "MyCertificatesAdapter";
    public MyCertificatesAdapter(Context mContext, List<HashMap<String, String>> certificateList, MyCertificatesFragment myCertificatesFragment) {
        this.mContext = mContext;
        this.certificateList = certificateList;
        this.myCertificatesFragment = myCertificatesFragment;
        this.searchList = new ArrayList<>();
        this.searchList.addAll(this.certificateList);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View v2 = inflater.inflate(R.layout.list_item_my_certificates, parent, false);
        viewHolder = new ViewHolderImages(v2);

        return viewHolder;
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return (null != certificateList ? certificateList.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolderImages holderImages = (ViewHolderImages) holder;
        bindImagesHolder(holderImages, position);
    }

    private void bindImagesHolder(ViewHolderImages holder, final int position) {

        if (position == 0) {
            if (getFormattedTime(certificateList.get(position).get("OrderDate")).equalsIgnoreCase(new SimpleDateFormat("MMM-dd-yyyy").format(new Date()))) {
                holder.tvDate.setText("TODAY");
            } else {
                holder.tvDate.setText(getFormattedTime(certificateList.get(position).get("OrderDate")));
            }
        }

        if (position > 0) {
            if (getFormattedTime(certificateList.get(position).get("OrderDate")).equalsIgnoreCase(getFormattedTime(certificateList.get(position - 1).get("OrderDate")))) {
                holder.llDate.setVisibility(View.GONE);
            } else {
                holder.llDate.setVisibility(View.VISIBLE);
                // 2018-04-12 12:53:53 server format
                holder.tvDate.setText(getFormattedTime(certificateList.get(position).get("OrderDate")));
            }

            if (position < certificateList.size() - 1) {
                if (getFormattedTime(certificateList.get(position).get("OrderDate")).equalsIgnoreCase(getFormattedTime(certificateList.get(position - 1).get("OrderDate")))) {
                    if (!getFormattedTime(certificateList.get(position).get("OrderDate")).equalsIgnoreCase(getFormattedTime(certificateList.get(position + 1).get("OrderDate")))) {
                        holder.line.setVisibility(View.GONE);
                    } else {
                        holder.line.setVisibility(View.VISIBLE);
                    }
                } else {
                    holder.line.setVisibility(View.VISIBLE);
                }
            }
        }

        holder.tvName.setText(certificateList.get(position).get("CustomerName"));
        holder.tvNumber.setText(certificateList.get(position).get("CertificateNo"));
        holder.tvTracking.setText(certificateList.get(position).get("TrackingNO"));

        /*if (!certificateList.get(position).get("OrderStatus").equalsIgnoreCase("Pending") && !TextUtils.isEmpty(certificateList.get(position).get("CertificateLink"))) {
            holder.ivStatus.setImageResource(R.drawable.right_green);
        } else {
            holder.ivStatus.setImageResource(R.drawable.close_red);
        }*/

        holder.ivEdit.setTag(position);
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();

                Bundle bundle = new Bundle();
                bundle.putString("order_id", certificateList.get(pos).get("OrderId"));

                ((HomeActivity) mContext).setIndex(13, TAG_COMMENTS);
                ((HomeActivity) mContext).replaceFragment(bundle);
            }
        });

        if (!certificateList.get(position).get("OrderStatus").equalsIgnoreCase("Pending") && !TextUtils.isEmpty(certificateList.get(position).get("CertificateLink"))) {
            holder.ivDownload.setTag(position);
            holder.ivStatus.setImageResource(R.drawable.right_green);
            holder.ivDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();
                   /* int pos = (int) v.getTag();
                    if (!Internet.isAvailable(mContext)) {
                        Internet.showAlertDialog(mContext, "Error!", "Required Internet Connection", false);
                    } else {
                        myCertificatesFragment.downloadPDF(pos, certificateList.get(pos).get("CertificateLink"), certificateList.get(pos).get("CardLink"));
                    }*/
                    Log.d(TAG, "onClick: "+certificateList.get(pos).get("CertificateLink"));
                    myCertificatesFragment.openDownloadDialog(pos, certificateList.get(pos).get("CertificateLink"), certificateList.get(pos).get("CardLink"));

                }
            });
        } else {
            holder.ivDownload.setAlpha(50);
            holder.ivStatus.setImageResource(R.drawable.close_red);
        }
    }

    public void filter ( final String text){
        new Thread(new Runnable() {
            @Override
            public void run() {
                certificateList.clear();
                if (TextUtils.isEmpty(text)) {
                    certificateList.addAll(searchList);
                } else {
                    for (HashMap<String, String> item : searchList) {

                        if (item.get("CustomerName").toLowerCase(Locale.getDefault()).contains(text)) {
                            certificateList.add(item);
                        }
                    }
                }

                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                    }
                });

            }
        }).start();

    }

    public String getFormattedTime(String date) {
        /// iphone  ///  2017-02-23T14:21:16.GMT
        /// android  /// 2018-04-12 12:53:53

        try {
            Log.i("DATE", "IN Android FORMAT");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            SimpleDateFormat newFormat = new SimpleDateFormat("MMM-dd-yyyy");
            Date currentDate = new Date();

            currentDate = format.parse(date);
            return newFormat.format(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public class ViewHolderImages extends RecyclerView.ViewHolder {

        public LinearLayout llDate;
        public TextView tvDate, tvName, tvNumber, tvTracking;
        public View line;
        public ImageView ivStatus, ivEdit, ivDownload;

        ViewHolderImages(View v) {
            super(v);
            llDate = v.findViewById(R.id.llDate);
            tvDate = v.findViewById(R.id.tvDate);
            line = v.findViewById(R.id.line);
            tvName = v.findViewById(R.id.tvName);
            tvNumber = v.findViewById(R.id.tvNumber);
            tvTracking = v.findViewById(R.id.tvTracking);
            ivStatus = v.findViewById(R.id.ivStatus);
            ivEdit = v.findViewById(R.id.ivEdit);
            ivDownload = v.findViewById(R.id.ivDownload);
        }
    }
}
