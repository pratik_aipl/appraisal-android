package com.federal.appraisal.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public class AllMaterial implements Serializable {

    List<HashMap<String, String>> ItemType, Shape, Clarity, Color, Wind, MaterialType, MaterialJewelaryType,GemMaterialWatchType, JewelaryType, MaterialWatchType;

    public List<HashMap<String, String>> getItemType() {
        return ItemType;
    }

    public void setItemType(List<HashMap<String, String>> itemType) {
        ItemType = itemType;
    }

    public List<HashMap<String, String>> getShape() {
        return Shape;
    }

    public void setShape(List<HashMap<String, String>> shape) {
        Shape = shape;
    }

    public List<HashMap<String, String>> getClarity() {
        return Clarity;
    }

    public void setClarity(List<HashMap<String, String>> clarity) {
        Clarity = clarity;
    }

    public List<HashMap<String, String>> getColor() {
        return Color;
    }

    public void setColor(List<HashMap<String, String>> color) {
        Color = color;
    }

    public List<HashMap<String, String>> getWind() {
        return Wind;
    }

    public void setWind(List<HashMap<String, String>> wind) {
        Wind = wind;
    }

    public List<HashMap<String, String>> getMaterialType() {
        return MaterialType;
    }

    public void setMaterialType(List<HashMap<String, String>> materialType) {
        MaterialType = materialType;
    }

    public List<HashMap<String, String>> getJewelaryType() {
        return JewelaryType;
    }

    public void setJewelaryType(List<HashMap<String, String>> jewelaryType) {
        JewelaryType = jewelaryType;
    }

    public List<HashMap<String, String>> getMaterialJewelaryType() {
        return MaterialJewelaryType;
    }

    public void setMaterialJewelaryType(List<HashMap<String, String>> materialJewelaryType) {
        MaterialJewelaryType = materialJewelaryType;
    }

    public List<HashMap<String, String>> getGemMaterialWatchType() {
        return GemMaterialWatchType;
    }

    public void setGemMaterialWatchType(List<HashMap<String, String>> gemMaterialWatchType) {
        GemMaterialWatchType = gemMaterialWatchType;
    }

    public List<HashMap<String, String>> getMaterialWatchType() {
        return MaterialWatchType;
    }

    public void setMaterialWatchType(List<HashMap<String, String>> materialWatchType) {
        MaterialWatchType = materialWatchType;
    }
}
