package com.federal.appraisal.model;

public class SavedCardListModel {

    String UserStripreID;
    String CustomerID;
    String CustomerCardID;
    String CardHolderName;
    String brand;
    String is_default;
    String exp_month;
    String exp_year;
    String lastfourdigit;

    public String getUserStripreID() {
        return UserStripreID;
    }

    public void setUserStripreID(String userStripreID) {
        UserStripreID = userStripreID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getCustomerCardID() {
        return CustomerCardID;
    }

    public void setCustomerCardID(String customerCardID) {
        CustomerCardID = customerCardID;
    }

    public String getCardHolderName() {
        return CardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        CardHolderName = cardHolderName;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getIs_default() {
        return is_default;
    }

    public void setIs_default(String is_default) {
        this.is_default = is_default;
    }

    public String getExp_month() {
        return exp_month;
    }

    public void setExp_month(String exp_month) {
        this.exp_month = exp_month;
    }

    public String getExp_year() {
        return exp_year;
    }

    public void setExp_year(String exp_year) {
        this.exp_year = exp_year;
    }

    public String getLastfourdigit() {
        return lastfourdigit;
    }

    public void setLastfourdigit(String lastfourdigit) {
        this.lastfourdigit = lastfourdigit;
    }
}
