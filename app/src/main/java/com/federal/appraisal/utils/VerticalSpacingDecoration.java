package com.federal.appraisal.utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class VerticalSpacingDecoration extends RecyclerView.ItemDecoration {

    private int spacing;

    private int right = (int) 2.5;
    private int left = (int) 2.5;


    public VerticalSpacingDecoration(int spacing) {
        this.spacing = spacing;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        outRect.top = spacing/2;
        outRect.right = spacing/2;
        outRect.left = spacing/2;
        outRect.bottom = spacing/2;

    }
}