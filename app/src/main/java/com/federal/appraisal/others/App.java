package com.federal.appraisal.others;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.federal.appraisal.BuildConfig;
import com.federal.appraisal.one_signal.NotificationOpenedHandler;
import com.federal.appraisal.one_signal.NotificationReceivedHandler;
import com.federal.appraisal.ws.MyConstants;
import com.onesignal.OneSignal;

import java.lang.reflect.Method;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Sagar Sojitra on 5/16/2016.
 */
public class App extends MultiDexApplication {

    public static App instance;
    public static User user = null;
    public static String HEALTH_TOPIC = "health_tips";
    public static String certificateNo;
    public SharedPreferences sharedPref;
    public String recentChatId = "";
    public String messageBadgeVal = "";
    /*public ArrayList<Discount> discountArray = new ArrayList<>();
    public ArrayList<ChatMessage> chatArray = new ArrayList<>();
    public ArrayList<FireMessage> fireChatArray = new ArrayList<>();*/
    public String countryName = "", stateName = "", timeZone = "";
    public App() {
        setInstance(this);
    }

    public static App getInstance() {
        return instance;
    }

    public static void setInstance(App instance) {
        App.instance = instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationReceivedHandler(new NotificationReceivedHandler())
                .setNotificationOpenedHandler(new NotificationOpenedHandler(getApplicationContext()))
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        OneSignal.idsAvailable( new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null)
                    MyConstants.ONESIGNAL_ID = userId;
                Log.d("debug", "User:" + userId);
            /*    if (registrationId != null)
                    MyConstants.DEVICE_ID = registrationId;*/
                Log.d("debug", "registrationId:" + registrationId);
            }
        });

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure();
        }

        try {
            instance = this;
            user = new User();
            sharedPref = getSharedPreferences(App.class.getSimpleName(), 0);
            MultiDex.install(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(!BuildConfig.DEBUG)
            Fabric.with(this, new Crashlytics());

    }

    @Override
    public void onTerminate() {

        super.onTerminate();
    }
}
