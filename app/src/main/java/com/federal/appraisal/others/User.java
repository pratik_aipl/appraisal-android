package com.federal.appraisal.others;

/**
 * Created by Badruduja on 3/21/16.
 */
public class User {

    public String userEmail;
    public String current_zone_date_time;
    public String profileUrl;
    public String name;
    public String sinch_id = "";
    public String free_consult = "";
    public String userID = "";
    public String user_DeviceID = "";
    public String user_Type;
    public String messageBadgeVal = "";
    public String loginType = "";
    public String firstName = "";
    public String lastName = "";
    public String phone = "";
    public String token = "";

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getCurrent_zone_date_time() {
        return current_zone_date_time;
    }

    public void setCurrent_zone_date_time(String current_zone_date_time) {
        this.current_zone_date_time = current_zone_date_time;
    }

    public String getSinch_id() {
        return sinch_id;
    }

    public void setSinch_id(String sinch_id) {
        this.sinch_id = sinch_id;
    }

    public String getFree_consult() {
        return free_consult;
    }

    public void setFree_consult(String free_consult) {
        this.free_consult = free_consult;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getMessageBadgeVal() {
        return messageBadgeVal;
    }

    public void setMessageBadgeVal(String messageBadgeVal) {
        this.messageBadgeVal = messageBadgeVal;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }


    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUser_DeviceID() {
        return user_DeviceID;
    }

    public void setUser_DeviceID(String user_DeviceID) {
        this.user_DeviceID = user_DeviceID;
    }

    public String getUser_Type() {
        return user_Type;
    }

    public void setUser_Type(String user_Type) {
        this.user_Type = user_Type;
    }

}
