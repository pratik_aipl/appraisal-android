package com.federal.appraisal.fragment;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.federal.appraisal.BuildConfig;
import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.animation.MyBounceInterpolator;
import com.federal.appraisal.others.App;
import com.federal.appraisal.others.Internet;
import com.federal.appraisal.ws.AsyncTaskListner;
import com.federal.appraisal.ws.CallRequest;
import com.federal.appraisal.ws.Constant;
import com.federal.appraisal.ws.MyConstants;
import com.federal.appraisal.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.federal.appraisal.activity.HomeActivity.TAG_FILE_UPLOAD;
import static com.federal.appraisal.fragment.CommentsFragment.orderId;

/**
 * A simple {@link Fragment} subclass.
 */
public class FileUploadFragment extends Fragment implements AsyncTaskListner {

    ImageView ivImage;
    float touchY;
    float touchX;
    TextView tvReset;
    Button btnSubmit;
    String TAG = "IMAGE::", imagePathTop = "", imagePathSide = "";
    Bitmap bitmapStoneTop, bitmapStoneSide;
    List<HashMap<String, String>> commentList = new ArrayList<>();

    public FileUploadFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_file_upload, container, false);

        try {
            ((HomeActivity) getActivity()).setFragmentTitle(14);
            ((HomeActivity) getActivity()).setIndex(14, TAG_FILE_UPLOAD);

            ivImage = view.findViewById(R.id.ivImage);
            tvReset = view.findViewById(R.id.tvReset);
            btnSubmit = view.findViewById(R.id.btnSubmit);

            final String imagePathTop1 = MyConstants.commentImage;

            System.out.println("comment image:::" + imagePathTop1);

            File f = new File(imagePathTop1);

            try {
            /*PicassoTrustAll.getInstance(getActivity())
                    .load(f)
                    .placeholder(R.drawable.no_img)
                    .error(R.drawable.no_img)
                    .into(ivImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            bitmapStoneTop = ((BitmapDrawable) ivImage.getDrawable()).getBitmap().copy(Bitmap.Config.ARGB_8888, true);
                        }

                        @Override
                        public void onError() {
                        }
                    });*/
                Bitmap selectedBitmap = BitmapFactory.decodeFile(imagePathTop1);
                ivImage.setImageBitmap(selectedBitmap);
                bitmapStoneTop = ((BitmapDrawable) ivImage.getDrawable()).getBitmap().copy(Bitmap.Config.ARGB_8888, true);
            } catch (Exception e) {
                e.printStackTrace();
            }


            ivImage.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    tvReset.setVisibility(View.VISIBLE);

                    int touchX = (int) (event.getX());
                    int touchY = (int) (event.getY());

                    if (bitmapStoneTop != null)
                        drawOnProjectedBitMap(ivImage, bitmapStoneTop, touchX, touchY);

                    return true;
                }
            });


            tvReset.setOnClickListener(v -> {
                final String imagePathTop11 = MyConstants.commentImage;
                Bitmap selectedBitmap = BitmapFactory.decodeFile(imagePathTop11);
                ivImage.setImageBitmap(selectedBitmap);
                bitmapStoneTop = ((BitmapDrawable) ivImage.getDrawable()).getBitmap().copy(Bitmap.Config.ARGB_8888, true);

                tvReset.setVisibility(View.GONE);
            });

            btnSubmit.setEnabled(true);
            btnSubmit.setOnClickListener(v -> {
                btnSubmit.setClickable(false);

                Bitmap bitmapTop = ((BitmapDrawable) ivImage.getDrawable()).getBitmap();
                storeImage(bitmapTop, "Top");
                if (!TextUtils.isEmpty(MyConstants.commentUploadImage)) {
                    addImageComment(MyConstants.commentUploadImage);
                }
                //MyConstants.orderData.put("issueImageTop", imagePathTop);
                Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                btnSubmit.startAnimation(myAnim);

            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        setHasOptionsMenu(true);

        return view;
    }

    private void addImageComment(String image) {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
            btnSubmit.setClickable(true);
        } else {

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "comment");
            map.put("header", "");
            map.put("token", App.user.getToken());
            map.put("user_id", App.user.getUserID());
            map.put("OrderID", orderId);
            map.put("Comment", "");
            map.put("image", image);
            map.put("CommentType", "I");

            new CallRequest(FileUploadFragment.this).addComment(map);
        }
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    private void storeImage(Bitmap image, String type) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d(TAG,
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.JPEG, 90, fos);
            fos.close();

            if (type.equalsIgnoreCase("Top")) {
                imagePathTop = pictureFile.getAbsolutePath();
                MyConstants.commentUploadImage = imagePathTop;
            }
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }

    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath() + "/Federal_Appraisal/Images");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmssSSS").format(new Date());
        File mediaFile;
        String mImageName = "Comment_" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }


    private void drawOnProjectedBitMap(ImageView iv, Bitmap bm, int x, int y) {
        if (x < 0 || y < 0 || x > iv.getWidth() || y > iv.getHeight()) {
            //outside ImageView
            return;
        } else {
            int projectedX = (int) ((double) x * ((double) bm.getWidth() / (double) iv.getWidth()));
            int projectedY = (int) ((double) y * ((double) bm.getHeight() / (double) iv.getHeight()));

            Paint paint = new Paint();
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(Color.GREEN);
            paint.setStrokeWidth(3);
            Canvas canvas = new Canvas(bm);
            canvas.drawCircle(projectedX, projectedY, 20, paint);
            iv.setImageBitmap(bm);
            iv.invalidate();


            System.out.println(x + ":" + y + "/" + iv.getWidth() + " : " + iv.getHeight() + "\n" +
                    projectedX + " : " + projectedY + "/" + bm.getWidth() + " : " + bm.getHeight());


        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case addComment:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {
                                Utils.showToast(mainObj.getString("message"), getActivity());

                                JSONObject object = mainObj.getJSONObject("data");

                                HashMap<String, String> map = new HashMap<>();
                                map.put("BlogCommentID", object.getString("BlogCommentID"));
                                map.put("Comment", object.getString("Comment"));
                                map.put("CommentedDate", object.getString("CommentedDate"));
                                map.put("LoginID", object.getString("LoginID"));

                                commentList.add(map);


                                MyConstants.commentImage = "";

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Do something after 100ms
                                        getFragmentManager().popBackStackImmediate();
                                    }
                                }, 1000);
                                /*commentsAdapter = new CommentsAdapter(getActivity(), commentList, CommentsFragment.this);
                                rvComments.setHasFixedSize(true);
                                rvComments.addItemDecoration(new VerticalSpacingDecoration(20));
                                rvComments.setItemViewCacheSize(20);
                                rvComments.setDrawingCacheEnabled(true);
                                rvComments.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                                rvComments.setAdapter(commentsAdapter);*/

                            } else {
                                Utils.showToast(mainObj.getString("message"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.showToast("Try Again", getActivity());
                            btnSubmit.setEnabled(true);
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            btnSubmit.setEnabled(true);
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
