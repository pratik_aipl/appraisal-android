package com.federal.appraisal.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.animation.MyBounceInterpolator;
import com.federal.appraisal.ws.MyConstants;

import static com.federal.appraisal.activity.HomeActivity.TAG_ORDER_DETAIL;
import static com.federal.appraisal.activity.HomeActivity.TAG_PAYMENT;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderDetailFragment extends Fragment {

    Button btnNext;
    TextView tvCustomerName, tvOrderFor, tvJewelleryType, tvMetalType, tvCaratWeight, tvSettingWeight, tvEstimatedValue,
            tvComments, tvPictures, tvDail, tvDiamondQuality, tvDiamondClarity, mQt,lblCaratWeight;
    LinearLayout llSettingWeight, llCustomerName, llOrderFor, llJewelleryType, llMetalType, llCaratWeight, llEstimatedValue,
            llComments, llDail, llPictures, llOthers, llQuality, llDiamondClarity;

    public OrderDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_detail, container, false);

        try {
            ((HomeActivity) getActivity()).setFragmentTitle(4);
            ((HomeActivity) getActivity()).setIndex(4, TAG_ORDER_DETAIL);

            btnNext = view.findViewById(R.id.btnNext);
            tvCustomerName = view.findViewById(R.id.tvCustomerName);
            tvOrderFor = view.findViewById(R.id.tvOrderFor);
            tvJewelleryType = view.findViewById(R.id.tvJewelleryType);
            mQt = view.findViewById(R.id.mQt);
            tvMetalType = view.findViewById(R.id.tvMetalType);
            tvCaratWeight = view.findViewById(R.id.tvCaratWeight);
            tvEstimatedValue = view.findViewById(R.id.tvEstimatedValue);
            tvSettingWeight = view.findViewById(R.id.tvSettingWeight);
            tvComments = view.findViewById(R.id.tvComments);
            tvDail = view.findViewById(R.id.tvDail);
            tvPictures = view.findViewById(R.id.tvPictures);
            llCustomerName = view.findViewById(R.id.llCustomerName);
            llSettingWeight = view.findViewById(R.id.llSettingWeight);
            llOrderFor = view.findViewById(R.id.llOrderFor);
            llJewelleryType = view.findViewById(R.id.llJewelleryType);
            llMetalType = view.findViewById(R.id.llMetalType);
            llCaratWeight = view.findViewById(R.id.llCaratWeight);
            llEstimatedValue = view.findViewById(R.id.llEstimatedValue);
            llComments = view.findViewById(R.id.llComments);
            llPictures = view.findViewById(R.id.llPictures);
            llOthers = view.findViewById(R.id.llOthers);
            llQuality = view.findViewById(R.id.llQuality);
            llDiamondClarity = view.findViewById(R.id.llDiamondClarity);
            llDail = view.findViewById(R.id.llDail);
            lblCaratWeight = view.findViewById(R.id.lblCaratWeight);
            tvDiamondQuality = view.findViewById(R.id.tvDiamondQuality);

            tvDiamondClarity = view.findViewById(R.id.tvDiamondClarity);
            llQuality.setVisibility(View.GONE);
            llDiamondClarity.setVisibility(View.GONE);
            lblCaratWeight.setText("Carat weight");
            if (MyConstants.orderData.get("ItemId").equalsIgnoreCase("4")) {

                tvCustomerName.setText(MyConstants.orderData.get("customerName"));
                llDail.setVisibility(View.GONE);
                tvDail.setText(MyConstants.orderData.get("dial"));
                tvOrderFor.setText("Weblink");
                if (!TextUtils.isEmpty(MyConstants.orderData.get("comment"))) {
                    tvComments.setText(MyConstants.orderData.get("comment"));
                } else {
                    tvComments.setText("N/A");
                }

                llJewelleryType.setVisibility(View.GONE);
                llMetalType.setVisibility(View.GONE);
                llCaratWeight.setVisibility(View.GONE);
                llEstimatedValue.setVisibility(View.GONE);
                llPictures.setVisibility(View.GONE);
            } else if (MyConstants.orderData.get("ItemId").equalsIgnoreCase("3")) {

                llQuality.setVisibility(View.VISIBLE);
                llDiamondClarity.setVisibility(View.VISIBLE);
                mQt.setText("Diamond Color");


                if (MyConstants.orderData.get("jew_color_check").equalsIgnoreCase("1")) {
                    tvDiamondQuality.setText("None");
                } else {
                    if (TextUtils.isEmpty(MyConstants.orderData.get("color"))) {
                    tvDiamondQuality.setText(getString(R.string.str_formate, checkData(MyConstants.orderData.get("jew_color_start_name")), checkData(MyConstants.orderData.get("jew_color_end_name"))));
                    } else {
                        tvDiamondQuality.setText(MyConstants.orderData.get("color"));
                    }
                }


                if (MyConstants.orderData.get("jew_clearity_check").equalsIgnoreCase("1")) {
                    tvDiamondClarity.setText("None");
                } else {
                    tvDiamondClarity.setText(getString(R.string.str_formate, MyConstants.orderData.get("jew_clearity_start_name"), MyConstants.orderData.get("jew_clearity_end_name")));
                }


                tvCustomerName.setText(MyConstants.orderData.get("customerName"));
                tvDail.setText(MyConstants.orderData.get("dial"));
                llDail.setVisibility(View.GONE);
                tvOrderFor.setText("Jewellery");
                if (!TextUtils.isEmpty(MyConstants.orderData.get("comment"))) {
                    tvComments.setText(MyConstants.orderData.get("comment"));
                } else {
                    tvComments.setText("N/A");
                }
                tvMetalType.setText(MyConstants.orderData.get("metalType"));
                tvJewelleryType.setText(MyConstants.orderData.get("jewelleryType"));
                lblCaratWeight.setText("Total Carat Weight");
                if (!TextUtils.isEmpty(MyConstants.orderData.get("caratWeight"))) {
                    tvCaratWeight.setText(MyConstants.orderData.get("caratWeight"));
                } else {
                    tvCaratWeight.setText("N/A");
                }
                if (!TextUtils.isEmpty(MyConstants.orderData.get("estimatedValue"))) {
                    tvEstimatedValue.setText("$" + MyConstants.orderData.get("estimatedValue"));
                } else {
                    tvEstimatedValue.setText("N/A");
                }
                tvPictures.setText(MyConstants.orderData.get("imagesCount") + " Files");

                if (MyConstants.orderData.get("gemMaterial").equalsIgnoreCase("Other")) {
                    addLayout("Gem Material", MyConstants.orderData.get("materialType"));
                } else {
                    addLayout("Gem Material", MyConstants.orderData.get("gemMaterial"));
                }
                addLayout("Weight of Item", MyConstants.orderData.get("weight_of_item"));

                llJewelleryType.setVisibility(View.VISIBLE);
                llMetalType.setVisibility(View.VISIBLE);
                llCaratWeight.setVisibility(View.VISIBLE);
                llEstimatedValue.setVisibility(View.VISIBLE);
                llPictures.setVisibility(View.VISIBLE);
            } else if (MyConstants.orderData.get("ItemId").equalsIgnoreCase("2")) {
                llDail.setVisibility(View.VISIBLE);
                tvCustomerName.setText(MyConstants.orderData.get("customerName"));
                tvDail.setText(MyConstants.orderData.get("dial"));
                tvOrderFor.setText("Watches");
                if (!TextUtils.isEmpty(MyConstants.orderData.get("comment"))) {
                    tvComments.setText(MyConstants.orderData.get("comment"));
                } else {
                    tvComments.setText("N/A");
                }

                if (!TextUtils.isEmpty(MyConstants.orderData.get("estimatedValue"))) {
                    tvEstimatedValue.setText("$" + MyConstants.orderData.get("estimatedValue"));
                } else {
                    tvEstimatedValue.setText("N/A");
                }
                tvPictures.setText(MyConstants.orderData.get("imagesCount") + " Files");
                tvMetalType.setText(MyConstants.orderData.get("metalType"));

                addLayout("Brand Name", MyConstants.orderData.get("brandName"));
                addLayout("Serial Number", MyConstants.orderData.get("serialNumber"));

                if (!TextUtils.isEmpty(MyConstants.orderData.get("modelName"))) {
                    addLayout("Model Name", MyConstants.orderData.get("modelName"));
                }
                if (!TextUtils.isEmpty(MyConstants.orderData.get("caseSize"))) {
                    addLayout("Case Size", MyConstants.orderData.get("caseSize"));
                }
                if (!TextUtils.isEmpty(MyConstants.orderData.get("wind"))) {
                    addLayout("Wind", MyConstants.orderData.get("wind"));
                }

                if (!TextUtils.isEmpty(MyConstants.orderData.get("materialType"))) {
                    if (MyConstants.orderData.get("materialType").equalsIgnoreCase("Other")) {
                        addLayout("Material Type", MyConstants.orderData.get("materialTypeOther"));
                    } else {
                        addLayout("Material Type", MyConstants.orderData.get("materialType"));
                    }
                }

//                if (MyConstants.orderData.get("gemMaterial").equalsIgnoreCase("Other")) {
//                    addLayout("Gem Material", MyConstants.orderData.get("materialType"));
//                } else {
                    addLayout("Gem Material", MyConstants.orderData.get("gemMaterial"));
//                }

                llQuality.setVisibility(View.VISIBLE);
                mQt.setText("Diamond Color");
                llDiamondClarity.setVisibility(View.VISIBLE);

                if (MyConstants.orderData.get("gemMaterial").equalsIgnoreCase("none")) {
                    llQuality.setVisibility(View.GONE);
                    llCaratWeight.setVisibility(View.GONE);
                    llDiamondClarity.setVisibility(View.GONE);
                } else {
                    llQuality.setVisibility(View.VISIBLE);
                    llDiamondClarity.setVisibility(View.VISIBLE);
                    llCaratWeight.setVisibility(View.VISIBLE);
                    lblCaratWeight.setText("Diamond weight");
                    if (!TextUtils.isEmpty(MyConstants.orderData.get("caratWeight"))) {
                        tvCaratWeight.setText(MyConstants.orderData.get("caratWeight"));
                    } else {
                        tvCaratWeight.setText("N/A");
                    }
                }
                if (TextUtils.isEmpty(MyConstants.orderData.get("color"))) {
                    tvDiamondQuality.setText(getString(R.string.str_formate, checkData(MyConstants.orderData.get("color_start_name")), checkData(MyConstants.orderData.get("color_end_name"))));
                } else {
                    tvDiamondQuality.setText(MyConstants.orderData.get("color"));
                }

                tvDiamondClarity.setText(getString(R.string.str_formate, MyConstants.orderData.get("clearity_start_name"), MyConstants.orderData.get("clearity_end_name")));

                llJewelleryType.setVisibility(View.GONE);
                llSettingWeight.setVisibility(View.GONE);
                llMetalType.setVisibility(View.GONE);
                llEstimatedValue.setVisibility(View.VISIBLE);
                llPictures.setVisibility(View.VISIBLE);
            } else if (MyConstants.orderData.get("ItemId").equalsIgnoreCase("1")) {

                tvCustomerName.setText(MyConstants.orderData.get("customerName"));
                tvDail.setText(MyConstants.orderData.get("dial"));
                llDail.setVisibility(View.GONE);
                if (MyConstants.orderData.get("isRing").equalsIgnoreCase("1")) {
                    tvOrderFor.setText("Engagement Rings");
                    tvMetalType.setText(MyConstants.orderData.get("metalType"));
                    llMetalType.setVisibility(View.VISIBLE);
                } else {
                    tvOrderFor.setText("Loose Stones");
                    llMetalType.setVisibility(View.GONE);
                }
                if (!TextUtils.isEmpty(MyConstants.orderData.get("comment"))) {
                    tvComments.setText(MyConstants.orderData.get("comment"));
                } else {
                    tvComments.setText("N/A");
                }
                lblCaratWeight.setText("Center Stone carat weight");
                if (!TextUtils.isEmpty(MyConstants.orderData.get("caratWeight"))) {
                    tvCaratWeight.setText(MyConstants.orderData.get("caratWeight"));
                } else {
                    tvCaratWeight.setText("N/A");
                }
                if (!TextUtils.isEmpty(MyConstants.orderData.get("estimatedValue"))) {
                    tvEstimatedValue.setText("$" + MyConstants.orderData.get("estimatedValue"));
                } else {
                    tvEstimatedValue.setText("N/A");
                }
                if (!TextUtils.isEmpty(MyConstants.orderData.get("settingWeight"))) {
                    tvSettingWeight.setText(MyConstants.orderData.get("settingWeight"));
                } else {
                    llSettingWeight.setVisibility(View.GONE);
                }
                tvPictures.setText(MyConstants.orderData.get("imagesCount") + " Files");


                addLayout("Shape", MyConstants.orderData.get("Shape"));
//                addLayout("Clarity Grade", MyConstants.orderData.get("Clarity"));
//                addLayout("Color", MyConstants.orderData.get("Color"));

                if (MyConstants.orderData.get("MaterialType").equalsIgnoreCase("none")) {
                    llQuality.setVisibility(View.GONE);
                    llDiamondClarity.setVisibility(View.GONE);
                } else {
                    mQt.setText("Diamond Color");
                    llQuality.setVisibility(View.VISIBLE);
                    llDiamondClarity.setVisibility(View.VISIBLE);
                }

                if (TextUtils.isEmpty(MyConstants.orderData.get("color"))) {
                    tvDiamondQuality.setText(getString(R.string.str_formate, checkData(MyConstants.orderData.get("color_start_name")),  checkData(MyConstants.orderData.get("color_end_name"))));
                } else {
                    tvDiamondQuality.setText(MyConstants.orderData.get("color"));
                }

                tvDiamondClarity.setText(getString(R.string.str_formate, MyConstants.orderData.get("clearity_start_name"), MyConstants.orderData.get("clearity_end_name")));

                if (MyConstants.orderData.get("MaterialType").equalsIgnoreCase("Other")) {
                    addLayout("Gem Material", MyConstants.orderData.get("materialType"));
                } else {
                    addLayout("Gem Material", MyConstants.orderData.get("MaterialType"));
                }

                llJewelleryType.setVisibility(View.GONE);
                llCaratWeight.setVisibility(View.VISIBLE);
                llEstimatedValue.setVisibility(View.VISIBLE);
                llPictures.setVisibility(View.VISIBLE);
            }

            btnNext.setClickable(true);
            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btnNext.setClickable(false);

                    Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                    // Use bounce interpolator with amplitude 0.2 and frequency 20
                    MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                    myAnim.setInterpolator(interpolator);

                    btnNext.startAnimation(myAnim);

                    new Handler().postDelayed(() -> {
                        //Do something after 100ms
                        ((HomeActivity) getActivity()).setIndex(5, TAG_PAYMENT);
                        ((HomeActivity) getActivity()).replaceFragment(null);
                    }, 1000);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        setHasOptionsMenu(true);

        return view;
    }

    private String checkData(String color_name) {
        if (!TextUtils.isEmpty(MyConstants.orderData.get("color"))) {
            return color_name.equalsIgnoreCase("more") ? MyConstants.orderData.get("color") : color_name;
        }
        return color_name;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    private void addLayout(String title, String value) {

        View layout2 = LayoutInflater.from(getActivity()).inflate(R.layout.item_order_other_attributes, llOthers, false);

        TextView tvTitle = layout2.findViewById(R.id.tvTitle);
        TextView tvValue = layout2.findViewById(R.id.tvValue);

        tvTitle.setText(title);
        tvValue.setText(value);

        llOthers.addView(layout2);
    }

}
