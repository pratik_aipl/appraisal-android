package com.federal.appraisal.fragment;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.federal.appraisal.BuildConfig;
import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.activity.WebViewActivity;
import com.federal.appraisal.adapter.MyCertificatesAdapter;
import com.federal.appraisal.others.App;
import com.federal.appraisal.others.Internet;
import com.federal.appraisal.utils.VerticalSpacingDecoration;
import com.federal.appraisal.ws.AsyncTaskListner;
import com.federal.appraisal.ws.CallRequest;
import com.federal.appraisal.ws.Constant;
import com.federal.appraisal.ws.MyConstants;
import com.federal.appraisal.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.federal.appraisal.ws.Utils.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyCertificatesFragment extends Fragment implements AsyncTaskListner {

    private static final String TAG = "MyCertificatesFragment";
    public static final int progress_bar_type = 0;
    public static ProgressDialog pDialog;
    @SuppressLint("StaticFieldLeak")
    static Context context;
    static String certificate, card;
    static File fileURL;
    RecyclerView rvCertificates;
    List<HashMap<String, String>> certificateList = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    MyCertificatesAdapter myCertificatesAdapter;
    EditText etSearch;
    String nameCertificate, nameCard;
    public static Dialog dialog;
    public Button btnCertificate, btnCard;

    public MyCertificatesFragment() {
        // Required empty public constructor
    }

    public static void downloadCard(String cardLink) {
        File sdCardDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/Federal_Appraisal/Cards");

        if (!sdCardDirectory.exists()) {
            sdCardDirectory.mkdirs();
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());

        String nw = "Card" + timeStamp + ".pdf";

        File file = new File(sdCardDirectory, nw);

        fileURL = file;
        certificate = cardLink;

        try {
            fileURL.createNewFile();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        new DownloaderCard().execute();
        // Downloader.DownloadFile(certificateLink, file);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_certificates, container, false);

        try {
            context = getActivity();

            ((HomeActivity) getActivity()).setFragmentTitle(1);
            ((HomeActivity) getActivity()).setIndex(1, HomeActivity.TAG_CERTIFICATES);

            rvCertificates = view.findViewById(R.id.rvCertificates);
            etSearch = view.findViewById(R.id.etSearch);

            getAppraisalList();

            linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            rvCertificates.setLayoutManager(linearLayoutManager);

            etSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    try {
                        Log.i("TAG", "SEARCH" + charSequence.toString());
                        myCertificatesAdapter.filter(charSequence.toString());
                    /*if (charSequence.length() > 0) {
                        //ivSearch.setVisibility(View.GONE);
                    } else {
                        //ivSearch.setVisibility(View.VISIBLE);
                    }*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    /*private void addData() {
        certificateList.add("April-3-2018");
        certificateList.add("April-3-2018");
        certificateList.add("April-2-2018");
        certificateList.add("April-2-2018");
        certificateList.add("April-2-2018");
    }*/

    private void getAppraisalList() {

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
        } else {

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "appraisal_list?");
            map.put("header", "");
            map.put("token", App.user.getToken());
            map.put("user_id", App.user.getUserID());

            new CallRequest(MyCertificatesFragment.this).getAppraisalList(map);
        }

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case getAppraisalList:
                        certificateList.clear();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {
                                //Utils.showToast(mainObj.getString("message"), LoginActivity.this);

                                JSONArray array = mainObj.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object = array.getJSONObject(i);

                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("OrderId", object.getString("Orderid"));
                                    map.put("OrderDate", object.getString("OrderDate"));
                                    map.put("CustomerName", object.getString("CustomerName"));
                                    map.put("OrderStatus", object.getString("OrderStatus"));
                                    map.put("CertificateLink", object.getString("certificate_link"));
                                    map.put("TrackingNO", object.getString("TrackingNO"));
                                    map.put("CertificateNo", object.getString("CertificateNo"));
                                    map.put("CardLink", object.getString("card_link"));

                                    certificateList.add(map);
                                }

                                myCertificatesAdapter = new MyCertificatesAdapter(getActivity(), certificateList, MyCertificatesFragment.this);
                                rvCertificates.setHasFixedSize(true);
                                rvCertificates.addItemDecoration(new VerticalSpacingDecoration(20));
                                rvCertificates.setItemViewCacheSize(20);
                                rvCertificates.setDrawingCacheEnabled(true);
                                rvCertificates.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                                rvCertificates.setAdapter(myCertificatesAdapter);

                            } else {
                                Utils.showToast(mainObj.getString("message"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    public void openDownloadDialog(int pos, final String certificateLink, final String cardLink){
        certificate = certificateLink;
        card = cardLink;

        createDialog();
        initDialogComponents();
        Log.d(TAG, "openDownloadDialog: "+certificateLink);
        if (TextUtils.isEmpty(certificateLink)){
            btnCertificate.setBackground(getResources().getDrawable(R.drawable.bg_button_grey));
        } else {
            btnCertificate.setBackground(getResources().getDrawable(R.drawable.bg_button));
            btnCertificate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("link", certificateLink));
                }
            });
        }

        if (TextUtils.isEmpty(cardLink)){
            btnCard.setBackground(getResources().getDrawable(R.drawable.bg_button_grey));
        } else {
            btnCard.setBackground(getResources().getDrawable(R.drawable.bg_button));
            btnCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("link", cardLink));
                }
            });
        }
    }

    private void createDialog() {
        dialog = new Dialog(getActivity(), R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_download);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }

    private void initDialogComponents() {
        btnCertificate = dialog.findViewById(R.id.btnCertificate);
        btnCard = dialog.findViewById(R.id.btnCard);
    }

    public void downloadPDF(int pos, String certificateLink, String cardLink) {

        certificate = certificateLink;
        card = cardLink;

        File sdCardDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/Federal_Appraisal/Certificates");

        if (!sdCardDirectory.exists()) {
            sdCardDirectory.mkdirs();
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS")
                .format(new Date());

        nameCertificate = "Certificate_" + timeStamp + ".pdf";
        nameCard = "Card" + timeStamp + ".pdf";

        if (Utils.checkStoragePermission(getActivity())) {
            Utils.downloadFileWithNotification(certificateLink, Environment.getExternalStorageDirectory().getPath() + "/Federal_Appraisal/Certificates/", nameCertificate, getActivity(),"Certificate downloaded successfully");
            Utils.downloadFileWithNotification(cardLink, Environment.getExternalStorageDirectory().getPath() + "/Federal_Appraisal/Cards/", nameCard, getActivity(),"Card downloaded successfully");
        }

        /*File sdCardDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/Federal_Appraisal/Certificates");

        if (!sdCardDirectory.exists()) {
            sdCardDirectory.mkdirs();
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());

        String nw = "Certificate_" + timeStamp + ".pdf";

        File file = new File(sdCardDirectory, nw);

        fileURL = file;
        certificate = certificateLink;
        card = cardLink;

        try {
            fileURL.createNewFile();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        new Downloader().execute();*/

        // Downloader.DownloadFile(certificateLink, file);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE){
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Utils.downloadFileWithNotification(certificate, Environment.getExternalStorageDirectory().getPath() + "/Federal_Appraisal/Certificates/", nameCertificate, getActivity(),"Certificate downloaded successfully");
                    Utils.downloadFileWithNotification(card, Environment.getExternalStorageDirectory().getPath() + "/Federal_Appraisal/Cards/", nameCard, getActivity(),"Card downloaded successfully");
                }
            }
        }
    }

    public static class Downloader extends AsyncTask<Void, String, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        private void showDialog(int id) {
            switch (id) {
                case progress_bar_type:
                    pDialog = new ProgressDialog(context);
                    pDialog.setMessage("Downloading certificate. Please wait...");
                    pDialog.setIndeterminate(false);
                    pDialog.setMax(100);
                    pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pDialog.setCancelable(true);
                    pDialog.show();
                    return;
                default:
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pDialog.dismiss();
            Utils.showToast("Certificate downloaded successfully", context);

            if (!TextUtils.isEmpty(card)) {
                downloadCard(card);
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {

                FileOutputStream f = new FileOutputStream(fileURL);
                URL u = new URL(certificate);
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();

                int lengthOfFile = c.getContentLength();

                InputStream in = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                long total = 0;

                while ((len1 = in.read(buffer)) > 0) {
                    total += len1;
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    f.write(buffer, 0, len1);
                }
                f.close();

                System.out.println("completed downloading");
            } catch (Exception e) {
                e.printStackTrace();
                Utils.showToast("Something went wrong", context);
            }

            return null;
        }

        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }
    }

    public static class DownloaderCard extends AsyncTask<Void, String, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        private void showDialog(int id) {
            switch (id) {
                case progress_bar_type:
                    pDialog = new ProgressDialog(context);
                    pDialog.setMessage("Downloading card. Please wait...");
                    pDialog.setIndeterminate(false);
                    pDialog.setMax(100);
                    pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pDialog.setCancelable(true);
                    pDialog.show();
                    return;
                default:
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pDialog.dismiss();
            Utils.showToast("Card downloaded successfully", context);
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {

                FileOutputStream f = new FileOutputStream(fileURL);
                URL u = new URL(certificate);
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();

                int lengthOfFile = c.getContentLength();

                InputStream in = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                long total = 0;

                while ((len1 = in.read(buffer)) > 0) {
                    total += len1;
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    f.write(buffer, 0, len1);
                }
                f.close();

                System.out.println("completed downloading");
            } catch (Exception e) {
                e.printStackTrace();
                Utils.showToast("Something went wrong", context);
            }

            return null;
        }

        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }
    }

}
