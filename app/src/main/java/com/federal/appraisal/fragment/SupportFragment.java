package com.federal.appraisal.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;

import com.federal.appraisal.BuildConfig;
import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.animation.MyBounceInterpolator;
import com.federal.appraisal.others.App;
import com.federal.appraisal.others.Internet;
import com.federal.appraisal.ws.AsyncTaskListner;
import com.federal.appraisal.ws.CallRequest;
import com.federal.appraisal.ws.Constant;
import com.federal.appraisal.ws.MyConstants;
import com.federal.appraisal.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class SupportFragment extends Fragment implements AsyncTaskListner{

    EditText etSubject, etComment;
    Button btnSend;

    public SupportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_support, container, false);

        try {
            ((HomeActivity) getActivity()).setFragmentTitle(2);
            ((HomeActivity) getActivity()).setIndex(2, HomeActivity.TAG_SUPPORT);

            etSubject = view.findViewById(R.id.etSubject);
            etComment = view.findViewById(R.id.etComment);
            btnSend = view.findViewById(R.id.btnSend);

            btnSend.setEnabled(true);
            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String subject = etSubject.getText().toString().trim();
                    String comment = etComment.getText().toString().trim();

                    if (TextUtils.isEmpty(subject)) {
                        etSubject.requestFocus();
                        etSubject.setError("Subject is required");
                    } else if (TextUtils.isEmpty(comment)) {
                        etComment.requestFocus();
                        etComment.setError("Comment is required");
                    } else {
                        btnSend.setClickable(false);

                        final Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);

                        // Use bounce interpolator with amplitude 0.2 and frequency 20
                        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                        myAnim.setInterpolator(interpolator);

                        btnSend.startAnimation(myAnim);

                        addSupport(subject, comment);
                    }

                }
            });
        } catch (Exception e){
            e.printStackTrace();
        }

        setHasOptionsMenu(true);

        return view;
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }


    private void addSupport(String subject, String comment) {

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
            btnSend.setClickable(true);
        } else {

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "support");
            map.put("header", "");
            map.put("token", App.user.getToken());
            map.put("user_id", App.user.getUserID());
            map.put("subject", subject);
            map.put("comment", comment);

            new CallRequest(SupportFragment.this).addSupport(map);
        }

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case addSupport:
                        btnSend.setClickable(true);
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {
                                Utils.showToast("Support added successfully", getActivity());

                                etSubject.setText("");
                                etComment.setText("");
                            } else {
                                Utils.showToast(mainObj.getString("message"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
