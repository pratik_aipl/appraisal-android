package com.federal.appraisal.fragment;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.activity.TermsAndConditionsActivity;
import com.federal.appraisal.adapter.JewelleryAdapter;
import com.federal.appraisal.adapter.NothingSelectedSpinnerAdapter;
import com.federal.appraisal.animation.MyBounceInterpolator;
import com.federal.appraisal.custom_views.CustomTextView;
import com.federal.appraisal.materialrangebar.RangeBar;
import com.federal.appraisal.utils.VerticalSpacingDecoration;
import com.federal.appraisal.ws.MyConstants;
import com.federal.appraisal.ws.Utils;
import com.warkiz.widget.IndicatorSeekBar;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;
import static com.federal.appraisal.activity.HomeActivity.TAG_JEWELLERY;
import static com.federal.appraisal.activity.HomeActivity.TAG_ORDER_DETAIL;

/**
 * A simple {@link Fragment} subclass.
 */
public class JewelleryFragment extends Fragment {

    private static final String TAG = "JewelleryFragment";

    private static int RESULT_CROP_DP = 3;
    public final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public Uri selectedUri;
    IndicatorSeekBar seekBarMetal;
    CardView cMoreView;
    RecyclerView rvImages;
    List<String> imagePath;
    LinearLayoutManager linearLayoutManager;
    Button btnNext;
    String filePathDp = "";
    String filePathFirst = "";
    JewelleryAdapter jewelleryAdapter;
    CardView mCaretView;
    Spinner /*spinnerJewelleryType,*/ spinnerGemMaterial;
    String gemMaterialId, jewelleryTypeId, metalTypeStr, gemMaterialStr, jewelleryTypeStr,
            jew_clearity_start, jew_clearity_end, jew_color_start, jew_color_end, jew_clearity_start_name, jew_clearity_end_name, jew_color_start_name, jew_color_end_name;
    EditText etCustomerName, etCaratWeight, etEstimatedValue, etComment, etOtherMaterial, etJewelleryType, etWeightOfItem,etMoreColorValue;
    CheckBox cbTerms;
    CustomTextView mDiamondQualityCheck, mDiamondClarityCheck;
    TextView tvTerms;
    //    private String[] metalType;
//    private String[] jewelleryType, gemMaterial;
    private String[] jewelleryType, metalType, color, clarityGrade, gemMaterial;

    private CardView cvOtherMaterial;
    RelativeLayout mClearityView, mDiamondQuality;
    RangeBar mClarityRangeBar, mDiamondColorRangeBar;

    public JewelleryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInmDiamondClarityCheckstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_jewellery, container, false);

        try {
            ((HomeActivity) getActivity()).setFragmentTitle(10);
            ((HomeActivity) getActivity()).setIndex(10, TAG_JEWELLERY);

            imagePath = new ArrayList<>();

            seekBarMetal = view.findViewById(R.id.seekbarMetal);
            mDiamondQualityCheck = view.findViewById(R.id.mDiamondQualityCheck);
            mDiamondClarityCheck = view.findViewById(R.id.mDiamondClarityCheck);
            mClearityView = view.findViewById(R.id.mClearityView);
            mDiamondQuality = view.findViewById(R.id.mDiamondQuality);
            mCaretView = view.findViewById(R.id.mCaretView);

            cMoreView = view.findViewById(R.id.cMoreView);
            etMoreColorValue = view.findViewById(R.id.etMoreColorValue);


            mClarityRangeBar = view.findViewById(R.id.mClarityRangeBar);
            mDiamondColorRangeBar = view.findViewById(R.id.mDiamondQualityRangeBar);

            metalType = getResources().getStringArray(R.array.metal_type_jewellery);

            clarityGrade = new String[MyConstants.allMaterial.getClarity().size()];
            for (int i = 0; i < MyConstants.allMaterial.getClarity().size(); i++) {
                clarityGrade[i] = MyConstants.allMaterial.getClarity().get(i).get("ClarityName");
            }

            color = new String[MyConstants.allMaterial.getColor().size()];
            for (int i = 0; i < MyConstants.allMaterial.getColor().size(); i++) {
                color[i] = MyConstants.allMaterial.getColor().get(i).get("ColorName");
            }

            mClarityRangeBar.setTickTopLabels(clarityGrade);
            mDiamondColorRangeBar.setTickTopLabels(color);

            if (MyConstants.allMaterial.getClarity().size() > 0) {
                jew_clearity_start = MyConstants.allMaterial.getClarity().get(0).get("ClarityID");
                jew_clearity_end = MyConstants.allMaterial.getClarity().get(1).get("ClarityID");

                jew_clearity_start_name = MyConstants.allMaterial.getClarity().get(0).get("ClarityName");
                jew_clearity_end_name = MyConstants.allMaterial.getClarity().get(1).get("ClarityName");
            }
            mClarityRangeBar.setTickEnd(MyConstants.allMaterial.getClarity().size());
            mClarityRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
                @Override
                public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                    jew_clearity_start = MyConstants.allMaterial.getClarity().get(leftPinIndex).get("ClarityID");
                    jew_clearity_end = MyConstants.allMaterial.getClarity().get(rightPinIndex).get("ClarityID");

                    jew_clearity_start_name = MyConstants.allMaterial.getClarity().get(leftPinIndex).get("ClarityName");
                    jew_clearity_end_name = MyConstants.allMaterial.getClarity().get(rightPinIndex).get("ClarityName");
                }

                @Override
                public void onTouchStarted(RangeBar rangeBar) {

                }

                @Override
                public void onTouchEnded(RangeBar rangeBar) {

                }
            });

//            mDiamondQualityCheck.setOnCheckedChangeListener((compoundButton, b) -> {
//                mDiamondColorRangeBar.setEnabled(b);
//            });
//            mDiamondClarityCheck.setOnCheckedChangeListener((compoundButton, b) -> {
//                mClarityRangeBar.setEnabled(b);
//            });

            if (MyConstants.allMaterial.getColor().size() > 0) {
                jew_color_start = MyConstants.allMaterial.getColor().get(0).get("ColorID");
                jew_color_end = MyConstants.allMaterial.getColor().get(color.length-1).get("ColorID");

                jew_color_start_name = MyConstants.allMaterial.getColor().get(0).get("ColorName");
                jew_color_end_name = MyConstants.allMaterial.getColor().get(color.length-1).get("ColorName");
                if (MyConstants.allMaterial.getColor().get(0).get("ColorID").equalsIgnoreCase("11") ||MyConstants.allMaterial.getColor().get(color.length - 1).get("ColorID").equalsIgnoreCase("11")){
                    cMoreView.setVisibility(View.VISIBLE);
                }else{
                    cMoreView.setVisibility(View.GONE);
                }
            }


            mDiamondColorRangeBar.setTickEnd(MyConstants.allMaterial.getColor().size());
            mDiamondColorRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
                @Override
                public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                    jew_color_start = MyConstants.allMaterial.getColor().get(leftPinIndex).get("ColorID");
                    jew_color_end = MyConstants.allMaterial.getColor().get(rightPinIndex).get("ColorID");

                    jew_color_start_name = MyConstants.allMaterial.getColor().get(leftPinIndex).get("ColorName");
                    jew_color_end_name = MyConstants.allMaterial.getColor().get(rightPinIndex).get("ColorName");
                    if (jew_color_start.equalsIgnoreCase("11")||jew_color_end.equalsIgnoreCase("11")) {
                        cMoreView.setVisibility(View.VISIBLE);
                    } else {
                        cMoreView.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onTouchStarted(RangeBar rangeBar) {

                }

                @Override
                public void onTouchEnded(RangeBar rangeBar) {

                }
            });

            seekBarMetal.setTextArray(metalType);
            metalTypeStr = metalType[0];

            seekBarMetal.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {

                }

                @Override
                public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {
                    System.out.println("textBelowTick:::" + textBelowTick);
                    metalTypeStr = textBelowTick;
                }

                @Override
                public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

                }

                @Override
                public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

                }
            });

            rvImages = view.findViewById(R.id.rvImages);
            btnNext = view.findViewById(R.id.btnNext);
            etCustomerName = view.findViewById(R.id.etCustomerName);
            etCaratWeight = view.findViewById(R.id.etCaratWeight);
            etWeightOfItem = view.findViewById(R.id.etWeightOfItem);
            etEstimatedValue = view.findViewById(R.id.etEstimatedValue);
            etComment = view.findViewById(R.id.etComment);
            etJewelleryType = view.findViewById(R.id.etJewelleryType);
            cbTerms = view.findViewById(R.id.cbTerms);
            //spinnerJewelleryType = view.findViewById(R.id.spinnerJewelleryType);
            spinnerGemMaterial = view.findViewById(R.id.spinnerGemMaterial);
            tvTerms = view.findViewById(R.id.tvTerms);
            cvOtherMaterial = view.findViewById(R.id.cvOtherMaterial);
            etOtherMaterial = view.findViewById(R.id.etOtherMaterial);

            tvTerms.setOnClickListener(v -> {
                Intent intent = new Intent(getActivity(), TermsAndConditionsActivity.class);
                startActivity(intent);
            });

            gemMaterial = new String[MyConstants.allMaterial.getMaterialJewelaryType().size()];
            for (int i = 0; i < MyConstants.allMaterial.getMaterialJewelaryType().size(); i++) {
                gemMaterial[i] = MyConstants.allMaterial.getMaterialJewelaryType().get(i).get("MaterialType");
            }

            ArrayAdapter aa = new ArrayAdapter<>(getContext(), R.layout.spinner_text, gemMaterial);
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinnerGemMaterial.setAdapter(new NothingSelectedSpinnerAdapter(aa, R.layout.nothing_selected_layout, getContext()));
            // spinnerGemMaterial.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, gemMaterial));

            if (gemMaterial.length > 1)
                spinnerGemMaterial.setSelection(1);

            jewelleryType = new String[MyConstants.allMaterial.getJewelaryType().size()];
            for (int i = 0; i < MyConstants.allMaterial.getJewelaryType().size(); i++) {
                jewelleryType[i] = MyConstants.allMaterial.getJewelaryType().get(i).get("JewelarystyleType");
            }

            spinnerGemMaterial.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        System.out.println("position:::" + position);
//                        if (position != 0) {
//                            position = position - 1;
//                        }
                        Log.d(TAG, "onItemSelected: pos "+position);
                        if (position !=0){
                            gemMaterialId = MyConstants.allMaterial.getMaterialJewelaryType().get(position-1).get("MaterialTypeID");
                            gemMaterialStr = gemMaterial[position-1];

                            Log.d(TAG, "onItemSelected: gemMaterial "+gemMaterialStr);


                            if (gemMaterialStr.equalsIgnoreCase("Other")) {
                                cvOtherMaterial.setVisibility(View.VISIBLE);
                                etCaratWeight.setText("");
                                etCaratWeight.setEnabled(true);
                                mDiamondQuality.setVisibility(View.VISIBLE);
                                mClearityView.setVisibility(View.VISIBLE);
                                mCaretView.setVisibility(View.VISIBLE);
                                if (jew_color_start.equalsIgnoreCase("11") || jew_color_end.equalsIgnoreCase("11")) {
                                    cMoreView.setVisibility(View.VISIBLE);
                                } else {
                                    cMoreView.setVisibility(View.GONE);
                                }
                            } else if (gemMaterialStr.equalsIgnoreCase("None")) {
                                etCaratWeight.setText("0");
                                etCaratWeight.setEnabled(false);
                                cvOtherMaterial.setVisibility(View.GONE);
                                mDiamondQuality.setVisibility(View.GONE);
                                mClearityView.setVisibility(View.GONE);
                                mCaretView.setVisibility(View.GONE);
                                cMoreView.setVisibility(View.GONE);
                            } else {
                                if (jew_color_start.equalsIgnoreCase("11") || jew_color_end.equalsIgnoreCase("11")) {
                                    cMoreView.setVisibility(View.VISIBLE);
                                } else {
                                    cMoreView.setVisibility(View.GONE);
                                }
                                cvOtherMaterial.setVisibility(View.GONE);
                                etCaratWeight.setText("");
                                etCaratWeight.setEnabled(true);
                                mDiamondQuality.setVisibility(View.VISIBLE);
                                mClearityView.setVisibility(View.VISIBLE);
                                mCaretView.setVisibility(View.VISIBLE);
                            }
                        }else {
                            mDiamondQuality.setVisibility(View.GONE);
                            mClearityView.setVisibility(View.GONE);
                            cMoreView.setVisibility(View.GONE);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    gemMaterialId = "";
                    gemMaterialStr = "";
                    mDiamondQuality.setVisibility(View.GONE);
                    mClearityView.setVisibility(View.GONE);
                }
            });

            //imagePath.clear();
            imagePath.add("by default");
            if (MyConstants.imagePath.size() > 0 && MyConstants.orderData.get("ItemId").equalsIgnoreCase("3")) {
                imagePath.addAll(MyConstants.imagePath);
            }
            linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            rvImages.setLayoutManager(linearLayoutManager);

            jewelleryAdapter = new JewelleryAdapter(getActivity(), imagePath, JewelleryFragment.this);
            rvImages.setHasFixedSize(true);
            rvImages.addItemDecoration(new VerticalSpacingDecoration(20));
            rvImages.setItemViewCacheSize(20);
            rvImages.setDrawingCacheEnabled(true);
            rvImages.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            rvImages.setAdapter(jewelleryAdapter);

            btnNext.setClickable(true);
            btnNext.setOnClickListener(v -> {
                String customerName = etCustomerName.getText().toString().trim();
                String caratWeight = etCaratWeight.getText().toString().trim();
                String weightOfItem = etWeightOfItem.getText().toString().trim();
                String estimatedValue = etEstimatedValue.getText().toString().trim();
                String comment = etComment.getText().toString().trim();
                String jewelleryType = etJewelleryType.getText().toString().trim();
                String otherMaterial = etOtherMaterial.getText().toString().trim();

               /* if (TextUtils.isEmpty(customerName)) {
                    etCustomerName.requestFocus();
                    etCustomerName.setError("Customer name is required");
                } else*/ if (TextUtils.isEmpty(jewelleryType)) {
                    etJewelleryType.requestFocus();
                    etJewelleryType.setError("Jewelry type is required");
                } else if (TextUtils.isEmpty(gemMaterialId)) {
                    Utils.showAlert("Please select GEM MATERIAL", getActivity());
                } else if (gemMaterialStr.equalsIgnoreCase("Other") && TextUtils.isEmpty(otherMaterial)) {
                    etOtherMaterial.requestFocus();
                    etOtherMaterial.setError("Other material is required");
                } /*else if (TextUtils.isEmpty(caratWeight)) {
                    etCaratWeight.requestFocus();
                    etCaratWeight.setError("Carat weight is required");
                }*/ else if (TextUtils.isEmpty(weightOfItem)) {
                    etWeightOfItem.requestFocus();
                    etWeightOfItem.setError("Weight of item is required");
                }else if (cMoreView.getVisibility() == View.VISIBLE && TextUtils.isEmpty(etMoreColorValue.getText().toString().trim())) {
                    etMoreColorValue.requestFocus();
                    etMoreColorValue.setError("Please enter color info.");
                } else if (TextUtils.isEmpty(estimatedValue)) {
                    etEstimatedValue.requestFocus();
                    etEstimatedValue.setError("Estimated value is required");
                } else if (imagePath.size() < 2) {
                    Utils.showAlert("Select at least one image", getActivity());
                } else if (!cbTerms.isChecked()) {
                    Utils.showAlert("You must agree to the terms and conditions before submitting!", getActivity());
                } else {
                    btnNext.setClickable(false);
                    Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                    MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                    myAnim.setInterpolator(interpolator);
                    btnNext.startAnimation(myAnim);

                    MyConstants.orderData.clear();
                    MyConstants.orderData.put("ItemId", "3");
                    MyConstants.orderData.put("customerName", customerName);
                    MyConstants.orderData.put("caratWeight", caratWeight);
                    MyConstants.orderData.put("weight_of_item", weightOfItem);
                    MyConstants.orderData.put("estimatedValue", estimatedValue);
                    MyConstants.orderData.put("jewelleryTypeId", jewelleryType);
                    MyConstants.orderData.put("jewelleryType", jewelleryType);
                    MyConstants.orderData.put("gemMaterialId", gemMaterialId);
                    MyConstants.orderData.put("gemMaterial", gemMaterialStr);
                    MyConstants.orderData.put("materialType", otherMaterial);
                    MyConstants.orderData.put("metalType", metalTypeStr);

                    if (gemMaterialStr.equalsIgnoreCase("none")) {
                        MyConstants.orderData.put("jew_clearity_start", "0");
                        MyConstants.orderData.put("jew_clearity_end", "0");
                        MyConstants.orderData.put("jew_color_start", "0");
                        MyConstants.orderData.put("jew_color_end", "0");

                        MyConstants.orderData.put("jew_clearity_start_name", "N/A");
                        MyConstants.orderData.put("jew_clearity_end_name", "N/A");

                        MyConstants.orderData.put("jew_color_start_name", "N/A");
                        MyConstants.orderData.put("jew_color_end_name", "N/A");

                    } else {
                        MyConstants.orderData.put("jew_clearity_start", jew_clearity_start);
                        MyConstants.orderData.put("jew_clearity_end", jew_clearity_end);
                        MyConstants.orderData.put("jew_color_start", jew_color_start);
                        MyConstants.orderData.put("jew_color_end", jew_color_end);

                        MyConstants.orderData.put("jew_clearity_start_name", jew_clearity_start_name);
                        MyConstants.orderData.put("jew_clearity_end_name", jew_clearity_end_name);

                        MyConstants.orderData.put("jew_color_start_name", jew_color_start_name);
                        MyConstants.orderData.put("jew_color_end_name", jew_color_end_name);

                    }

                    if (cMoreView.getVisibility() == View.VISIBLE && !TextUtils.isEmpty(etMoreColorValue.getText().toString().trim()))
                        MyConstants.orderData.put("color", etMoreColorValue.getText().toString().trim());
                    else
                        MyConstants.orderData.put("color", "");


                    MyConstants.orderData.put("jew_clearity_check",gemMaterialStr.equalsIgnoreCase("None")?"1":"0");
                    MyConstants.orderData.put("jew_color_check",gemMaterialStr.equalsIgnoreCase("None")?"1":"0");

                    MyConstants.orderData.put("imagesCount", String.valueOf(imagePath.size() - 1));
                    MyConstants.orderData.put("comment", comment);
                    MyConstants.imagePath.clear();
                    MyConstants.imagePath = imagePath;
                    MyConstants.imagePath.remove(0);

                    ((HomeActivity) getActivity()).setIndex(4, TAG_ORDER_DETAIL);
                    ((HomeActivity) getActivity()).replaceFragment(null);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        setHasOptionsMenu(true);

        return view;
    }
    @Override
    public void onResume() {
        super.onResume();

    }
    public void selectImage() {
        try {
            final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
            builder.setTitle("Select Option");
            builder.setItems(options, (dialog, item) -> {
                if (options[item].equals("Take Photo")) {
                    dialog.dismiss();
                    File f = new File(Environment.getExternalStorageDirectory() + "/Federal_Appraisal/Images");
                    if (!f.exists()) {
                        f.mkdirs();
                    }
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(), "/Federal_Appraisal/Images" + System.currentTimeMillis() + ".jpg");
                    selectedUri = Uri.fromFile(file);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                    startActivityForResult(intent, PICK_IMAGE_CAMERA);

                }
                if (options[item].equals("Choose From Gallery")) {
                    dialog.dismiss();
                    Intent i = new Intent(Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, PICK_IMAGE_GALLERY);
                }
            });
            builder.show();

        } catch (Exception e) {
            Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PICK_IMAGE_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImage();
                } else {
                    Toast.makeText(getActivity(), "The app was not allowed to write in your storage", Toast.LENGTH_LONG).show();
                }
            }
            case PICK_IMAGE_GALLERY: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImage();
                } else {
                    Toast.makeText(getActivity(), "The app was not allowed to write in your storage", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public String getPath(Uri uri) {
        return new File(Objects.requireNonNull(uri.getPath())).getAbsolutePath();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_CAMERA) {
            filePathFirst = getPath(selectedUri);
            System.out.println("image path::::" + filePathFirst);
            System.out.println("uri:::" + selectedUri);
            try {
                imagePath.add(filePathFirst);
                jewelleryAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (PICK_IMAGE_GALLERY == 2 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            filePathFirst = cursor.getString(columnIndex);
            imagePath.add(filePathFirst);
            jewelleryAdapter.notifyDataSetChanged();
            cursor.close();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

//    public void loadImageFromGallery() {
//        // Create intent to Open Image applications like Gallery, Google Photos
//        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        // Start the Intent
//        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        try {
//            // When an Image is picked
//            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
//                    && null != data) {
//                // Get the Image from data
//
//                Uri selectedImage = data.getData();
//                String[] filePathColumn = {MediaStore.Images.Media.DATA};
//
//                // Get the cursor
//                Cursor cursor = null;
//                if (selectedImage != null) {
//                    cursor = getActivity().getContentResolver().query(selectedImage,
//                            filePathColumn, null, null, null);
//                }
//                // Move to first row
//                int columnIndex = 0;
//                if (cursor != null) {
//                    cursor.moveToFirst();
//                    columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//                    filePathFirst = cursor.getString(columnIndex);
//                    System.out.println(filePathFirst);
//                    //imagePath.add(filePathFirst);
//                    cursor.close();
//
//                    //mentionQueryAdapter.notifyDataSetChanged();
//                    //drQuestionnaire3Adapter.notifyDataSetChanged();
//                }
//
//                doCropDP(filePathFirst);
//
//            }
//
//            if (requestCode == RESULT_CROP_DP) {
//                if (resultCode == RESULT_OK) {
//                    if (!TextUtils.isEmpty(filePathDp)) {
//
//                        Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePathDp, 100, 100);
//                        System.out.println("selectedBitmap:::::" + selectedBitmap);
//
//                        for (int i = 0; i < imagePath.size(); i++) {
//                            System.out.println("image list::" + imagePath.get(i));
//                        }
//
//                        // Set The Bitmap Data To ImageView
//                        imagePath.add(filePathDp);
//                        //ivUser.setImageBitmap(selectedBitmap);
//                        //ivPageDp.setScaleType(ImageView.ScaleType.FIT_XY);
//                        jewelleryAdapter.notifyDataSetChanged();
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private void doCropDP(String picPath) {
        try {

            Intent cropIntent = new Intent("com.android.camera.action.CROP");

            File f = new File(picPath);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");

            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", false);

            File sdCardDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/Federal_Appraisal/Images");

            if (!sdCardDirectory.exists()) {
                sdCardDirectory.mkdirs();
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                    .format(new Date());

            String nw = "Image_" + timeStamp + ".jpeg";

            File image = new File(sdCardDirectory, nw);

            //uploadPathDp = sdCardDirectory + "/" + nw;
            filePathDp = new File(sdCardDirectory, nw).getAbsolutePath();
            System.out.println("UploadPath:::" + filePathDp);

            try {
                image.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }

            Uri uri = Uri.fromFile(image);

            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP_DP);
        } catch (ActivityNotFoundException anfe) {
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

}
