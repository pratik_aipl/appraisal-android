package com.federal.appraisal.fragment;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.federal.appraisal.BuildConfig;
import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.adapter.CommentsAdapter;
import com.federal.appraisal.others.App;
import com.federal.appraisal.others.Internet;
import com.federal.appraisal.utils.VerticalSpacingDecoration;
import com.federal.appraisal.ws.AsyncTaskListner;
import com.federal.appraisal.ws.CallRequest;
import com.federal.appraisal.ws.Constant;
import com.federal.appraisal.ws.MyConstants;
import com.federal.appraisal.ws.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.federal.appraisal.activity.HomeActivity.TAG_FILE_UPLOAD;

/**
 * A simple {@link Fragment} subclass.
 */
public class CommentsFragment extends Fragment implements AsyncTaskListner {

    public static String orderId;
    private static int RESULT_LOAD_IMG = 1;
    private static int RESULT_CROP_DP = 3;
    RecyclerView rvComments;
    CommentsAdapter commentsAdapter;
    EditText etComment;
    ImageView ivSend, ivAttach;
    List<HashMap<String, String>> commentList = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    String filePathDp = "";
    String filePathFirst = "";

    public CommentsFragment() {
        // Required empty public constructor
    }

    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,
                                                     int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        return bmp;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_comments, container, false);
        try {
            ((HomeActivity) getActivity()).setFragmentTitle(13);
            ((HomeActivity) getActivity()).setIndex(13, HomeActivity.TAG_COMMENTS);

            Bundle bundle = getArguments();
            orderId = bundle.getString("order_id");

            rvComments = view.findViewById(R.id.rvComments);
            etComment = view.findViewById(R.id.etComment);
            ivSend = view.findViewById(R.id.ivSend);
            ivAttach = view.findViewById(R.id.ivAttach);

            getCommentsList();

            linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            rvComments.setLayoutManager(linearLayoutManager);

            ivSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String comment = etComment.getText().toString().trim();

                    if (TextUtils.isEmpty(comment)) {
                        return;
                    }


                    addComment(comment);
                }
            });

            ivAttach.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.checkPermission(getActivity())) {
                        loadImageFromGallery();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        setHasOptionsMenu(true);

        return view;
    }

    public void loadImageFromGallery() {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = null;
                if (selectedImage != null) {
                    cursor = getActivity().getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                }
                // Move to first row
                int columnIndex = 0;
                if (cursor != null) {
                    cursor.moveToFirst();
                    columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePathFirst = cursor.getString(columnIndex);
                    System.out.println(filePathFirst);
                    //imagePath.add(filePathFirst);
                    cursor.close();

                    //mentionQueryAdapter.notifyDataSetChanged();
                    //drQuestionnaire3Adapter.notifyDataSetChanged();
                }

                doCropDP(filePathFirst);

            }

            if (requestCode == RESULT_CROP_DP) {
                if (resultCode == RESULT_OK) {
                    if (!TextUtils.isEmpty(filePathDp)) {

                        Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePathDp, 100, 100);
                        System.out.println("selectedBitmap:::::" + selectedBitmap);

                        MyConstants.commentImage = filePathDp;


                        System.out.println("corped image==" + MyConstants.commentImage);

                        ((HomeActivity) getActivity()).setIndex(14, TAG_FILE_UPLOAD);
                        ((HomeActivity) getActivity()).replaceFragment(null);

                        // Set The Bitmap Data To ImageView
                        //ivUser.setImageBitmap(selectedBitmap);
                        //ivPageDp.setScaleType(ImageView.ScaleType.FIT_XY);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (TextUtils.isEmpty(MyConstants.commentUploadImage)) {
            // send image to server
            System.out.println("sending image to server");
        }
    }

    private void doCropDP(String picPath) {
        try {

            Intent cropIntent = new Intent("com.android.camera.action.CROP");

            File f = new File(picPath);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");

            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", false);

            File sdCardDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/Federal_Appraisal/Images");

            if (!sdCardDirectory.exists()) {
                sdCardDirectory.mkdirs();
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                    .format(new Date());

            String nw = "Image_" + timeStamp + ".jpeg";

            File image = new File(sdCardDirectory, nw);

            //uploadPathDp = sdCardDirectory + "/" + nw;
            filePathDp = new File(sdCardDirectory, nw).getAbsolutePath();
            System.out.println("UploadPath:::" + filePathDp);

            try {
                image.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }

            Uri uri = Uri.fromFile(image);

            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP_DP);
        } catch (ActivityNotFoundException anfe) {
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    private void addComment(String comment) {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
        } else {

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "comment");
            map.put("header", "");
            map.put("token", App.user.getToken());
            map.put("user_id", App.user.getUserID());
            map.put("OrderID", orderId);
            map.put("Comment", comment);
            map.put("CommentType", "T");

            new CallRequest(CommentsFragment.this).addComment(map);
        }
    }

    private void getCommentsList() {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
        } else {

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "comment/" + orderId + "?");
            map.put("header", "");
            map.put("token", App.user.getToken());
            map.put("user_id", App.user.getUserID());

            new CallRequest(CommentsFragment.this).getCommentList(map);
        }
    }

   /* private void addData() {
        commentList.add("admin");
        commentList.add("user");
        commentList.add("admin");
        commentList.add("user");
        commentList.add("admin");
    }*/

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case getCommentList:
                        commentList.clear();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {
                                //Utils.showToast(mainObj.getString("message"), LoginActivity.this);

                                JSONArray array = mainObj.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object = array.getJSONObject(i);

                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("BlogCommentID", object.getString("BlogCommentID"));
                                    map.put("Comment", object.getString("Comment"));
                                    map.put("CommentedDate", object.getString("CommentedDate"));
                                    map.put("ImageURL", object.getString("ImageURL"));
                                    map.put("LoginID", object.getString("LoginID"));
                                    map.put("CommentType", object.getString("CommentType"));

                                    commentList.add(map);
                                }

                                commentsAdapter = new CommentsAdapter(getActivity(), commentList, CommentsFragment.this);
                                rvComments.setHasFixedSize(true);
                                rvComments.addItemDecoration(new VerticalSpacingDecoration(20));
                                rvComments.setItemViewCacheSize(20);
                                rvComments.setDrawingCacheEnabled(true);
                                rvComments.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                                rvComments.setAdapter(commentsAdapter);

                                rvComments.smoothScrollToPosition(commentList.size() != 0 ? commentList.size() - 1 : 0);
                            } else {
                                Utils.showToast(mainObj.getString("message"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case addComment:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {
                                //Utils.showToast(mainObj.getString("message"), getActivity());

                                JSONObject object = mainObj.getJSONObject("data");

                                HashMap<String, String> map = new HashMap<>();
                                map.put("BlogCommentID", object.getString("BlogCommentID"));
                                map.put("Comment", object.getString("Comment"));
                                map.put("CommentedDate", object.getString("CommentedDate"));
                                if (object.has("ImageURL"))
                                    map.put("ImageURL", object.getString("ImageURL"));
                                map.put("LoginID", object.getString("LoginID"));
                                map.put("CommentType", object.getString("CommentType"));
                                commentList.add(map);

                                commentsAdapter.notifyDataSetChanged();
                                rvComments.smoothScrollToPosition(commentList.size() - 1);
                                etComment.setText("");
                                MyConstants.commentImage = "";

                                /*commentsAdapter = new CommentsAdapter(getActivity(), commentList, CommentsFragment.this);
                                rvComments.setHasFixedSize(true);
                                rvComments.addItemDecoration(new VerticalSpacingDecoration(20));
                                rvComments.setItemViewCacheSize(20);
                                rvComments.setDrawingCacheEnabled(true);
                                rvComments.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                                rvComments.setAdapter(commentsAdapter);*/

                            } else {
                                Utils.showToast(mainObj.getString("message"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.showToast("Try Again", getActivity());
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
