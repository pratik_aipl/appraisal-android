package com.federal.appraisal.fragment;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.federal.appraisal.BuildConfig;
import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.animation.MyBounceInterpolator;
import com.federal.appraisal.others.App;
import com.federal.appraisal.others.Internet;
import com.federal.appraisal.ws.AsyncTaskListner;
import com.federal.appraisal.ws.CallRequest;
import com.federal.appraisal.ws.Constant;
import com.federal.appraisal.ws.MyConstants;
import com.federal.appraisal.ws.Utils;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.federal.appraisal.activity.HomeActivity.TAG_CARDDETAIL;
import static com.federal.appraisal.activity.HomeActivity.TAG_ORDER_CONFIRM;
import static com.federal.appraisal.activity.HomeActivity.TAG_PAYMENT;


/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentFragment extends Fragment implements AsyncTaskListner {

    // for paypal
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;
    private static final String sandBox = "AVTu4D2bUNkuUffAO3A6rX7d5B7QXSafv1Fg34BCAAytod9xdBRZm9_xyTaVN9pYmspZcBw1zbDBE9Ci";
    private static final String live = "AXORmeUOWK9QW0hw68V6iO7kZMKYpasXqZiG6PaiwMrDJTs9oSc13g5uyRdoNi7mDl77OAtI4FGbhZwx";
    private static String CONFIG_ENVIRONMENT;
    private static PayPalConfiguration config;
    public String TAG = "Federal Appraisal Payment";
    RadioButton rbPayment1, rbPayment2, rbPayment3;
    RadioGroup rgShipping;
    TextView tvShipAppraisal, tvLocalPickupOnline, tvLocalPickup, tvAppraisalCost, tvShipping, tvPayableAmount, tvShipOvernight;
    Button btnPayNow;
    List<HashMap<String, String>> shippingList = new ArrayList<>();
    String shippingMethodID;
    float appraisalCost, shipping, payableAmount;
    String orderId, shippingMethod;
    String PaymentID;
    DecimalFormat df = new DecimalFormat("##.##");

    public PaymentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment, container, false);

        try {
            ((HomeActivity) getActivity()).setFragmentTitle(5);
            ((HomeActivity) getActivity()).setIndex(5, TAG_PAYMENT);
            df.setMinimumFractionDigits(2);
            if (MyConstants.PAYMENT_CONFIG_ENVIRONMENT.equalsIgnoreCase("0")) {
                CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
                config = new PayPalConfiguration()
                        .environment(CONFIG_ENVIRONMENT)
                        .clientId(sandBox)
                        .merchantName("Federal Appraisal")
                        .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
                        .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
            } else {
                CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
                config = new PayPalConfiguration()
                        .environment(CONFIG_ENVIRONMENT)
                        .clientId(live)
                        .merchantName("Federal Appraisal")
                        .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
                        .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
            }

            rbPayment1 = view.findViewById(R.id.rbPayment1);
            rbPayment2 = view.findViewById(R.id.rbPayment2);
            rbPayment3 = view.findViewById(R.id.rbPayment3);
            rgShipping = view.findViewById(R.id.rgShipping);
            tvShipAppraisal = view.findViewById(R.id.tvShipAppraisal);
            tvLocalPickupOnline = view.findViewById(R.id.tvLocalPickupOnline);
            tvLocalPickup = view.findViewById(R.id.tvLocalPickup);
            tvShipOvernight = view.findViewById(R.id.tvShipOvernight);
            tvAppraisalCost = view.findViewById(R.id.tvAppraisalCost);
            tvShipping = view.findViewById(R.id.tvShipping);
            tvPayableAmount = view.findViewById(R.id.tvPayableAmount);
            btnPayNow = view.findViewById(R.id.btnPayNow);

            PaymentID = "sku-" + System.currentTimeMillis();

            if (MyConstants.orderData.get("ItemId").equalsIgnoreCase("1")) {
                tvAppraisalCost.setText("$" + MyConstants.allMaterial.getItemType().get(0).get("AppraisalCost"));
                appraisalCost = Float.parseFloat(MyConstants.allMaterial.getItemType().get(0).get("AppraisalCost"));
            } else if (MyConstants.orderData.get("ItemId").equalsIgnoreCase("2")) {
                tvAppraisalCost.setText("$" + MyConstants.allMaterial.getItemType().get(1).get("AppraisalCost"));
                appraisalCost = Float.parseFloat(MyConstants.allMaterial.getItemType().get(1).get("AppraisalCost"));
            } else if (MyConstants.orderData.get("ItemId").equalsIgnoreCase("3")) {
                tvAppraisalCost.setText("$" + MyConstants.allMaterial.getItemType().get(2).get("AppraisalCost"));
                appraisalCost = Float.parseFloat(MyConstants.allMaterial.getItemType().get(2).get("AppraisalCost"));
            } else if (MyConstants.orderData.get("ItemId").equalsIgnoreCase("4")) {
                tvAppraisalCost.setText("$" + MyConstants.allMaterial.getItemType().get(3).get("AppraisalCost"));
                appraisalCost = Float.parseFloat(MyConstants.allMaterial.getItemType().get(3).get("AppraisalCost"));
            }

            getShippingMethod();

            btnPayNow.setClickable(true);
            btnPayNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btnPayNow.setClickable(false);
                    final Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                    // Use bounce interpolator with amplitude 0.2 and frequency 20
                    MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                    myAnim.setInterpolator(interpolator);

                    btnPayNow.startAnimation(myAnim);

                /*new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        ((HomeActivity) getActivity()).setIndex(6, TAG_ORDER_CONFIRM);
                        ((HomeActivity) getActivity()).replaceFragment(null);
                    }
                }, 1000);*/

                    addOrder();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    private void addOrder() {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
            btnPayNow.setClickable(true);
        } else {

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "order");
            map.put("header", "");
            map.put("token", App.user.getToken());
            map.put("user_id", App.user.getUserID());
            map.put("ItemTypeID", MyConstants.orderData.get("ItemId"));
            map.put("CustomerName", MyConstants.orderData.get("customerName"));
            map.put("ShippingTypeID", shippingMethodID);
            if (!TextUtils.isEmpty(MyConstants.orderData.get("comment"))) {
                map.put("CustomerComment", MyConstants.orderData.get("comment"));
            } else {
                map.put("CustomerComment", "");
            }

            if (MyConstants.orderData.get("ItemId").equalsIgnoreCase("4")) {
                map.put("webURL", MyConstants.orderData.get("webLink"));
            } else if (MyConstants.orderData.get("ItemId").equalsIgnoreCase("3")) {
                map.put("CaratDiamondWeight", MyConstants.orderData.get("caratWeight"));
                map.put("JewlaryStyleTypeID", MyConstants.orderData.get("jewelleryTypeId"));
                map.put("MetalType", MyConstants.orderData.get("metalType"));
                map.put("MaterialTypeID", MyConstants.orderData.get("gemMaterialId"));
                map.put("EstimatedValue", MyConstants.orderData.get("estimatedValue"));
                map.put("weight_of_item", MyConstants.orderData.get("weight_of_item"));

                if (MyConstants.orderData.get("jew_color_check").equalsIgnoreCase("0")) {
                    map.put("jew_color_start", MyConstants.orderData.get("jew_color_start"));
                    map.put("jew_color_end", MyConstants.orderData.get("jew_color_end"));
                } else {
                    map.put("jew_color_start", "0");
                    map.put("jew_color_end", "0");

                }
                map.put("jew_quality_none", MyConstants.orderData.get("jew_color_check"));

                if (MyConstants.orderData.get("jew_clearity_check").equalsIgnoreCase("0")) {
                    map.put("jew_clearity_start", MyConstants.orderData.get("jew_clearity_start"));
                    map.put("jew_clearity_end", MyConstants.orderData.get("jew_clearity_end"));
                } else {
                    map.put("jew_clearity_start", "0");
                    map.put("jew_clearity_end", "0");
                }
                map.put("jew_clerity_none", MyConstants.orderData.get("jew_clearity_check"));

                if (MyConstants.orderData.get("gemMaterial").equalsIgnoreCase("Other")) {
                    map.put("MaterialType", MyConstants.orderData.get("materialType"));
                }

                Log.d(TAG, "addOrder: " + map.toString());
                if (MyConstants.imagePath.size() > 0) {
                    for (int i = 0; i < MyConstants.imagePath.size(); i++) {
                        String key = "image[" + i + "]";
                        map.put(key, MyConstants.imagePath.get(0));
                    }
                }
            } else if (MyConstants.orderData.get("ItemId").equalsIgnoreCase("2")) {
                map.put("CaratDiamondWeight", MyConstants.orderData.get("caratWeight"));
                map.put("BrandName", MyConstants.orderData.get("brandName"));
                map.put("SerialNo", MyConstants.orderData.get("serialNumber"));
                map.put("dial", MyConstants.orderData.get("dial"));
                map.put("WindTypeID", MyConstants.orderData.get("windId"));
                map.put("ModelName", MyConstants.orderData.get("modelName"));
                map.put("caseSize", MyConstants.orderData.get("caseSize"));
                map.put("isAllPartOriginal", MyConstants.orderData.get("isAllPartOriginal"));
                map.put("MaterialTypeID", MyConstants.orderData.get("materialTypeId"));
                map.put("EstimatedValue", MyConstants.orderData.get("estimatedValue"));

                map.put("WatchMaterialTypeID", MyConstants.orderData.get("gemMaterialId"));

                if (MyConstants.orderData.get("gemMaterialOther").equalsIgnoreCase("Other")) {
                    map.put("GemMaterialOther",MyConstants.orderData.get("gemMaterial"));
                }else{
                    map.put("GemMaterialOther","None");
                }

                if (MyConstants.orderData.get("materialType").equalsIgnoreCase("Other")) {
                    map.put("MaterialType", MyConstants.orderData.get("materialType"));
                    map.put("metal_other", MyConstants.orderData.get("materialTypeOther"));
                } else {
                    map.put("MaterialType", MyConstants.orderData.get("materialType"));
                }

                if (MyConstants.imagePath.size() > 0) {
                    for (int i = 0; i < MyConstants.imagePath.size(); i++) {
                        String key = "image[" + i + "]";
                        map.put(key, MyConstants.imagePath.get(0));
                    }
                }
                map.put("issue_image[0]", MyConstants.orderData.get("issueImage"));
                map.put("selected_part", MyConstants.orderData.get("selected_part"));
                if (MyConstants.orderData.get("gemMaterial").equalsIgnoreCase("none")) {
                    map.put("watch_clearity_start", "0");
                    map.put("watch_clearity_end", "0");

                    map.put("watch_color_start", "0");
                    map.put("watch_color_end", "0");
                } else {
                    map.put("watch_clearity_start", MyConstants.orderData.get("clearity_start"));
                    map.put("watch_clearity_end", MyConstants.orderData.get("clearity_end"));

                    map.put("watch_color_start", MyConstants.orderData.get("color_start"));
                    map.put("watch_color_end", MyConstants.orderData.get("color_end"));
                }

            } else if (MyConstants.orderData.get("ItemId").equalsIgnoreCase("1")) {
                map.put("CaratDiamondWeight", MyConstants.orderData.get("caratWeight"));
                map.put("SettingCaratWeight", MyConstants.orderData.get("settingWeight"));
                map.put("EngagementOrLooseRing", MyConstants.orderData.get("isRing"));
                map.put("MetalType", MyConstants.orderData.get("metalType"));
                map.put("ShapeID", MyConstants.shapeId);
//                map.put("ClarityID", MyConstants.orderData.get("ClarityID"));
//                map.put("ColorID", MyConstants.orderData.get("ColorID"));
//                if (MyConstants.orderData.get("clearity_check").equalsIgnoreCase("0")) {
                map.put("clearity_start", MyConstants.orderData.get("clearity_start"));
                map.put("clearity_end", MyConstants.orderData.get("clearity_end"));
//                } else {
//                    map.put("clearity_start", "0");
//                    map.put("clearity_end", "0");
//                }
//                if (MyConstants.orderData.get("color_check").equalsIgnoreCase("0")) {
                map.put("color_start", MyConstants.orderData.get("color_start"));
                map.put("color_end", MyConstants.orderData.get("color_end"));
//                } else {
//                    map.put("color_start", "0");
//                    map.put("color_end", "0");
//                }

//                map.put("clerity_none", MyConstants.orderData.get("clearity_check"));
//                map.put("color_none", MyConstants.orderData.get("color_check"));


                map.put("MaterialTypeID", MyConstants.orderData.get("MaterialTypeID"));
                map.put("EstimatedValue", MyConstants.orderData.get("estimatedValue"));

                if (MyConstants.orderData.get("MaterialType").equalsIgnoreCase("Other")) {
                    map.put("MaterialType", MyConstants.orderData.get("materialType"));
                }

                if (MyConstants.imagePath.size() > 0) {
                    for (int i = 0; i < MyConstants.imagePath.size(); i++) {
                        String key = "image[" + i + "]";
                        map.put(key, MyConstants.imagePath.get(0));
                    }
                }
                map.put("issue_image[0]", MyConstants.orderData.get("issueImageTop"));
                map.put("issue_image[1]", MyConstants.orderData.get("issueImageSide"));
            }

            if (!TextUtils.isEmpty(MyConstants.orderData.get("color"))) {
                map.put("color",MyConstants.orderData.get("color"));
            }

            new CallRequest(PaymentFragment.this).addOrder(map);
        }
    }

    private void getShippingMethod() {

        if (!Internet.isAvailable(getActivity())) {
            //Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(getActivity()).create();

            // Setting Dialog Title
            alertDialog.setTitle("Error!");

            // Setting Dialog Message
            alertDialog.setMessage("Required Internet Connection");

            // Setting alert dialog icon
            //    alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

            // Setting OK Button
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    getShippingMethod();
                }
            });
            // Showing Alert Message
            alertDialog.show();
        } else {

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "shipping_method?");
            map.put("header", "");
            map.put("token", App.user.getToken());
            map.put("user_id", App.user.getUserID());

            new CallRequest(PaymentFragment.this).getShippingMethod(map);
        }

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case getShippingMethod:

                        shippingList.clear();
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {
                                //Utils.showToast(mainObj.getString("message"), LoginActivity.this);

                                JSONArray array = mainObj.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object = array.getJSONObject(i);

                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("ShippingMethodID", object.getString("ShippingMethodID"));
                                    map.put("ShippingMethod", object.getString("ShippingMethod"));
                                    map.put("ShippingPrice", object.getString("ShippingPrice"));
                                    Log.d(TAG, "onTaskCompleted: "+object.getString("ShippingMethodID"));
                                    shippingList.add(map);
                                }

                                rbPayment1.setText(shippingList.get(0).get("ShippingMethod"));
                                rbPayment2.setText(shippingList.get(1).get("ShippingMethod"));
                                rbPayment3.setText(shippingList.get(2).get("ShippingMethod"));

                                // for first time
                                tvShipping.setText("$" + shippingList.get(0).get("ShippingPrice"));
                                shipping = Float.parseFloat(shippingList.get(0).get("ShippingPrice"));

                                payableAmount = appraisalCost + shipping;
                                tvPayableAmount.setText("$" + df.format(payableAmount));

                                btnPayNow.setText("Pay Now");
                                //btnPayNow.setBackground(getResources().getDrawable(R.drawable.bg_button_green));

                                shippingMethodID = shippingList.get(0).get("ShippingMethodID");
                                shippingMethod = shippingList.get(0).get("ShippingMethod");
                                tvShipAppraisal.setText("Shipping charges $" + df.format(shipping) + " additional in order during payment");


                                rbPayment1.setChecked(true);

                                // radio group event
                                rgShipping.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                    @Override
                                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                                        RadioButton radioButton = group.findViewById(checkedId);

                                        if (radioButton == rbPayment1) {
                                            shippingMethodID = shippingList.get(0).get("ShippingMethodID");
                                            shippingMethod = shippingList.get(0).get("ShippingMethod");

                                            tvShipOvernight.setVisibility(View.VISIBLE);
                                            tvLocalPickupOnline.setVisibility(View.GONE);
                                            tvLocalPickup.setVisibility(View.GONE);

                                            tvShipping.setText("$" + shippingList.get(0).get("ShippingPrice"));
                                            shipping = Float.parseFloat(shippingList.get(0).get("ShippingPrice"));

                                            /// Shipping charges $XX.XX additional in order during payment


                                            tvShipOvernight.setText("Shipping charges $" + df.format(shipping) + " additional in order during payment");

                                            payableAmount = appraisalCost + shipping;
                                            tvPayableAmount.setText("$" + df.format(payableAmount));

                                            btnPayNow.setText("Pay Now");
                                            //btnPayNow.setBackground(getResources().getDrawable(R.drawable.bg_button));
                                        } else if (radioButton == rbPayment2) {
                                            shippingMethodID = shippingList.get(1).get("ShippingMethodID");
                                            shippingMethod = shippingList.get(1).get("ShippingMethod");

                                            tvShipOvernight.setVisibility(View.GONE);
                                            tvShipAppraisal.setVisibility(View.GONE);
                                            tvLocalPickupOnline.setVisibility(View.VISIBLE);
                                            tvLocalPickup.setVisibility(View.GONE);
                                            tvLocalPickupOnline.setText("Shipping charges $" + shippingList.get(1).get("ShippingPrice") + " additional in order during payment");

                                            tvShipping.setText("$" + shippingList.get(1).get("ShippingPrice"));
                                            shipping = Float.parseFloat(shippingList.get(1).get("ShippingPrice"));

                                            payableAmount = appraisalCost + shipping;
                                            tvPayableAmount.setText("$" + df.format(payableAmount));

                                            btnPayNow.setText("Pay Now");
                                            //btnPayNow.setBackground(getResources().getDrawable(R.drawable.bg_button));
                                        } else if (radioButton == rbPayment3) {
                                            shippingMethodID = shippingList.get(2).get("ShippingMethodID");
                                            shippingMethod = shippingList.get(2).get("ShippingMethod");

                                            tvShipAppraisal.setVisibility(View.GONE);
                                            tvLocalPickupOnline.setVisibility(View.GONE);
                                            tvLocalPickup.setVisibility(View.VISIBLE);
                                            tvShipOvernight.setVisibility(View.GONE);

                                            tvShipping.setText("$" + shippingList.get(2).get("ShippingPrice"));
                                            shipping = Float.parseFloat(shippingList.get(2).get("ShippingPrice"));

                                            payableAmount = appraisalCost + shipping;
                                            tvPayableAmount.setText("$" + df.format(payableAmount));
                                            tvLocalPickup.setText("Shipping charges $" + shippingList.get(2).get("ShippingPrice") + " additional in order during payment");
                                            btnPayNow.setText("Finish");
                                            //btnPayNow.setBackground(getResources().getDrawable(R.drawable.bg_button_green));
                                        } /*else if (radioButton == rbPayment4) {
                                            shippingMethodID = shippingList.get(3).get("ShippingMethodID");
                                            shippingMethod = shippingList.get(3).get("ShippingMethod");

                                            tvShipAppraisal.setVisibility(View.GONE);
                                            //tvLocalPickupOnline.setVisibility(View.GONE);
                                            //tvLocalPickup.setVisibility(View.GONE);
                                            tvShipOvernight.setVisibility(View.VISIBLE);

                                            tvShipping.setText("$" + shippingList.get(3).get("ShippingPrice"));
                                            shipping = Float.parseFloat(shippingList.get(3).get("ShippingPrice"));

                                            tvShipOvernight.setText("Shipping charges $" + df.format(shipping) + " additional in order during payment");

                                            payableAmount = appraisalCost + shipping;
                                            tvPayableAmount.setText("$" + df.format(payableAmount));

                                            btnPayNow.setText("Pay Now");
                                            //btnPayNow.setBackground(getResources().getDrawable(R.drawable.bg_button));
                                        }*/
                                    }
                                });
//                                rbPayment1.setChecked(true);
                                System.out.println("in API");

                            } else {
                                Utils.showToast(mainObj.getString("message"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case addOrder:
                        btnPayNow.setClickable(true);
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {
                                //Utils.showToast(mainObj.getString("message"), LoginActivity.this);
                                JSONObject object = mainObj.getJSONObject("data");

                                orderId = object.getString("order_id");

                                if (shippingMethodID.equalsIgnoreCase("3")) {
                                    setOrderPaymentStatus("Local_0", "approved");
                                } else {
                                    //doPayPalPayment();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("total", String.valueOf(payableAmount));
                                    bundle.putString("order_id", orderId);
                                    bundle.putString("appraisal_cost", String.valueOf(appraisalCost));
                                    bundle.putString("shipping", String.valueOf(shipping));
                                    bundle.putString("shipping_method", shippingMethod);
                                    ((HomeActivity) getActivity()).setIndex(15, TAG_CARDDETAIL);
                                    ((HomeActivity) getActivity()).replaceFragment(bundle);
                                }

                                // TODO
                                //setOrderPaymentStatus("123456789", "approved");

                            } else {
                                Utils.showToast(mainObj.getString("message"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case orderPayment:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {
                                //Utils.showToast(mainObj.getString("message"), LoginActivity.this);
                                JSONObject object = mainObj.getJSONObject("data");

                                App.certificateNo = object.getString("certificate_no");
                                System.out.println("certificte no:==" + App.certificateNo);
                                //orderId = object.getString("order_id");

                                MyConstants.paymentSuccess = true;
                            } else {
                                Utils.showToast(mainObj.getString("message"), getActivity());
                                MyConstants.paymentSuccess = false;
                                MyConstants.errorPayment = mainObj.getString("message");
                            }
                            MyConstants.totalAmount = String.valueOf(payableAmount);
                            MyConstants.orderType = String.valueOf(shippingMethod);

                            ((HomeActivity) getActivity()).setIndex(6, TAG_ORDER_CONFIRM);
                            ((HomeActivity) getActivity()).replaceFragment(null);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    private void doPayPalPayment() {
        Intent intent = new Intent(getActivity(), PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        getActivity().startService(intent);

        PayPalPayment thingToBuy = getStuffToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intentPay = new Intent(getActivity(), com.paypal.android.sdk.payments.PaymentActivity.class);

        intentPay.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intentPay.putExtra(com.paypal.android.sdk.payments.PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        startActivityForResult(intentPay, REQUEST_CODE_PAYMENT);

    }

    private PayPalPayment getStuffToBuy(String paymentIntent) {
        PayPalItem[] items;

        // TODO payableAmount instead of 0.01


        items = new PayPalItem[]{
                new PayPalItem("Appraisal charge", 1, new BigDecimal(payableAmount).setScale(2, BigDecimal.ROUND_HALF_UP), "USD",
                        PaymentID),
        };

        BigDecimal subtotal = PayPalItem.getItemTotal(items);
        Log.i("Payment", "Payment subtotal : " + subtotal.toString());
        BigDecimal shipping = new BigDecimal(0);
        Log.i("Payment", "Payment shipping : " + shipping.toString());
        BigDecimal tax = new BigDecimal(0);
        Log.i("Payment", "Payment tax : " + tax.toString());

        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
        BigDecimal amount = subtotal.add(shipping).add(tax);
        Log.i("Payment", "Payment : " + payableAmount);
        Log.i("Payment", "Payment : " + amount.toString());
        PayPalPayment payment;

        payment = new PayPalPayment(amount, "USD", "Appraisal charge", paymentIntent);
        payment.items(items).paymentDetails(paymentDetails);


        return payment;
    }

    @Override
    public void onDestroy() {
        // Stop service when done
        getActivity().stopService(new Intent(getActivity(), PayPalService.class));
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(com.paypal.android.sdk.payments.PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {


                       /* {
                            "response": {
                            "state": "approved",
                                    "id": "PAY-18X32451H0459092JKO7KFUI",
                                    "create_time": "2014-07-18T18:46:55Z",
                                    "intent": "sale"
                        },
                            "client": {
                            "platform": "Android",
                                    "paypal_sdk_version": "2.14.2",
                                    "product_name": "PayPal-Android-SDK",
                                    "environment": "mock"
                        },
                            "response_type": "payment"
                        }
                        06-02 05:21:30.830 com.markteq.wms I: {
                            "short_description": "sample item",
                                    "amount": "0.01",
                                    "intent": "sale",
                                    "currency_code": "USD"
                        }*/

                        JSONObject jObj = confirm.toJSONObject();
                        String payId = jObj.getJSONObject("response").getString("id");
                        String status = jObj.getJSONObject("response").getString("state");
                        Log.i(TAG, "Resutl1 : Confirm " + confirm.toJSONObject().toString(4));
                        Log.i(TAG, "Resutl1 : Payment " + confirm.getPayment().toJSONObject().toString(4));

                        setOrderPaymentStatus(payId, status);


                    } catch (JSONException e) {
                        Log.e(TAG, "An extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
                btnPayNow.setClickable(true);
            } else if (resultCode == com.paypal.android.sdk.payments.PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
                btnPayNow.setClickable(true);
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        //displayResultText("Future Payment code received from PayPal");

                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_PROFILE_SHARING) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("ProfileSharingExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("ProfileSharingExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        //displayResultText("Profile Sharing code received from PayPal");

                    } catch (JSONException e) {


                        Log.e("ProfileSharingExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("ProfileSharingExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "ProfileSharingExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    private void setOrderPaymentStatus(String payId, String status) {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
        } else {
            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "order_payment");
            map.put("header", "");
            map.put("token", App.user.getToken());
            map.put("user_id", App.user.getUserID());
            map.put("OrderID", orderId);
            map.put("TransactionNo", payId);
            map.put("FeeAmount", String.valueOf(appraisalCost));
            map.put("ShippingAmount", String.valueOf(shipping));
            map.put("Discount", "0");
            map.put("Total", String.valueOf(payableAmount));
            map.put("Status", status);

            new CallRequest(PaymentFragment.this).orderPayment(map);
        }
    }

    public void onFuturePaymentPressed(View pressed) {
        Intent intent = new Intent(getActivity(),
                PayPalFuturePaymentActivity.class);

        startActivityForResult(intent, REQUEST_CODE_FUTURE_PAYMENT);
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization) {

    }

    public void onFuturePaymentPurchasePressed(View pressed) {
        // Get the Application Correlation ID from the SDK
        String correlationId = PayPalConfiguration
                .getApplicationCorrelationId(getActivity());

        Log.i("FuturePaymentExample", "Application Correlation ID: "
                + correlationId);

        // TODO: Send correlationId and transaction details to your server for
        // processing with
        // PayPal...
        Toast.makeText(getActivity(),
                "App Correlation ID received from SDK", Toast.LENGTH_LONG)
                .show();
    }
}
