package com.federal.appraisal.fragment;


import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.federal.appraisal.BuildConfig;
import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.activity.TermsAndConditionsActivity;
import com.federal.appraisal.adapter.EngagementRingsAdapter;
import com.federal.appraisal.adapter.EngagementShapesAdapter;
import com.federal.appraisal.adapter.NothingSelectedSpinnerAdapter;
import com.federal.appraisal.animation.MyBounceInterpolator;
import com.federal.appraisal.custom_views.CustomCheckBoxView;
import com.federal.appraisal.custom_views.CustomTextView;
import com.federal.appraisal.materialrangebar.RangeBar;
import com.federal.appraisal.utils.VerticalSpacingDecoration;
import com.federal.appraisal.ws.MyConstants;
import com.federal.appraisal.ws.Utils;
import com.warkiz.widget.IndicatorSeekBar;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.federal.appraisal.activity.HomeActivity.TAG_ENGAGEMENT_RINGS;
import static com.federal.appraisal.activity.HomeActivity.TAG_STONE_AUTHENTIC_PARTS;

/**
 * A simple {@link Fragment} subclass.
 */
public class EngagementRingsFragment extends Fragment {

    private static final String TAG = "EngagementRingsFragment";

    private static int RESULT_LOAD_IMG = 1;
    private static int RESULT_CROP_DP = 3;
    RelativeLayout mClearityView, mColorView;
    IndicatorSeekBar seekBarMetal/*, seekBarClarityGrade, seekBarColor*/;
    RangeBar mClarityRangeBar, mColorRangeBar;
    RecyclerView rvImages, rvShapes;
    List<String> imagePath;
    LinearLayoutManager linearLayoutManager;
    GridLayoutManager gridLayoutManager;
    Button btnNext;
    String filePathDp = "";
    String filePathFirst = "";
    CardView cMoreView;
    EngagementRingsAdapter engagementRingsAdapter;
    EngagementShapesAdapter engagementShapesAdapter;
    RadioGroup rgStone;
    RadioButton rbRing, rbStone;
    LinearLayout llMetalType;
    Spinner spinnerGemMaterial;
    private String[] metalType, colorQ, clarityGrade, gemMaterial;
    CheckBox cbTerms;
    EditText etCustomerName, etCaratWeight, etEstimatedValue, etComment, etOtherMaterial, etSettingWeight, etMoreColorValue;
    String metalTypeStr, isRing = "1", gemMaterialId, gemMaterialStr;
    String clearity_start, clearity_end, color_start, color_end, clearity_start_name, clearity_end_name, color_start_name, color_end_name;
    public Uri selectedUri;
    public final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    TextView tvTerms;
    private CardView cvOtherMaterial;
    CustomTextView mDiamondColorCheck, mDiamondClarityCheck;

    public EngagementRingsFragment() {
        // Required empty public constructor
    }

    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,
                                                     int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        return bmp;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_engagement_rings, container, false);

        ((HomeActivity) getActivity()).setFragmentTitle(11);
        ((HomeActivity) getActivity()).setIndex(11, TAG_ENGAGEMENT_RINGS);

        imagePath = new ArrayList<>();

        seekBarMetal = view.findViewById(R.id.seekbarMetal);

        mClearityView = view.findViewById(R.id.mClearityView);
        mColorView = view.findViewById(R.id.mColorView);
        mClarityRangeBar = view.findViewById(R.id.mClarityRangeBar);
        mColorRangeBar = view.findViewById(R.id.mColorRangeBar);

        mDiamondColorCheck = view.findViewById(R.id.mDiamondColorCheck);
        mDiamondClarityCheck = view.findViewById(R.id.mDiamondClarityCheck);
        rvImages = view.findViewById(R.id.rvImages);
        rvShapes = view.findViewById(R.id.rvShapes);
        btnNext = view.findViewById(R.id.btnNext);
        rgStone = view.findViewById(R.id.rgStone);
        rbRing = view.findViewById(R.id.rbRing);
        rbStone = view.findViewById(R.id.rbStone);
        cMoreView = view.findViewById(R.id.cMoreView);
        etMoreColorValue = view.findViewById(R.id.etMoreColorValue);
        llMetalType = view.findViewById(R.id.llMetalType);
        spinnerGemMaterial = view.findViewById(R.id.spinnerGemMaterial);
        cbTerms = view.findViewById(R.id.cbTerms);
        etCustomerName = view.findViewById(R.id.etCustomerName);
        etCaratWeight = view.findViewById(R.id.etCaratWeight);
        etSettingWeight = view.findViewById(R.id.etSettingCrtWght);
        etEstimatedValue = view.findViewById(R.id.etEstimatedValue);
        etComment = view.findViewById(R.id.etComment);
        tvTerms = view.findViewById(R.id.tvTerms);
        cvOtherMaterial = view.findViewById(R.id.cvOtherMaterial);
        etOtherMaterial = view.findViewById(R.id.etOtherMaterial);

        metalType = getResources().getStringArray(R.array.metal_type);
        //clarityGrade = getResources().getStringArray(R.array.clarity_grade);
        //color = getResources().getStringArray(R.array.color);

        clarityGrade = new String[MyConstants.allMaterial.getClarity().size()];
        for (int i = 0; i < MyConstants.allMaterial.getClarity().size(); i++) {
            clarityGrade[i] = MyConstants.allMaterial.getClarity().get(i).get("ClarityName");
        }

        colorQ = new String[MyConstants.allMaterial.getColor().size()];
        for (int i = 0; i < MyConstants.allMaterial.getColor().size(); i++) {
            colorQ[i] = MyConstants.allMaterial.getColor().get(i).get("ColorName");
        }

        seekBarMetal.setTextArray(metalType);

//        seekBarClarityGrade.setTextArray(clarityGrade);
//        seekBarColor.setTextArray(color);

        metalTypeStr = metalType[0];
        seekBarMetal.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {

            }

            @Override
            public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {
                System.out.println("textBelowTick:::" + textBelowTick);
                metalTypeStr = textBelowTick;
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }
        });

//        mDiamondColorCheck.setOnCheckedChangeListener((compoundButton, b) -> {
//            mColorRangeBar.setEnabled(b);
//        });
//        mDiamondClarityCheck.setOnCheckedChangeListener((compoundButton, b) -> {
//            mClarityRangeBar.setEnabled(b);
//        });

//        mColorRangeBar.setLeft(1);
//        mColorRangeBar.setRight(colorQ.length-2);
        if (MyConstants.allMaterial.getClarity().size() > 1) {
            color_start = MyConstants.allMaterial.getColor().get(0).get("ColorID");
            color_end = MyConstants.allMaterial.getColor().get(colorQ.length - 1).get("ColorID");
            color_start_name = MyConstants.allMaterial.getColor().get(0).get("ColorName");
            color_end_name = MyConstants.allMaterial.getColor().get(colorQ.length - 1).get("ColorName");
            if (color_start.equalsIgnoreCase("11") || color_end.equalsIgnoreCase("11")) {
                cMoreView.setVisibility(View.VISIBLE);
            } else {
                cMoreView.setVisibility(View.GONE);
            }

        }

//        mColorRangeBar.setTickStart(1);
        mColorRangeBar.setTickEnd(MyConstants.allMaterial.getColor().size());
        mColorRangeBar.setTickTopLabels(colorQ);

//        mColorRangeBar.setLeft(1);
//        mColorRangeBar.setRight(colorQ.length-2);
        mColorRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                color_start = MyConstants.allMaterial.getColor().get(leftPinIndex).get("ColorID");
                color_end = MyConstants.allMaterial.getColor().get(rightPinIndex).get("ColorID");
                color_start_name = MyConstants.allMaterial.getColor().get(leftPinIndex).get("ColorName");
                color_end_name = MyConstants.allMaterial.getColor().get(rightPinIndex).get("ColorName");
                if (color_start.equalsIgnoreCase("11") || color_end.equalsIgnoreCase("11")) {
                    cMoreView.setVisibility(View.VISIBLE);
                } else {
                    cMoreView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTouchStarted(RangeBar rangeBar) {

            }

            @Override
            public void onTouchEnded(RangeBar rangeBar) {

            }
        });

        if (MyConstants.allMaterial.getClarity().size() > 0) {
            clearity_start = MyConstants.allMaterial.getClarity().get(0).get("ClarityID");
            clearity_end = MyConstants.allMaterial.getClarity().get(1).get("ClarityID");
            clearity_start_name = MyConstants.allMaterial.getClarity().get(0).get("ClarityName");
            clearity_end_name = MyConstants.allMaterial.getClarity().get(1).get("ClarityName");
        }
        mClarityRangeBar.setTickTopLabels(clarityGrade);
        mClarityRangeBar.setTickEnd(MyConstants.allMaterial.getClarity().size());

        mClarityRangeBar.setLeft(0);
        mClarityRangeBar.setRight(1);

        mClarityRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                clearity_start = MyConstants.allMaterial.getClarity().get(leftPinIndex).get("ClarityID");
                clearity_end = MyConstants.allMaterial.getClarity().get(rightPinIndex).get("ClarityID");

                clearity_start_name = MyConstants.allMaterial.getClarity().get(leftPinIndex).get("ClarityName");
                clearity_end_name = MyConstants.allMaterial.getClarity().get(rightPinIndex).get("ClarityName");
            }

            @Override
            public void onTouchStarted(RangeBar rangeBar) {

            }

            @Override
            public void onTouchEnded(RangeBar rangeBar) {

            }
        });


        if (BuildConfig.DEBUG) {
            etCustomerName.setText("test");
            etCaratWeight.setText("2");
            etSettingWeight.setText("2");
            etEstimatedValue.setText("2");
            cbTerms.setChecked(true);
        }

        tvTerms.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), TermsAndConditionsActivity.class);
            startActivity(intent);
        });

        gemMaterial = new String[MyConstants.allMaterial.getMaterialType().size()];
        for (int i = 0; i < MyConstants.allMaterial.getMaterialType().size(); i++) {
            gemMaterial[i] = MyConstants.allMaterial.getMaterialType().get(i).get("MaterialType");
        }

//        gemMaterial = new String[MyConstants.allMaterial.getMaterialType().size()];
//        for (int i = 0; i < MyConstants.allMaterial.getMaterialType().size(); i++) {
//            gemMaterial[i] = MyConstants.allMaterial.getMaterialType().get(i).get("MaterialType");
//        }

        ArrayAdapter aa = new ArrayAdapter<>(getContext(), R.layout.spinner_text, gemMaterial);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGemMaterial.setAdapter(new NothingSelectedSpinnerAdapter(aa, R.layout.nothing_selected_layout, getContext()));
        spinnerGemMaterial.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    gemMaterialId = MyConstants.allMaterial.getMaterialType().get(position - 1).get("MaterialTypeID");
                    gemMaterialStr = gemMaterial[position - 1];
                    if (gemMaterialStr.equalsIgnoreCase("Other")) {
                        mColorView.setVisibility(View.VISIBLE);
                        if (color_start.equalsIgnoreCase("11") || color_end.equalsIgnoreCase("11")) {
                            cMoreView.setVisibility(View.VISIBLE);
                        } else {
                            cMoreView.setVisibility(View.GONE);
                        }
                        mClearityView.setVisibility(View.VISIBLE);
                        cvOtherMaterial.setVisibility(View.VISIBLE);
                        etCaratWeight.setText("");
                        etCaratWeight.setEnabled(true);
                    } else if (gemMaterialStr.equalsIgnoreCase("None")) {
                        etCaratWeight.setText("0");
                        etCaratWeight.setEnabled(false);
                        cvOtherMaterial.setVisibility(View.GONE);
                        cMoreView.setVisibility(View.GONE);
                        mColorView.setVisibility(View.GONE);
                        mClearityView.setVisibility(View.GONE);
                    } else {
                        mColorView.setVisibility(View.VISIBLE);
                        mClearityView.setVisibility(View.VISIBLE);
                        if (color_start.equalsIgnoreCase("11") || color_end.equalsIgnoreCase("11")) {
                            cMoreView.setVisibility(View.VISIBLE);
                        } else {
                            cMoreView.setVisibility(View.GONE);
                        }
                        etCaratWeight.setText("");
                        etCaratWeight.setEnabled(true);
                        cvOtherMaterial.setVisibility(View.GONE);
                    }
                } else {
                    cMoreView.setVisibility(View.GONE);
                    mColorView.setVisibility(View.GONE);
                    mClearityView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
//                gemMaterialId = MyConstants.allMaterial.getMaterialType().get(0).get("MaterialTypeID");
//                gemMaterialStr = gemMaterial[0];
            }
        });

        //imagePath.clear();
        imagePath.add("by default");
        if (MyConstants.imagePath.size() > 0 && MyConstants.orderData.get("ItemId").equalsIgnoreCase("1")) {
            imagePath.addAll(MyConstants.imagePath);
        }
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvImages.setLayoutManager(linearLayoutManager);

        gridLayoutManager = new GridLayoutManager(getActivity(), 4, GridLayoutManager.VERTICAL, false);
        rvShapes.setLayoutManager(gridLayoutManager);

        engagementShapesAdapter = new EngagementShapesAdapter(getActivity(), MyConstants.allMaterial.getShape(), EngagementRingsFragment.this);
        rvShapes.setHasFixedSize(true);
        rvShapes.addItemDecoration(new VerticalSpacingDecoration(20));
        rvShapes.setItemViewCacheSize(20);
        rvShapes.setDrawingCacheEnabled(true);
        rvShapes.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvShapes.setAdapter(engagementShapesAdapter);

        engagementRingsAdapter = new EngagementRingsAdapter(getActivity(), imagePath, EngagementRingsFragment.this);
        rvImages.setHasFixedSize(true);
        rvImages.addItemDecoration(new VerticalSpacingDecoration(20));
        rvImages.setItemViewCacheSize(20);
        rvImages.setDrawingCacheEnabled(true);
        rvImages.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvImages.setAdapter(engagementRingsAdapter);

        rgStone.setOnCheckedChangeListener((radioGroup, i) -> {
            RadioButton checkedRadioButton = (RadioButton) radioGroup.findViewById(i);
            if (checkedRadioButton == rbRing) {
                isRing = "1";
                llMetalType.setVisibility(View.VISIBLE);
                etSettingWeight.setVisibility(View.VISIBLE);
            } else {
                isRing = "0";
                llMetalType.setVisibility(View.GONE);
                etSettingWeight.setVisibility(View.GONE);
                etSettingWeight.setText("");

            }
        });

        MyConstants.shapeId = MyConstants.allMaterial.getShape().get(0).get("ShapeID");
        MyConstants.shapePos = 0;

        btnNext.setClickable(true);
        btnNext.setOnClickListener(view1 -> {

            String customerName = etCustomerName.getText().toString().trim();
            String caratWeight = etCaratWeight.getText().toString().trim();
            String settingWeight = etSettingWeight.getText().toString().trim();
            String estimatedValue = etEstimatedValue.getText().toString().trim();
            String comment = etComment.getText().toString().trim();
            String otherMaterial = etOtherMaterial.getText().toString().trim();

           /* if (TextUtils.isEmpty(customerName)) {
                etCustomerName.requestFocus();
                etCustomerName.setError("Customer name is required");
            } else*/
            if (TextUtils.isEmpty(caratWeight)) {
                etCaratWeight.requestFocus();
                etCaratWeight.setError("Center Stone carat weight is required");
            } else if (TextUtils.isEmpty(settingWeight) && isRing.equalsIgnoreCase("1")) {
                etSettingWeight.requestFocus();
                etSettingWeight.setError("Setting Carat weight is required");
            } else if (TextUtils.isEmpty(gemMaterialId)) {
                Utils.showAlert("Please select GEM MATERIAL.", getActivity());
            } else if (gemMaterialStr.equalsIgnoreCase("Other") && TextUtils.isEmpty(otherMaterial)) {
                etOtherMaterial.requestFocus();
                etOtherMaterial.setError("Other material is required");
            } else if (cMoreView.getVisibility() == View.VISIBLE && TextUtils.isEmpty(etMoreColorValue.getText().toString().trim())) {
                etMoreColorValue.requestFocus();
                etMoreColorValue.setError("Please enter color info.");
            } else if (TextUtils.isEmpty(estimatedValue)) {
                etEstimatedValue.requestFocus();
                etEstimatedValue.setError("Estimated value is required");
            } else if (imagePath.size() < 2) {
                Utils.showAlert("Select at least one image", getActivity());
            } else if (!cbTerms.isChecked()) {
                Utils.showAlert("You must agree to the terms and conditions before submitting!", getActivity());
            } else {

                btnNext.setClickable(false);

                final Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);

                // Use bounce interpolator with amplitude 0.2 and frequency 20
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                btnNext.startAnimation(myAnim);

                MyConstants.orderData.clear();
                MyConstants.orderData.put("ItemId", "1");
                MyConstants.orderData.put("customerName", customerName);
                MyConstants.orderData.put("caratWeight", caratWeight);
                MyConstants.orderData.put("settingWeight", settingWeight);
                MyConstants.orderData.put("estimatedValue", estimatedValue);
                MyConstants.orderData.put("isRing", isRing);
                MyConstants.orderData.put("ShapeID", MyConstants.shapeId);
                MyConstants.orderData.put("Shape", MyConstants.allMaterial.getShape().get(MyConstants.shapePos).get("ShapeName"));

                if (gemMaterialStr.equalsIgnoreCase("none")) {
                    MyConstants.orderData.put("clearity_start", "0");
                    MyConstants.orderData.put("clearity_end", "0");
                    MyConstants.orderData.put("color_start", "0");
                    MyConstants.orderData.put("color_end", "0");

                    MyConstants.orderData.put("clearity_start_name", "N/A");
                    MyConstants.orderData.put("clearity_end_name", "N/A");
                    MyConstants.orderData.put("color_start_name", "N/A");
                    MyConstants.orderData.put("color_end_name", "N/A");


                } else {

                    MyConstants.orderData.put("clearity_start", clearity_start);
                    MyConstants.orderData.put("clearity_end", clearity_end);
                    MyConstants.orderData.put("color_start", color_start);
                    MyConstants.orderData.put("color_end", color_end);


                    MyConstants.orderData.put("clearity_start_name", clearity_start_name);
                    MyConstants.orderData.put("clearity_end_name", clearity_end_name);
                    MyConstants.orderData.put("color_start_name", color_start_name);
                    MyConstants.orderData.put("color_end_name", color_end_name);


                }

                if (cMoreView.getVisibility() == View.VISIBLE && !TextUtils.isEmpty(etMoreColorValue.getText().toString().trim()))
                    MyConstants.orderData.put("color", etMoreColorValue.getText().toString().trim());
                else
                    MyConstants.orderData.put("color", "");
                MyConstants.orderData.put("clearity_check", "0");
                MyConstants.orderData.put("color_check", "0");

                if (isRing.equalsIgnoreCase("1")) {
                    MyConstants.orderData.put("metalType", metalTypeStr);
                } else {
                    MyConstants.orderData.put("metalType", "");
                }
                MyConstants.orderData.put("MaterialTypeID", gemMaterialId);
                MyConstants.orderData.put("MaterialType", gemMaterialStr);
                MyConstants.orderData.put("materialType", otherMaterial);
                MyConstants.orderData.put("imagesCount", String.valueOf(imagePath.size() - 1));
                MyConstants.orderData.put("comment", comment);

                MyConstants.imagePath.clear();
                MyConstants.imagePath = imagePath;
                MyConstants.imagePath.remove(0);

                etCustomerName.setText("");
                etCaratWeight.setText("");
                etSettingWeight.setText("");
                etEstimatedValue.setText("");
                etComment.setText("");


                ((HomeActivity) getActivity()).setIndex(12, TAG_STONE_AUTHENTIC_PARTS);
                ((HomeActivity) getActivity()).replaceFragment(null);
            }
        });
        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    public void selectImage() {
        try {
            final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
            //final CharSequence[] options = {"Take Photo"};
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
            builder.setTitle("Select Option");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        dialog.dismiss();

                        File f = new File(Environment.getExternalStorageDirectory() + "/Federal_Appraisal/Images");
                        if (!f.exists()) {
                            f.mkdirs();
                        }
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File file = new File(Environment.getExternalStorageDirectory(), "/Federal_Appraisal/Images/Image_" + System.currentTimeMillis() + ".jpeg");
                        selectedUri = Uri.fromFile(file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                        startActivityForResult(intent, PICK_IMAGE_CAMERA);

                    }
                    if (options[item].equals("Choose From Gallery")) {
                        dialog.dismiss();
                        Intent i = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, PICK_IMAGE_GALLERY);
                    }
                }
            });
            builder.show();

        } catch (Exception e) {
            Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PICK_IMAGE_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //openCameraIntent();
                    selectImage();
                } else {
                    Toast.makeText(getActivity(), "The app was not allowed to write in your storage", Toast.LENGTH_LONG).show();
                }
            }
            case PICK_IMAGE_GALLERY: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //openCameraIntent();
                    selectImage();
                } else {
                    Toast.makeText(getActivity(), "The app was not allowed to write in your storage", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public String getPath(Uri uri) {
        File myFile = new File(uri.getPath());
        myFile.getAbsolutePath();
        return myFile.getAbsolutePath();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_CAMERA && resultCode == RESULT_OK) {
            filePathFirst = getPath(selectedUri);
            System.out.println("image path::::" + filePathFirst);
            System.out.println("uri:::" + selectedUri);
            try {
                imagePath.add(filePathFirst);
                engagementRingsAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == PICK_IMAGE_GALLERY && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            filePathFirst = cursor.getString(columnIndex);
            Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePathFirst, 100, 100);
            imagePath.add(filePathFirst);
            // ivDoc.setImageBitmap(BitmapFactory.decodeFile(imgPath));
            engagementRingsAdapter.notifyDataSetChanged();

            for (int i = 0; i < imagePath.size(); i++) {
                System.out.println("image list::" + imagePath.get(i));
            }
            cursor.close();
        }
    }


}
