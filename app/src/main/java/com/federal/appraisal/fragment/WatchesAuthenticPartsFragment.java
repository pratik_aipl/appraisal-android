package com.federal.appraisal.fragment;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.animation.MyBounceInterpolator;
import com.federal.appraisal.ws.MyConstants;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.federal.appraisal.activity.HomeActivity.TAG_ORDER_DETAIL;
import static com.federal.appraisal.activity.HomeActivity.TAG_WATCHES_AUTHENTIC_PARTS;

/**
 * A simple {@link Fragment} subclass.
 */
public class WatchesAuthenticPartsFragment extends Fragment {

    ImageView ivBand, ivDial, ivBezel, ivWatch;
    String band = "", bezel = "", dial = "";
    String Band = "", Bezel = "", Dial = "";
    int image = R.drawable.watch_full;
    Button btnNext;
    String TAG = "IMAGE::", imagePath = "";

    public WatchesAuthenticPartsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_watches_authentic_parts, container, false);

        try {
            ((HomeActivity) getActivity()).setFragmentTitle(9);
            ((HomeActivity) getActivity()).setIndex(9, TAG_WATCHES_AUTHENTIC_PARTS);

            ivBand = view.findViewById(R.id.ivBand);
            ivBezel = view.findViewById(R.id.ivBezel);
            ivDial = view.findViewById(R.id.ivDial);
            ivWatch = view.findViewById(R.id.ivWatch);
            btnNext = view.findViewById(R.id.btnNext);

            ivBand.setTag(1);
            ivBand.setOnClickListener(view13 -> {
                int pos = (int) ivBand.getTag();
                if (pos == 1) {
                    ivBand.setImageResource(R.drawable.band);
                    ivBand.setTag(2);
                    band = "band";
                    Band = "1";
                } else {
                    ivBand.setImageResource(R.drawable.band_gray);
                    ivBand.setTag(1);
                    band = "";
                    Band = "";
                }

                setImage();

            });

            ivBezel.setTag(1);
            ivBezel.setOnClickListener(view12 -> {
                int pos = (int) ivBezel.getTag();
                if (pos == 1) {
                    ivBezel.setImageResource(R.drawable.besel);
                    ivBezel.setTag(2);
                    bezel = "bezel";
                    Bezel = "3";
                } else {
                    ivBezel.setImageResource(R.drawable.besel_gray);
                    ivBezel.setTag(1);
                    bezel = "";
                    Bezel = "";

                }

                setImage();
            });

            ivDial.setTag(1);
            ivDial.setOnClickListener(view1 -> {
                int pos = (int) ivDial.getTag();
                if (pos == 1) {
                    ivDial.setImageResource(R.drawable.dial);
                    ivDial.setTag(2);
                    dial = "dial";
                    Dial = "2";
                } else {
                    ivDial.setImageResource(R.drawable.dial_gray);
                    ivDial.setTag(1);
                    dial = "";
                    Dial = "";

                }
                setImage();
            });

            btnNext.setClickable(true);
            btnNext.setOnClickListener(v -> {

                Bitmap bm = BitmapFactory.decodeResource(getResources(), image);
                storeImage(bm);

                MyConstants.orderData.put("issueImage", imagePath);
                MyConstants.orderData.put("selected_part", String.valueOf(Band + Dial + Bezel));

                btnNext.setClickable(false);

                final Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);

                // Use bounce interpolator with amplitude 0.2 and frequency 20
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                btnNext.startAnimation(myAnim);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        ((HomeActivity) getActivity()).setIndex(4, TAG_ORDER_DETAIL);
                        ((HomeActivity) getActivity()).replaceFragment(null);
                    }
                }, 1000);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    private void setImage() {
        if (TextUtils.isEmpty(band)) {
            // band is not selected
            if (TextUtils.isEmpty(bezel)) {
                // bezel is not selected
                if (TextUtils.isEmpty(dial)) {
                    // dial is not selected
                    ivWatch.setImageResource(R.drawable.watch_full);
                    image = R.drawable.watch_full;
                } else {
                    // dial is selected
                    ivWatch.setImageResource(R.drawable.dial_watch);
                    image = R.drawable.dial_watch;
                }
            } else {
                // bezel is selected
                if (TextUtils.isEmpty(dial)) {
                    // dial is not selected
                    ivWatch.setImageResource(R.drawable.bezel_watch);
                    image = R.drawable.bezel_watch;
                } else {
                    // dial is selected
                    ivWatch.setImageResource(R.drawable.bezel_dial_watch);
                    image = R.drawable.bezel_dial_watch;
                }
            }
        } else {
            // band is selected
            if (TextUtils.isEmpty(bezel)) {
                // bezel is not selected
                if (TextUtils.isEmpty(dial)) {
                    // dial is not selected
                    ivWatch.setImageResource(R.drawable.band_watch);
                    image = R.drawable.band_watch;
                } else {
                    // dial is selected
                    ivWatch.setImageResource(R.drawable.band_dial_watch);
                    image = R.drawable.band_dial_watch;
                }
            } else {
                // bezel is selected
                if (TextUtils.isEmpty(dial)) {
                    // dial is not selected
                    ivWatch.setImageResource(R.drawable.band_bezel_watch);
                    image = R.drawable.band_bezel_watch;
                } else {
                    // dial is selected
                    ivWatch.setImageResource(R.drawable.band_bezel_dial_watch);
                    image = R.drawable.band_bezel_dial_watch;
                }
            }
        }
    }

    private void storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d(TAG,
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();

            imagePath = pictureFile.getAbsolutePath();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }

    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath() + "/Federal_Appraisal/Images");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmssSSS").format(new Date());
        File mediaFile;
        String mImageName = "Watch_" + timeStamp + ".png";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

}
