package com.federal.appraisal.fragment;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.animation.MyBounceInterpolator;
import com.federal.appraisal.others.PicassoTrustAll;
import com.federal.appraisal.ws.MyConstants;
import com.squareup.picasso.Callback;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.federal.appraisal.activity.HomeActivity.TAG_ORDER_DETAIL;
import static com.federal.appraisal.activity.HomeActivity.TAG_STONE_AUTHENTIC_PARTS;

/**
 * A simple {@link Fragment} subclass.tvSkip
 */
public class StonesAuthenticPartsFragment extends Fragment {

    ImageView ivStoneTop, ivStoneSide;
    float touchY;
    float touchX;
    TextView tvReset;
    Button btnSubmit, tvSkip;
    boolean isSkip = false;
    String TAG = "IMAGE::", imagePathTop = "", imagePathSide = "";
    Bitmap bitmapStoneTop, bitmapStoneSide;

    public StonesAuthenticPartsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_stones_authentic_parts, container, false);

        try {
            ((HomeActivity) getActivity()).setFragmentTitle(12);
            ((HomeActivity) getActivity()).setIndex(12, TAG_STONE_AUTHENTIC_PARTS);

            ivStoneTop = view.findViewById(R.id.ivStoneTop);
            ivStoneSide = view.findViewById(R.id.ivStoneSide);
            tvSkip = view.findViewById(R.id.tvSkip);
            tvReset = view.findViewById(R.id.tvReset);
            btnSubmit = view.findViewById(R.id.btnSubmit);

            final String imagePathTop1 = MyConstants.allMaterial.getShape().get(MyConstants.shapePos).get("FirstImage");
            final String imagePathSide1 = MyConstants.allMaterial.getShape().get(MyConstants.shapePos).get("SecondImage");
            PicassoTrustAll.getInstance(getActivity())
                    .load(imagePathTop1)
                    .placeholder(R.drawable.no_img)
                    .error(R.drawable.no_img)
                    .into(ivStoneTop, new Callback() {
                        @Override
                        public void onSuccess() {
                            bitmapStoneTop = ((BitmapDrawable) ivStoneTop.getDrawable()).getBitmap().copy(Bitmap.Config.ARGB_8888, true);
                        }

                        @Override
                        public void onError() {
                        }
                    });
            PicassoTrustAll.getInstance(getActivity())
                    .load(imagePathSide1)
                    .placeholder(R.drawable.no_img)
                    .error(R.drawable.no_img)
                    .into(ivStoneSide, new Callback() {
                        @Override
                        public void onSuccess() {
                            bitmapStoneSide = ((BitmapDrawable) ivStoneSide.getDrawable()).getBitmap().copy(Bitmap.Config.ARGB_8888, true);
                        }

                        @Override
                        public void onError() {
                        }
                    });

            ivStoneTop.setOnTouchListener((v, event) -> {
                tvReset.setVisibility(View.VISIBLE);
                int touchX = (int) (event.getX());
                int touchY = (int) (event.getY());
                if (bitmapStoneTop != null)
                    drawOnProjectedBitMap(ivStoneTop, bitmapStoneTop, touchX, touchY);
                return true;
            });

            ivStoneSide.setOnTouchListener((v, event) -> {
                tvReset.setVisibility(View.VISIBLE);
                int touchX = (int) (event.getX());
                int touchY = (int) (event.getY());
                if (bitmapStoneTop != null)
                    drawOnProjectedBitMap(ivStoneSide, bitmapStoneSide, touchX, touchY);
                return true;
            });

            tvSkip.setOnClickListener(v -> {
                isSkip = true;
                tvReset.performClick();
                btnSubmit.performClick();
            });

            tvReset.setOnClickListener(v -> {
                try {
                    PicassoTrustAll.getInstance(getActivity())
                            .load(imagePathTop1)
                            .placeholder(R.drawable.no_img)
                            .error(R.drawable.no_img)
                            .into(ivStoneTop, new Callback() {
                                @Override
                                public void onSuccess() {
                                    bitmapStoneTop = ((BitmapDrawable) ivStoneTop.getDrawable()).getBitmap().copy(Bitmap.Config.ARGB_8888, true);
                                }

                                @Override
                                public void onError() {
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    PicassoTrustAll.getInstance(getActivity())
                            .load(imagePathSide1)
                            .placeholder(R.drawable.no_img)
                            .error(R.drawable.no_img)
                            .into(ivStoneSide, new Callback() {
                                @Override
                                public void onSuccess() {
                                    bitmapStoneSide = ((BitmapDrawable) ivStoneSide.getDrawable()).getBitmap().copy(Bitmap.Config.ARGB_8888, true);
                                }

                                @Override
                                public void onError() {
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }

                tvReset.setVisibility(View.GONE);
            });

            btnSubmit.setEnabled(true);
            btnSubmit.setOnClickListener(v -> {
                btnSubmit.setClickable(false);
                MyConstants.orderData.put("issueImageTop", storeImage(((BitmapDrawable) ivStoneTop.getDrawable()).getBitmap(), "Top"));
                MyConstants.orderData.put("issueImageSide", storeImage(((BitmapDrawable) ivStoneSide.getDrawable()).getBitmap(), "Side"));

                Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                if (isSkip)
                    tvSkip.startAnimation(myAnim);
                else
                    btnSubmit.startAnimation(myAnim);

                isSkip=false;

                new Handler().postDelayed(() -> {
                    ((HomeActivity) getActivity()).setIndex(4, TAG_ORDER_DETAIL);
                    ((HomeActivity) getActivity()).replaceFragment(null);
                }, 1000);

            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    private String storeImage(Bitmap image, String type) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d(TAG, "Error creating media file, check storage permissions: ");// e.getMessage());
            return "";
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.JPEG, 90, fos);
            fos.close();
            if (type.equalsIgnoreCase("Top")) {
                imagePathTop = pictureFile.getAbsolutePath();
            } else {
                imagePathSide = pictureFile.getAbsolutePath();
            }
        } catch (Exception e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
        return type.equalsIgnoreCase("Top") ? imagePathTop : imagePathSide;

    }

    private static Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    public static Bitmap resizeBitmapImageForFitSquare(Bitmap image, int maxResolution) {

        if (maxResolution <= 0)
            return image;

        int width = image.getWidth();
        int height = image.getHeight();
        float ratio = (width >= height) ? (float) maxResolution / width : (float) maxResolution / height;

        int finalWidth = (int) ((float) width * ratio);
        int finalHeight = (int) ((float) height * ratio);

        image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);

        if (image.getWidth() == image.getHeight())
            return image;
        else {
            //fit height and width
            int left = 0;
            int top = 0;

            if (image.getWidth() != maxResolution)
                left = (maxResolution - image.getWidth()) / 2;

            if (image.getHeight() != maxResolution)
                top = (maxResolution - image.getHeight()) / 2;

            Bitmap bitmap = Bitmap.createBitmap(maxResolution, maxResolution, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            canvas.drawBitmap(image, left, top, null);
            canvas.save();
            canvas.restore();

            return bitmap;
        }
    }

    private File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath() + "/Federal_Appraisal/Images");
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmssSSS").format(new Date());
        File mediaFile;
        String mImageName = "Stone_" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }


    private void drawOnProjectedBitMap(ImageView iv, Bitmap bm, int x, int y) {
        if (x < 0 || y < 0 || x > iv.getWidth() || y > iv.getHeight()) {
            //outside ImageView
            return;
        } else {
            int projectedX = (int) ((double) x * ((double) bm.getWidth() / (double) iv.getWidth()));
            int projectedY = (int) ((double) y * ((double) bm.getHeight() / (double) iv.getHeight()));

            Paint paint = new Paint();
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(Color.GREEN);
            paint.setStrokeWidth(3);
            Canvas canvas = new Canvas(bm);
            canvas.drawCircle(projectedX, projectedY, 20, paint);
            iv.setImageBitmap(bm);
            iv.invalidate();

            System.out.println(x + ":" + y + "/" + iv.getWidth() + " : " + iv.getHeight() + "\n" + projectedX + " : " + projectedY + "/" + bm.getWidth() + " : " + bm.getHeight());
        }
    }
}
