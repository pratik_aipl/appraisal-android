package com.federal.appraisal.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.animation.MyBounceInterpolator;
import com.federal.appraisal.custom_views.CustomTextView;
import com.federal.appraisal.others.Internet;
import com.federal.appraisal.ws.MyConstants;

import java.util.Calendar;

import static com.federal.appraisal.activity.HomeActivity.TAG_ENGAGEMENT_RINGS;
import static com.federal.appraisal.activity.HomeActivity.TAG_HOME;
import static com.federal.appraisal.activity.HomeActivity.TAG_JEWELLERY;
import static com.federal.appraisal.activity.HomeActivity.TAG_WATCHES;
import static com.federal.appraisal.activity.HomeActivity.TAG_WEBLINKS;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    CardView cvStones, cvWatches, cvJewellery, cvWebLinks;
    TextView mCall;
    CustomTextView mYear;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        try {

            ((HomeActivity) getActivity()).setFragmentTitle(0);
            ((HomeActivity) getActivity()).setIndex(0, TAG_HOME);

            mYear = view.findViewById(R.id.mYear);

            int year = Calendar.getInstance().get(Calendar.YEAR);
            mYear.setText(getString(R.string.copyright_da, year));


            mCall = view.findViewById(R.id.mCall);
            cvStones = view.findViewById(R.id.cvStones);
            cvWatches = view.findViewById(R.id.cvWatches);
            cvJewellery = view.findViewById(R.id.cvJewellery);
            cvWebLinks = view.findViewById(R.id.cvWebLinks);

            cvWatches.setClickable(true);
            cvWebLinks.setClickable(true);
            cvJewellery.setClickable(true);
            cvStones.setClickable(true);

            MyConstants.orderData.clear();
            MyConstants.imagePath.clear();

            mCall.setOnClickListener(view1 -> {
                Uri u = Uri.parse("tel:6468087797");
                // Create the intent and set the data for the
                // intent as the phone number.
                Intent i = new Intent(Intent.ACTION_DIAL, u);
                startActivity(i);
            });

            cvStones.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   if (!Internet.isAvailable(getActivity())) {
                        Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
                    } else {
                        cvStones.setClickable(false);
                        final Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                        // Use bounce interpolator with amplitude 0.2 and frequency 20
                        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                        myAnim.setInterpolator(interpolator);
                        cvStones.startAnimation(myAnim);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                ((HomeActivity) getActivity()).setIndex(11, TAG_ENGAGEMENT_RINGS);
                                ((HomeActivity) getActivity()).replaceFragment(null);
                            }
                        }, 1000);

                    }
                }
            });

            cvJewellery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!Internet.isAvailable(getActivity())) {
                        Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
                    } else {
                        cvJewellery.setClickable(false);

                        final Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);

                        // Use bounce interpolator with amplitude 0.2 and frequency 20
                        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                        myAnim.setInterpolator(interpolator);

                        cvJewellery.startAnimation(myAnim);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                ((HomeActivity) getActivity()).setIndex(10, TAG_JEWELLERY);
                                ((HomeActivity) getActivity()).replaceFragment(null);
                            }
                        }, 1000);

                    }
                }
            });

            cvWatches.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!Internet.isAvailable(getActivity())) {
                        Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
                    } else {
                        cvWatches.setClickable(false);

                        final Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);

                        // Use bounce interpolator with amplitude 0.2 and frequency 20
                        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                        myAnim.setInterpolator(interpolator);

                        cvWatches.startAnimation(myAnim);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                ((HomeActivity) getActivity()).setIndex(8, TAG_WATCHES);
                                ((HomeActivity) getActivity()).replaceFragment(null);
                            }
                        }, 1000);
                    }

                }
            });

            cvWebLinks.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!Internet.isAvailable(getActivity())) {
                        Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
                    } else {
                        cvWebLinks.setClickable(false);

                        final Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);

                        // Use bounce interpolator with amplitude 0.2 and frequency 20
                        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                        myAnim.setInterpolator(interpolator);

                        cvWebLinks.startAnimation(myAnim);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                ((HomeActivity) getActivity()).setIndex(3, TAG_WEBLINKS);
                                ((HomeActivity) getActivity()).replaceFragment(null);
                            }
                        }, 1000);

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

}
