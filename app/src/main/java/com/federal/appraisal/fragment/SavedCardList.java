package com.federal.appraisal.fragment;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.federal.appraisal.BuildConfig;
import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.activity.RemoveCard;
import com.federal.appraisal.activity.SavedCardpay;
import com.federal.appraisal.adapter.SavedCardAdapter;
import com.federal.appraisal.model.SavedCardListModel;
import com.federal.appraisal.others.App;
import com.federal.appraisal.others.Internet;
import com.federal.appraisal.ws.AsyncTaskListner;
import com.federal.appraisal.ws.CallRequest;
import com.federal.appraisal.ws.Constant;
import com.federal.appraisal.ws.MyConstants;
import com.federal.appraisal.ws.Utils;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Charge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.federal.appraisal.activity.HomeActivity.TAG_ORDER_CONFIRM;

public class SavedCardList extends Fragment implements AsyncTaskListner, SavedCardpay, RemoveCard {

    private static final String TAG = "SavedCardList";
    RecyclerView rvSavedList;
    List<SavedCardListModel> savedCardListModels = new ArrayList<>();
    float total;
    String charge_id, status, orderId, appraisalCost, shipping, shippingMethod;
    SavedCardAdapter savedCardAdapter;

    int selPos = -1;
    String userStripreID;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_savedlist, container, false);

        Bundle bundle = getArguments();
        if (bundle != null) {
            orderId = bundle.getString("order_id");
            total = Float.parseFloat(bundle.getString("total"));
            appraisalCost = bundle.getString("appraisal_cost");
            shipping = bundle.getString("shipping");
            shippingMethod = bundle.getString("shipping_method");
        }
        rvSavedList = view.findViewById(R.id.rv_savedlist);

        rvSavedList.setHasFixedSize(true);
        rvSavedList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvSavedList.setItemAnimator(new DefaultItemAnimator());

        savedCardAdapter = new SavedCardAdapter(getActivity(), savedCardListModels, this, this);

        rvSavedList.setAdapter(savedCardAdapter);

        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
        } else {
            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "user_card_list?");
            map.put("header", "");
            map.put("token", App.user.getToken());
            map.put("user_id", App.user.getUserID());
            new CallRequest(SavedCardList.this).getSavedCard(map);
        }
        return view;
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {

                    case getSavedCard:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {

                                JSONArray mainarray = mainObj.getJSONArray("data");

                                savedCardListModels.clear();
                                for (int i = 0; i < mainarray.length(); i++) {

                                    SavedCardListModel savedCardListModel = new SavedCardListModel();
                                    JSONObject data = mainarray.getJSONObject(i);
                                    savedCardListModel.setUserStripreID(data.getString("UserStripreID"));
                                    savedCardListModel.setLastfourdigit(data.getString("lastfourdigit"));
                                    savedCardListModel.setCardHolderName(data.getString("CardHolderName"));
                                    savedCardListModel.setExp_month(data.getString("exp_month"));
                                    savedCardListModel.setExp_year(data.getString("exp_year"));
                                    savedCardListModel.setBrand(data.getString("brand"));
                                    savedCardListModel.setCustomerID(data.getString("CustomerID"));
                                    savedCardListModels.add(savedCardListModel);
                                }

                            } else {
                                Utils.showToast(mainObj.getString("message"), getActivity());

                            }
                            savedCardAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case orderPayment:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {
                                //Utils.showToast(mainObj.getString("message"), LoginActivity.this);
                                JSONObject object = mainObj.getJSONObject("data");

                                App.certificateNo = object.getString("certificate_no");
                                System.out.println("certificte no:==" + App.certificateNo);
                                //orderId = object.getString("order_id");

                                MyConstants.paymentSuccess = true;
                            } else {
                                Utils.showToast(mainObj.getString("message"), getActivity());
                                MyConstants.paymentSuccess = false;
                                MyConstants.errorPayment = mainObj.getString("message");
                            }
                            MyConstants.totalAmount = String.valueOf(total);
                            MyConstants.orderType = String.valueOf(shippingMethod);

                            ((HomeActivity) getActivity()).setIndex(6, TAG_ORDER_CONFIRM);
                            ((HomeActivity) getActivity()).replaceFragment(null);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case removeSavedCard:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {
                                Utils.showToast(mainObj.getString("message"), getActivity());
                                //JSONObject object = mainObj.getJSONObject("data");

                                if (selPos != -1)
                                    savedCardListModels.remove(selPos);
                                savedCardAdapter.notifyDataSetChanged();
                                selPos = -1;
                            } else {
                                Utils.showToast(mainObj.getString("message"), getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onKeyPay(String CustId, String userStripreID) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Alert!")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("Are you sure you want pay using this card?")
                .setPositiveButton("Yes", (dialog, which) -> {
                    dialog.dismiss();
                    new CreateChargeAndDoPayment().execute(CustId, userStripreID);
                })
                .setNegativeButton("No", (dialog, which) -> dialog.dismiss())
                .show();

    }

    @Override
    public void onKeyRemove(String StripId, int position) {
        new AlertDialog.Builder(getActivity()).setTitle("Alert!")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("Are you sure to Remove this card?")
                .setPositiveButton("Yes", (dialog, which) -> {
                    dialog.dismiss();
                    selPos = position;
                    if (!Internet.isAvailable(getActivity())) {
                        Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
                    } else {
                        Map<String, String> map = new HashMap<String, String>();
                        map.put("url", BuildConfig.BASE_URL + "delete_user_card");
                        map.put("header", "");
                        map.put("token", App.user.getToken());
                        map.put("user_id", App.user.getUserID());
                        map.put("UserStripreID", StripId);

                        new CallRequest(SavedCardList.this).removeSavedCard(map);
                    }
                }).setNegativeButton("No", (dialog, which) -> {
            dialog.dismiss();
            selPos = -1;
        }).show();
    }


    @SuppressLint("StaticFieldLeak")
    private class CreateChargeAndDoPayment extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.showProgressDialog(getActivity());
        }

        @Override
        protected Void doInBackground(String... strings) {
            String customerId = strings[0];
            userStripreID = strings[1];
            Log.d(TAG, "doInBackground: " + customerId);
            com.stripe.Stripe.apiKey = BuildConfig.STRIPE_SECRET_KEY;
            Number num = 0;
            try {
                num = NumberFormat.getInstance().parse(String.valueOf(total));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            int grand_total1 = 0;
            if (num != null) {
                grand_total1 = num.intValue() * 100;
            }
            Map<String, Object> chargeParams = new HashMap<String, Object>();
            chargeParams.put("amount", grand_total1);
            chargeParams.put("description", "Appraisal");
            chargeParams.put("currency", "usd");
            chargeParams.put("customer", customerId);
            System.out.println("amount:::::::" + grand_total1);
            System.out.println("description:::" + "Appraisal");
            try {
                Charge charge = Charge.create(chargeParams, com.stripe.Stripe.apiKey);
                System.out.println("charge:::::" + charge);
                status = charge.getStatus();
                System.out.println("status::::::" + status);
                if (status.equalsIgnoreCase("succeeded")) {
                    charge_id = charge.getId();
                    System.out.println("charge_id::::" + charge_id);
                    System.out.println("payment done");
                    //paymentDone();
                } else {
                    Toast.makeText(getActivity(), "Payment Failed", Toast.LENGTH_SHORT).show();
                }

            } catch (AuthenticationException | InvalidRequestException | APIConnectionException | APIException e) {
                e.printStackTrace();
            } catch (CardException e) {
                e.printStackTrace();
                System.out.println("Status is: " + e.getCode());
                System.out.println("Message is: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            Utils.hideProgressDialog();

            if (!TextUtils.isEmpty(status)) {
                if (status.equalsIgnoreCase("succeeded")) {
                    setOrderPaymentStatus(charge_id, "approved", userStripreID);
                }
            } else {
                //btnConfirmPay.setClickable(true);
                Toast.makeText(getActivity(), "Payment Failed", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void setOrderPaymentStatus(String payId, String status, String userStripreID) {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
        } else {
            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "order_payment");
            map.put("header", "");
            map.put("token", App.user.getToken());
            map.put("user_id", App.user.getUserID());
            map.put("OrderID", orderId);
            map.put("TransactionNo", payId);
            map.put("FeeAmount", String.valueOf(appraisalCost));
            map.put("ShippingAmount", String.valueOf(shipping));
            map.put("Discount", "0");
            map.put("Total", String.valueOf(total));
            map.put("Status", status);
            map.put("UserStripreID", userStripreID);

            new CallRequest(SavedCardList.this).orderPayment(map);

        }
    }

}
