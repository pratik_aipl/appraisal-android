package com.federal.appraisal.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.federal.appraisal.BuildConfig;
import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.activity.TermsAndConditionsActivity;
import com.federal.appraisal.animation.MyBounceInterpolator;
import com.federal.appraisal.ws.MyConstants;
import com.federal.appraisal.ws.Utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.federal.appraisal.activity.HomeActivity.TAG_ORDER_DETAIL;
import static com.federal.appraisal.activity.HomeActivity.TAG_WEBLINKS;

/**
 * A simple {@link Fragment} subclass.
 */
public class WebLinkFragment extends Fragment {

    public static final String URL_REGEX = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?";
    Button btnNext;
    EditText etCustomerName, etWebLink, etComment;
    CheckBox cbTerms;
    TextView tvTerms;

    public WebLinkFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_web_link, container, false);

        try {
            btnNext = view.findViewById(R.id.btnNext);
            etCustomerName = view.findViewById(R.id.etCustomerName);
            etWebLink = view.findViewById(R.id.etWebLink);
            etComment = view.findViewById(R.id.etComment);
            cbTerms = view.findViewById(R.id.cbTerms);
            tvTerms = view.findViewById(R.id.tvTerms);

            if (BuildConfig.DEBUG){
                etCustomerName.setText("test");
                etWebLink.setText("http://www.test.com");
            }
            tvTerms.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), TermsAndConditionsActivity.class);
                    startActivity(intent);
                }
            });

            ((HomeActivity) getActivity()).setFragmentTitle(3);
            ((HomeActivity) getActivity()).setIndex(3, TAG_WEBLINKS);

            btnNext.setClickable(true);

            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String customerName = etCustomerName.getText().toString().trim();
                    String webLink = etWebLink.getText().toString().trim();
                    String comment = etComment.getText().toString().trim();

                    Pattern p = Pattern.compile(URL_REGEX);
                    Matcher m = p.matcher(webLink);//replace with string to compare

/*                    if (TextUtils.isEmpty(customerName)) {
                        etCustomerName.requestFocus();
                        etCustomerName.setError("Customer name is required");
                    } else */if (!TextUtils.isEmpty(customerName) && customerName.length() < 4) {
                        etCustomerName.requestFocus();
                        etCustomerName.setError("Customer name is not valid");
                    } else if (TextUtils.isEmpty(webLink)) {
                        etWebLink.requestFocus();
                        etWebLink.setError("Weblink is required");
                    } else if (!URLUtil.isValidUrl(webLink)) {
                        etWebLink.requestFocus();
                        etWebLink.setError("Weblink is not valid");
                    } else if (!cbTerms.isChecked()) {
                        Utils.showAlert("You must agree to the terms and conditions before submitting!", getActivity());
                    } else {

                        btnNext.setClickable(false);

                        final Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);

                        // Use bounce interpolator with amplitude 0.2 and frequency 20
                        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                        myAnim.setInterpolator(interpolator);

                        btnNext.startAnimation(myAnim);

                        MyConstants.orderData.clear();
                        MyConstants.orderData.put("ItemId", "4");
                        MyConstants.orderData.put("customerName", customerName);
                        MyConstants.orderData.put("webLink", webLink);
                        MyConstants.orderData.put("comment", comment);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                ((HomeActivity) getActivity()).setIndex(4, TAG_ORDER_DETAIL);
                                ((HomeActivity) getActivity()).replaceFragment(null);
                            }
                        }, 1000);

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

}
