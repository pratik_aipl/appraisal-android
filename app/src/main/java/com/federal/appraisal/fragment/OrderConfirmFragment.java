package com.federal.appraisal.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.animation.MyBounceInterpolator;
import com.federal.appraisal.others.App;
import com.federal.appraisal.ws.MyConstants;

import java.text.DecimalFormat;

import static com.federal.appraisal.activity.HomeActivity.TAG_HOME;
import static com.federal.appraisal.activity.HomeActivity.TAG_ORDER_CONFIRM;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderConfirmFragment extends Fragment {

    Button btnOK;
    LinearLayout llOrderStatus, llError;
    ImageView ivOrderStatus;
    TextView tvOrderStatus, tvCertificateNumber, tvAmount, tvOrderType, tvError;
    DecimalFormat df = new DecimalFormat("##.##");

    public OrderConfirmFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_confirm, container, false);

        try {
            ((HomeActivity) getActivity()).setFragmentTitle(6);
            ((HomeActivity) getActivity()).setIndex(6, TAG_ORDER_CONFIRM);

            df.setMinimumFractionDigits(2);

            btnOK = view.findViewById(R.id.btnOK);
            llOrderStatus = view.findViewById(R.id.llOrderStatus);
            llError = view.findViewById(R.id.llError);
            ivOrderStatus = view.findViewById(R.id.ivOrderStatus);
            tvOrderStatus = view.findViewById(R.id.tvOrderStatus);
            tvCertificateNumber = view.findViewById(R.id.tvCertificateNumber);
            tvAmount = view.findViewById(R.id.tvAmount);
            tvOrderType = view.findViewById(R.id.tvOrderType);
            tvError = view.findViewById(R.id.tvError);

            if (MyConstants.paymentSuccess) {
                llOrderStatus.setBackground(getResources().getDrawable(R.drawable.rounded_top_corners_bg_green));
                llError.setVisibility(View.GONE);
                tvOrderStatus.setText("Order Successful");
                ivOrderStatus.setImageDrawable(getResources().getDrawable(R.drawable.right));
                tvAmount.setText("Paid : $" + df.format(Double.parseDouble(MyConstants.totalAmount)));
                tvCertificateNumber.setText("Certificate # " + App.certificateNo);
                tvOrderType.setText(MyConstants.orderType);
            } else {
                llOrderStatus.setBackground(getResources().getDrawable(R.drawable.rounded_top_corners_bg_red));
                llError.setVisibility(View.VISIBLE);
                ivOrderStatus.setImageDrawable(getResources().getDrawable(R.drawable.close));
                tvOrderStatus.setText("Order Failed");
                tvAmount.setText("Pay For : $" + df.format(Double.parseDouble(MyConstants.totalAmount)));
                tvError.setText(MyConstants.errorPayment);

                tvOrderType.setText(MyConstants.orderType);
            }

            btnOK.setClickable(true);
            btnOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btnOK.setClickable(false);

                    final Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);

                    // Use bounce interpolator with amplitude 0.2 and frequency 20
                    MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                    myAnim.setInterpolator(interpolator);

                    btnOK.startAnimation(myAnim);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            MyConstants.paymentSuccess = false;
                            //Do something after 100ms
                            ((HomeActivity) getActivity()).setIndex(0, TAG_HOME);
                            ((HomeActivity) getActivity()).replaceFragment(null);
                            ((HomeActivity) getActivity()).clearBackStack();
                        }
                    }, 1000);
                }
            });
        } catch (Exception e){
            e.printStackTrace();
        }

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

}
