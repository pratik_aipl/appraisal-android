package com.federal.appraisal.fragment;


import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.federal.appraisal.BuildConfig;
import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.animation.MyBounceInterpolator;
import com.federal.appraisal.others.App;
import com.federal.appraisal.others.Internet;
import com.federal.appraisal.ws.AsyncTaskListner;
import com.federal.appraisal.ws.CallRequest;
import com.federal.appraisal.ws.Constant;
import com.federal.appraisal.ws.MyConstants;
import com.federal.appraisal.ws.Utils;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Charge;
import com.stripe.model.Customer;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static com.federal.appraisal.activity.HomeActivity.TAG_ORDER_CONFIRM;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckOutFragment extends Fragment implements AsyncTaskListner {

    public EditText etCardNumber, etCardHolderName, etMonth, etYear, etCVV, etZipcode;
    SharedPreferences sharedpreferences;
    String stripe_id;
    Button btnConfirmPay;
    TextView tvPaymentWith;
    Card card;
    String TOKEN_ID, user_email = "Guest", customerId, charge_id, status, orderId, appraisalCost, shipping, shippingMethod;
    boolean saveCardForPayment;
    float total;
    String cardHolderName, userStripreID;

    public CheckOutFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_check_out, container, false);

        setHasOptionsMenu(true);

        Bundle bundle = getArguments();
        if (bundle != null) {
            orderId = bundle.getString("order_id");
            total = Float.parseFloat(bundle.getString("total"));
            appraisalCost = bundle.getString("appraisal_cost");
            shipping = bundle.getString("shipping");
            shippingMethod = bundle.getString("shipping_method");
        }

        sharedpreferences = getActivity().getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);

        stripe_id = sharedpreferences.getString(MyConstants.STRIPE_ID, "");
        user_email = sharedpreferences.getString(MyConstants.USER_EMAIL, "");

        btnConfirmPay = view.findViewById(R.id.btnConfirmPay);
        tvPaymentWith = view.findViewById(R.id.tvPaymentWith);
        etCardHolderName = view.findViewById(R.id.etCardHolderName);
        etCardNumber = view.findViewById(R.id.etCardNumber);
        etMonth = view.findViewById(R.id.etMonth);
        etYear = view.findViewById(R.id.etYear);
        etZipcode = view.findViewById(R.id.etZipcode);
        etCVV = view.findViewById(R.id.etCVV);

        String paymentWith = "stripe.com";
        tvPaymentWith.setText(Html.fromHtml(paymentWith));

        etCardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());
        etMonth.addTextChangedListener(new MonthValidateWatcher());
        etYear.addTextChangedListener(new YearValidateWatcher());

        //  btnConfirmPay.setClickable(true);
        btnConfirmPay.setOnClickListener(view1 -> {
            //System.out.println("card payment:::"+cardPaymentFragment);

            //btnConfirmPay.setClickable(false);

            final Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);

            // Use bounce interpolator with amplitude 0.2 and frequency 20
            MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
            myAnim.setInterpolator(interpolator);

            btnConfirmPay.startAnimation(myAnim);

            String cardNumber = etCardNumber.getText().toString();
            cardHolderName = etCardHolderName.getText().toString().trim();
            String month = etMonth.getText().toString();
            String year = etYear.getText().toString();
            String cvv = etCVV.getText().toString();
            String zip = etZipcode.getText().toString();
            //boolean saveCard = switchSave.isChecked();

            if (TextUtils.isEmpty(cardNumber)) {
                etCardNumber.requestFocus();
                etCardNumber.setError("Enter valid card number");
            } else if (TextUtils.isEmpty(cardHolderName)) {
                etCardHolderName.requestFocus();
                etCardHolderName.setError("Enter card holder name");
            } else if (TextUtils.isEmpty(month) || !month.matches("(?:0[1-9]|1[0-2])")) {
                etMonth.requestFocus();
                etMonth.setError("Enter valid month");
            } else if (TextUtils.isEmpty(year)) {
                etYear.requestFocus();
                etYear.setError("Enter valid year");
            } else if (TextUtils.isEmpty(cvv)) {
                etCVV.requestFocus();
                etCVV.setError("Enter valid CVV");
            } else if (TextUtils.isEmpty(zip)) {
                etZipcode.requestFocus();
                etZipcode.setError("Enter valid Zipcode");
            } else {
                //int tabIndex = tabLayout.getSelectedTabPosition();

                /*if (!TextUtils.isEmpty(stripe_id)
                        && cardNumber.substring(cardNumber.length() - 4).equalsIgnoreCase(last4)
                        && month.equalsIgnoreCase(exp_month)
                        && year.equalsIgnoreCase(exp_year)) {
                    System.out.println("saved card:::::::::");
                    customerId = stripe_id;
                    saveCardForPayment = true;
                    new CreateChargeAndDoPayment().execute();
                } else {*/
                System.out.println("new card:::::::::");
                doStripePayment(cardNumber, cardHolderName, month, year, cvv, false, zip);
                //}
            }
        });

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    private void doStripePayment(String cardNumber, String cardHolderName, String month, String year, String cvv, final boolean saveCard, String addressZip) {

        Utils.showProgressDialog(getActivity(), "Loading");

        String mYear = year.substring(Math.max(year.length() - 2, 0));

        // validate card
        card = new Card(cardNumber, Integer.valueOf(month), Integer.valueOf(mYear), cvv);
        card.setAddressZip(addressZip);

        if (!card.validateNumber()) {
            etCardNumber.requestFocus();
            Toast.makeText(getActivity(), "Enter valid card number", Toast.LENGTH_LONG).show();
            Utils.hideProgressDialog();
        } else if (!card.validateCVC()) {
            etCVV.requestFocus();
            Utils.hideProgressDialog();
            Toast.makeText(getActivity(), "Enter valid CVV number", Toast.LENGTH_LONG).show();
        } else if (!card.validateExpMonth()) {
            etMonth.requestFocus();
            Utils.hideProgressDialog();
            Toast.makeText(getActivity(), "Invalid month", Toast.LENGTH_LONG).show();
        } else if (!card.validateExpYear()) {
            etYear.requestFocus();
            Utils.hideProgressDialog();
            Toast.makeText(getActivity(), "Invalid year", Toast.LENGTH_LONG).show();
        } else {
            String PUBLISH_KEY = BuildConfig.STRIPE_PUBLISH_KEY;
            System.out.println("keys ==>" + PUBLISH_KEY);
            new Stripe().createToken(card, PUBLISH_KEY, new TokenCallback() {
                public void onSuccess(Token token) {
                    //Toast.makeText( getApplicationContext(),"Token created: " + token.getId(),Toast.LENGTH_LONG).show();
                    TOKEN_ID = token.getId();
                    System.out.println("Token:::" + TOKEN_ID);

                    // Create a Customer:
                    if (TextUtils.isEmpty(user_email)) {
                        user_email = "Guest";
                    }
                    System.out.println("email::::" + user_email);
                    com.stripe.Stripe.apiKey = BuildConfig.STRIPE_SECRET_KEY;
                    Map<String, Object> customerParams = new HashMap<String, Object>();
                    customerParams.put("email", user_email);
                    customerParams.put("source", TOKEN_ID);

                    saveCardForPayment = saveCard;
                    AddUserCard();
//                    if (saveCardForPayment) {
//                        new CreateCustomer().execute(customerParams);
//                    } else {
//                        new CreateChargeAndDoPayment().execute();
//                    }
                }

                public void onError(Exception error) {
                    Utils.hideProgressDialog();
                    //btnConfirmPay.setClickable(true);
                    Toast.makeText(getActivity(), "Your card was declined", Toast.LENGTH_LONG).show();

                    Log.d("Stripe", error.getLocalizedMessage());
                }
            });
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {

                    case orderPayment:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {
                                //Utils.showToast(mainObj.getString("message"), LoginActivity.this);
                                JSONObject object = mainObj.getJSONObject("data");

                                App.certificateNo = object.getString("certificate_no");
                                System.out.println("certificte no:==" + App.certificateNo);
                                //orderId = object.getString("order_id");

                                MyConstants.paymentSuccess = true;
                            } else {
                                Utils.showToast(mainObj.getString("message"), getActivity());
                                MyConstants.paymentSuccess = false;
                                MyConstants.errorPayment = mainObj.getString("message");
                            }
                            MyConstants.totalAmount = String.valueOf(total);
                            MyConstants.orderType = String.valueOf(shippingMethod);

                            ((HomeActivity) getActivity()).setIndex(6, TAG_ORDER_CONFIRM);
                            ((HomeActivity) getActivity()).replaceFragment(null);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case addUserCard:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {
                                // Utils.showToast(mainObj.getString("message"), getActivity());
                                new CreateChargeAndDoPayment().execute(mainObj.getJSONObject("data").getString("Customer_id"), mainObj.getJSONObject("data").getString("UserStripeID"));
                            } else {
                                //Utils.showToast(mainObj.getString("message"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    // TODO: 3/13/2019  Order Payment
    private void setOrderPaymentStatus(String payId, String status, String userStripreID) {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
        } else {
            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "order_payment");
            map.put("header", "");
            map.put("token", App.user.getToken());
            map.put("user_id", App.user.getUserID());
            map.put("OrderID", orderId);
            map.put("TransactionNo", payId);
            map.put("FeeAmount", String.valueOf(appraisalCost));
            map.put("ShippingAmount", String.valueOf(shipping));
            map.put("Discount", "0");
            map.put("Total", String.valueOf(total));
            map.put("Status", status);
            map.put("UserStripreID", userStripreID);

            new CallRequest(CheckOutFragment.this).orderPayment(map);

        }
    }

    private void AddUserCard() {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
        } else {
            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "add_user_card");
            map.put("header", "");
            map.put("token", App.user.getToken());
            map.put("user_id", App.user.getUserID());
            map.put("tokenId", TOKEN_ID);
            map.put("card_holder_name", cardHolderName);
            map.put("email", user_email);

            new CallRequest(CheckOutFragment.this).addUserCard(map);

        }
    }

    public class FourDigitCardFormatWatcher implements TextWatcher {

        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove all spacing char
            int pos = 0;
            while (true) {
                if (pos >= s.length()) break;
                if (space == s.charAt(pos) && (((pos + 1) % 5) != 0 || pos + 1 == s.length())) {
                    s.delete(pos, pos + 1);
                } else {
                    pos++;
                }
            }

            // Insert char where needed.
            pos = 4;
            while (true) {
                if (pos >= s.length()) break;
                final char c = s.charAt(pos);
                // Only if its a digit where there should be a space we insert a space
                if ("0123456789".indexOf(c) >= 0) {
                    s.insert(pos, "" + space);
                }
                pos += 5;
            }
        }
    }

    public class MonthValidateWatcher implements TextWatcher {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!etMonth.getText().toString().matches("(?:0[1-9]|1[0-2])")) {
                etMonth.setError("Invalid month");
            }
        }
    }

    public class YearValidateWatcher implements TextWatcher {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!etYear.getText().toString().matches("^20((1[7-9])|([2-9][0-9]))$")) {
                etYear.setError("Invalid year");
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class CreateCustomer extends AsyncTask<Map<String, Object>, Void, Void> {

        @Override
        protected Void doInBackground(Map<String, Object>[] maps) {
            try {
                com.stripe.Stripe.apiKey = BuildConfig.STRIPE_SECRET_KEY;
                Customer customer = Customer.create(maps[0], com.stripe.Stripe.apiKey);
                customerId = customer.getId();

                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(MyConstants.STRIPE_ID, customerId);
                editor.apply();

            } catch (AuthenticationException | InvalidRequestException | APIConnectionException | CardException | APIException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            Utils.hideProgressDialog();
            //AddUserCard();
            new CreateChargeAndDoPayment().execute();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class CreateChargeAndDoPayment extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog

        }

        @Override
        protected Void doInBackground(String... strings) {
            String customerId = strings[0];
            userStripreID = strings[1];
            com.stripe.Stripe.apiKey = BuildConfig.STRIPE_SECRET_KEY;
            Number num = 0;
            try {
                num = NumberFormat.getInstance().parse(String.valueOf(total));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            int grand_total1 = 0;
            if (num != null) {
                grand_total1 = num.intValue() * 100;
            }
            Map<String, Object> chargeParams = new HashMap<String, Object>();
            chargeParams.put("amount", grand_total1);
            chargeParams.put("description", "Appraisal");
            chargeParams.put("currency", "usd");

//            if (saveCardForPayment) {
            chargeParams.put("customer", customerId);
            System.out.println("customer::::" + customerId);
//            } else {
//                chargeParams.put("source", TOKEN_ID);
//                System.out.println("TOKEN_ID::::" + TOKEN_ID);
//            }
            System.out.println("amount:::::::" + grand_total1);
            System.out.println("description:::" + "Appraisal");

            try {
                Charge charge = Charge.create(chargeParams, com.stripe.Stripe.apiKey);
                System.out.println("charge:::::" + charge);
                status = charge.getStatus();
                System.out.println("status::::::" + status);
                if (status.equalsIgnoreCase("succeeded")) {
                    charge_id = charge.getId();
                    System.out.println("charge_id::::" + charge_id);
                    System.out.println("payment done");
                    //paymentDone();
                } else {
                    Toast.makeText(getActivity(), "Payment Failed", Toast.LENGTH_SHORT).show();
                }

            } catch (AuthenticationException e) {
                e.printStackTrace();
            } catch (InvalidRequestException e) {
                e.printStackTrace();
            } catch (APIConnectionException e) {
                e.printStackTrace();
            } catch (CardException e) {
                e.printStackTrace();
                System.out.println("Status is: " + e.getCode());
                System.out.println("Message is: " + e.getMessage());
            } catch (APIException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            Utils.hideProgressDialog();

            if (!TextUtils.isEmpty(status)) {
                if (status.equalsIgnoreCase("succeeded")) {
                    //Toast.makeText(getApplicationContext(), "Payment has been successful", Toast.LENGTH_SHORT).show();

                    //  setOrderPaymentStatus(charge_id, "approved");
                    setOrderPaymentStatus(charge_id, "approved", userStripreID);

                    /*Intent intent = new Intent(PaymentActivity.this, PTDashboardActivity.class);
                    startActivity(intent);
                    ActivityCompat.finishAffinity(PaymentActivity.this);*/
                }
            } else {
                //btnConfirmPay.setClickable(true);
                Toast.makeText(getActivity(), "Payment Failed", Toast.LENGTH_SHORT).show();
            }

        }
    }

}
