package com.federal.appraisal.fragment;


import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.federal.appraisal.BuildConfig;
import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.activity.TermsAndConditionsActivity;
import com.federal.appraisal.adapter.NothingSelectedSpinnerAdapter;
import com.federal.appraisal.adapter.WatchesAdapter;
import com.federal.appraisal.animation.MyBounceInterpolator;
import com.federal.appraisal.materialrangebar.RangeBar;
import com.federal.appraisal.utils.VerticalSpacingDecoration;
import com.federal.appraisal.ws.MyConstants;
import com.federal.appraisal.ws.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.federal.appraisal.activity.HomeActivity.TAG_ORDER_DETAIL;
import static com.federal.appraisal.activity.HomeActivity.TAG_WATCHES;
import static com.federal.appraisal.activity.HomeActivity.TAG_WATCHES_AUTHENTIC_PARTS;

/**
 * A simple {@link Fragment} subclass.
 */
public class WatchesFragment extends Fragment {

    private static int RESULT_LOAD_IMG = 1;
    private static int RESULT_CROP_DP = 3;
    public final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public Uri selectedUri;
    RecyclerView rvImages;
    RelativeLayout mClearityView, mDiamondColor;
    RangeBar mClarityRangeBar, mColorRangeBar;
    CardView cMoreView, cvOtherGemMaterial, mDimondWeight, cvOtherMaterial;
    List<String> imagePath;
    LinearLayoutManager linearLayoutManager;
    WatchesAdapter watchesAdapter;
    Button btnNext;
    String filePathDp = "";
    String filePathFirst = "";
    Spinner spinnerWind, spinnerMaterialType, spinnerGemMaterial;
    CheckBox cbTerms;
    EditText etCustomerName, etdial, etBrandName, etModelName, etSerialNumber, etCaseSize, etEstimatedValue, etDiamondWeight, etComment, etOtherMaterial,
            etOtherGemMaterial, etMoreColorValue /*etMetal*/;
    RadioGroup rgOriginalParts;
    RadioButton rbYes, rbNo;
    String windStr, materialTypeStr = "", materialTypeId = "", windId = "", originalParts = "1";
    String gemMaterialId, gemMaterialStr, clearity_start, clearity_end, color_start, color_end, clearity_start_name, clearity_end_name, color_start_name, color_end_name;

    TextView tvTerms, tvUploadPictures;
    String TAG = "IMAGE::";
    private String[] wind, materialType, colorQ, clarityGrade, gemMaterial;
    private String issueImagePath;

    public WatchesFragment() {
        // Required empty public constructor
    }

    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,
                                                     int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        return bmp;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_watches, container, false);

        try {
            ((HomeActivity) getActivity()).setFragmentTitle(8);
            ((HomeActivity) getActivity()).setIndex(0, TAG_WATCHES);

            imagePath = new ArrayList<>();
            etdial = view.findViewById(R.id.etdial);
            rvImages = view.findViewById(R.id.rvImages);
            btnNext = view.findViewById(R.id.btnNext);
            spinnerWind = view.findViewById(R.id.spinnerWind);
            //etMetal = view.findViewById(R.id.etMetal);
            mDimondWeight = view.findViewById(R.id.mDimondWeight);
            etOtherMaterial = view.findViewById(R.id.etOtherMaterial);
            cvOtherMaterial = view.findViewById(R.id.cvOtherMaterial);
            cvOtherGemMaterial = view.findViewById(R.id.cvOtherGemMaterial);
            etOtherGemMaterial = view.findViewById(R.id.etOtherGemMaterial);
            spinnerMaterialType = view.findViewById(R.id.spinnerMaterialType);
            spinnerGemMaterial = view.findViewById(R.id.spinnerGemMaterial);
            cbTerms = view.findViewById(R.id.cbTerms);
            etCustomerName = view.findViewById(R.id.etCustomerName);
            etBrandName = view.findViewById(R.id.etBrandName);
            etModelName = view.findViewById(R.id.etModelName);
            etSerialNumber = view.findViewById(R.id.etSerialNumber);
            etCaseSize = view.findViewById(R.id.etCaseSize);
            cMoreView = view.findViewById(R.id.cMoreView);
            etMoreColorValue = view.findViewById(R.id.etMoreColorValue);
            etEstimatedValue = view.findViewById(R.id.etEstimatedValue);
            etDiamondWeight = view.findViewById(R.id.etDiamondWeight);
            etComment = view.findViewById(R.id.etComment);
            rgOriginalParts = view.findViewById(R.id.rgOriginalParts);
            rbYes = view.findViewById(R.id.rbYes);
            rbNo = view.findViewById(R.id.rbNo);
            tvTerms = view.findViewById(R.id.tvTerms);
            tvUploadPictures = view.findViewById(R.id.tvUploadPictures);

            mDiamondColor = view.findViewById(R.id.mDiamondColor);
            mClearityView = view.findViewById(R.id.mClearityView);
            mClarityRangeBar = view.findViewById(R.id.mClarityRangeBar);
            mColorRangeBar = view.findViewById(R.id.mColorRangeBar);


            /*String customerName = "<font color='#ff0000'>*</font> Customer Name";
            etCustomerName.setHint(Html.fromHtml(customerName));

            String brandName = "<font color='#ff0000'>*</font> Brand Name";
            etBrandName.setHint(Html.fromHtml(brandName));

            String serial = "<font color='#ff0000'>*</font> Serial #";
            etSerialNumber.setHint(Html.fromHtml(serial));

            String uploadPicture = "<font color='#ff0000'>*</font> Upload Pictures";
            tvUploadPictures.setText(Html.fromHtml(uploadPicture));*/

            clarityGrade = new String[MyConstants.allMaterial.getClarity().size()];
            for (int i = 0; i < MyConstants.allMaterial.getClarity().size(); i++) {
                clarityGrade[i] = MyConstants.allMaterial.getClarity().get(i).get("ClarityName");
            }

            colorQ = new String[MyConstants.allMaterial.getColor().size()];
            for (int i = 0; i < MyConstants.allMaterial.getColor().size(); i++) {
                colorQ[i] = MyConstants.allMaterial.getColor().get(i).get("ColorName");
            }

            wind = new String[MyConstants.allMaterial.getWind().size()];
            for (int i = 0; i < MyConstants.allMaterial.getWind().size(); i++) {
                wind[i] = MyConstants.allMaterial.getWind().get(i).get("WindType");
            }

            ArrayAdapter adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, wind);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerWind.setAdapter(new NothingSelectedSpinnerAdapter(adapter, R.layout.nothing_selected_wind, getContext()));


            //spinnerMaterialType.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, materialType));


            spinnerWind.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        position = position - 1;
                    }
                    windId = MyConstants.allMaterial.getWind().get(position).get("WindTypeID");
                    windStr = wind[position];
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    windId = "";
                    windStr = "";
                }
            });

            materialType = new String[MyConstants.allMaterial.getMaterialWatchType().size()];
            for (int i = 0; i < MyConstants.allMaterial.getMaterialWatchType().size(); i++) {
                materialType[i] = MyConstants.allMaterial.getMaterialWatchType().get(i).get("MaterialType");
            }

            ArrayAdapter aa = new ArrayAdapter<>(getContext(), R.layout.spinner_text, materialType);
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerMaterialType.setAdapter(new NothingSelectedSpinnerAdapter(aa, R.layout.nothing_selected_material, getContext()));

            spinnerMaterialType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        materialTypeId = MyConstants.allMaterial.getMaterialWatchType().get(position - 1).get("MaterialTypeID");
                        materialTypeStr = materialType[position - 1];

                        if (materialTypeStr.equalsIgnoreCase("Other")) {
                            cvOtherMaterial.setVisibility(View.VISIBLE);
                        } else {
                            cvOtherMaterial.setVisibility(View.GONE);
                        }
                    } else {
                        materialTypeId = "";
                        materialTypeStr = "";
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    materialTypeId = "";
                    materialTypeStr = "";
                }
            });

            gemMaterial = new String[MyConstants.allMaterial.getGemMaterialWatchType().size()];
//            gemMaterial[0]="Gem Material";
            for (int i = 0; i < MyConstants.allMaterial.getGemMaterialWatchType().size(); i++) {
                gemMaterial[i] = MyConstants.allMaterial.getGemMaterialWatchType().get(i).get("MaterialType");
            }

            ArrayAdapter gm = new ArrayAdapter<>(getContext(), R.layout.spinner_text, gemMaterial);
            gm.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerGemMaterial.setAdapter(new NothingSelectedSpinnerAdapter(gm, R.layout.nothing_selected_layout, getContext()));

            if (gemMaterial.length > 1)
                spinnerGemMaterial.setSelection(1);
            spinnerGemMaterial.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        System.out.println("position:::" + position);
                        Log.d(TAG, "onItemSelected: pos " + position);
                        if (position != 0) {
                            gemMaterialId = MyConstants.allMaterial.getGemMaterialWatchType().get(position - 1).get("MaterialTypeID");
                            gemMaterialStr = gemMaterial[position - 1];

                            Log.d(TAG, "onItemSelected: gemMaterial " + gemMaterialStr);
                            if (gemMaterialStr.equalsIgnoreCase("Other")) {
                                mClearityView.setVisibility(View.VISIBLE);
                                mDiamondColor.setVisibility(View.VISIBLE);
                                mDimondWeight.setVisibility(View.VISIBLE);
                                cvOtherGemMaterial.setVisibility(View.VISIBLE);
                                etOtherGemMaterial.setVisibility(View.VISIBLE);
                                cMoreView.setVisibility(View.VISIBLE);
                            } else if (gemMaterialStr.equalsIgnoreCase("None")) {
                                mClearityView.setVisibility(View.GONE);
                                mDiamondColor.setVisibility(View.GONE);
                                mDimondWeight.setVisibility(View.GONE);
                                cvOtherGemMaterial.setVisibility(View.GONE);
                                etOtherGemMaterial.setVisibility(View.GONE);
                                cMoreView.setVisibility(View.GONE);
                            } else {
                                if (color_start.equalsIgnoreCase("11") || color_end.equalsIgnoreCase("11")) {
                                    cMoreView.setVisibility(View.VISIBLE);
                                } else {
                                    cMoreView.setVisibility(View.GONE);
                                }
                                cvOtherGemMaterial.setVisibility(View.GONE);
                                etOtherGemMaterial.setVisibility(View.GONE);
                                mClearityView.setVisibility(View.VISIBLE);
                                mDiamondColor.setVisibility(View.VISIBLE);
                                mDimondWeight.setVisibility(View.VISIBLE);
                            }
                        } else {
                            mClearityView.setVisibility(View.GONE);
                            mDiamondColor.setVisibility(View.GONE);
                            cMoreView.setVisibility(View.GONE);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    gemMaterialId = "";
                    gemMaterialStr = "";
                    mClearityView.setVisibility(View.GONE);
                    mDiamondColor.setVisibility(View.GONE);
                }
            });


            if (MyConstants.allMaterial.getClarity().size() > 0) {
                color_start = MyConstants.allMaterial.getColor().get(0).get("ColorID");
                color_end = MyConstants.allMaterial.getColor().get(colorQ.length - 1).get("ColorID");
                color_start_name = MyConstants.allMaterial.getColor().get(0).get("ColorName");
                color_end_name = MyConstants.allMaterial.getColor().get(colorQ.length - 1).get("ColorName");
                if (MyConstants.allMaterial.getColor().get(0).get("ColorID").equalsIgnoreCase("11") || MyConstants.allMaterial.getColor().get(colorQ.length - 1).get("ColorID").equalsIgnoreCase("11")) {
                    cMoreView.setVisibility(View.VISIBLE);
                } else {
                    cMoreView.setVisibility(View.GONE);
                }
            }
            mColorRangeBar.setTickEnd(MyConstants.allMaterial.getColor().size());
            mColorRangeBar.setTickTopLabels(colorQ);

//            mColorRangeBar.setLeft(0);
//            mColorRangeBar.setRight(1);
            mColorRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
                @Override
                public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                    color_start = MyConstants.allMaterial.getColor().get(leftPinIndex).get("ColorID");
                    color_end = MyConstants.allMaterial.getColor().get(rightPinIndex).get("ColorID");
                    color_start_name = MyConstants.allMaterial.getColor().get(leftPinIndex).get("ColorName");
                    color_end_name = MyConstants.allMaterial.getColor().get(rightPinIndex).get("ColorName");
                    if (color_start.equalsIgnoreCase("11") || color_end.equalsIgnoreCase("11")) {
                        cMoreView.setVisibility(View.VISIBLE);
                    } else {
                        cMoreView.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onTouchStarted(RangeBar rangeBar) {

                }

                @Override
                public void onTouchEnded(RangeBar rangeBar) {

                }
            });

            if (MyConstants.allMaterial.getClarity().size() > 0) {
                clearity_start = MyConstants.allMaterial.getClarity().get(0).get("ClarityID");
                clearity_end = MyConstants.allMaterial.getClarity().get(1).get("ClarityID");
                clearity_start_name = MyConstants.allMaterial.getClarity().get(0).get("ClarityName");
                clearity_end_name = MyConstants.allMaterial.getClarity().get(1).get("ClarityName");
            }
            mClarityRangeBar.setTickTopLabels(clarityGrade);
            mClarityRangeBar.setTickEnd(MyConstants.allMaterial.getClarity().size());

            mClarityRangeBar.setLeft(0);
            mClarityRangeBar.setRight(1);

            mClarityRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
                @Override
                public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                    clearity_start = MyConstants.allMaterial.getClarity().get(leftPinIndex).get("ClarityID");
                    clearity_end = MyConstants.allMaterial.getClarity().get(rightPinIndex).get("ClarityID");

                    clearity_start_name = MyConstants.allMaterial.getClarity().get(leftPinIndex).get("ClarityName");
                    clearity_end_name = MyConstants.allMaterial.getClarity().get(rightPinIndex).get("ClarityName");
                }

                @Override
                public void onTouchStarted(RangeBar rangeBar) {

                }

                @Override
                public void onTouchEnded(RangeBar rangeBar) {

                }
            });

            tvTerms.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), TermsAndConditionsActivity.class);
                    startActivity(intent);
                }
            });

            rgOriginalParts.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    RadioButton checkedRadioButton = (RadioButton) radioGroup.findViewById(i);
                    if (checkedRadioButton == rbYes) {
                        originalParts = "1";
                    } else {
                        originalParts = "0";
                    }
                }
            });

            //imagePath.clear();
            imagePath.add("by default");
            if (MyConstants.imagePath.size() > 0 && MyConstants.orderData.get("ItemId").equalsIgnoreCase("2")) {
                imagePath.addAll(MyConstants.imagePath);
            }
            linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            rvImages.setLayoutManager(linearLayoutManager);

            watchesAdapter = new WatchesAdapter(getActivity(), imagePath, WatchesFragment.this);
            rvImages.setHasFixedSize(true);
            rvImages.addItemDecoration(new VerticalSpacingDecoration(20));
            rvImages.setItemViewCacheSize(20);
            rvImages.setDrawingCacheEnabled(true);
            rvImages.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            rvImages.setAdapter(watchesAdapter);

            if (BuildConfig.DEBUG) {
                etBrandName.setText("test");
                etModelName.setText("test");
                etSerialNumber.setText("5245SAS");
                etCaseSize.setText("12");
                etEstimatedValue.setText("10");
                etDiamondWeight.setText("8");
                etdial.setText("2");
            }

            btnNext.setClickable(true);
            btnNext.setOnClickListener(view1 -> {

                String customerName = etCustomerName.getText().toString().trim();
                String brandName = etBrandName.getText().toString().trim();
                String modelName = etModelName.getText().toString().trim();
                String serialNumber = etSerialNumber.getText().toString().trim();
                String caseSize = etCaseSize.getText().toString().trim();
                String estimatedValue = etEstimatedValue.getText().toString().trim();
                String diamondWeight = etDiamondWeight.getText().toString().trim();
                String comment = etComment.getText().toString().trim();
                //String metal = etMetal.getText().toString().trim();
                String otherMaterial = etOtherMaterial.getText().toString().trim();

                /*if (TextUtils.isEmpty(customerName)) {
                    etCustomerName.requestFocus();
                    etCustomerName.setError("Customer name is required");
                } else*/
                if (TextUtils.isEmpty(brandName)) {
                    etBrandName.requestFocus();
                    etBrandName.setError("Brand name is required");
                } /*else if (TextUtils.isEmpty(modelName)) {
                etModelName.requestFocus();
                etModelName.setError("Model name is required");
                }*/ else if (TextUtils.isEmpty(modelName)) {
                    etModelName.requestFocus();
                    etModelName.setError("Model name is required");
                } else if (!Utils.isSerialNoValid(modelName)) {
                    etModelName.requestFocus();
                    etModelName.setError("Please enter valid Model Name.");
                } else if (TextUtils.isEmpty(serialNumber)) {
                    etSerialNumber.requestFocus();
                    etSerialNumber.setError("Serial number is required");
                } else if (!Utils.isSerialNoValid(serialNumber)) {
                    etSerialNumber.requestFocus();
                    etSerialNumber.setError("Please enter valid Serial number.");
                } else if (TextUtils.isEmpty(materialTypeId) && !materialTypeStr.equalsIgnoreCase("Other")) {
                    Utils.showAlert("Please select  Metal Type.", getActivity());
                } else if (materialTypeStr.equalsIgnoreCase("Other") && TextUtils.isEmpty(otherMaterial)) {
                    etOtherMaterial.requestFocus();
                    etOtherMaterial.setError("Other material is required");
                } else if (TextUtils.isEmpty(caseSize)) {
                    etCaseSize.requestFocus();
                    etCaseSize.setError("Case size is required");
                } else if (cMoreView.getVisibility() == View.VISIBLE && TextUtils.isEmpty(etMoreColorValue.getText().toString().trim())) {
                    etMoreColorValue.requestFocus();
                    etMoreColorValue.setError("Please enter color info.");
                } else if (cvOtherGemMaterial.getVisibility() == View.VISIBLE && TextUtils.isEmpty(etOtherGemMaterial.getText().toString().trim())) {
                    etOtherGemMaterial.requestFocus();
                    etOtherGemMaterial.setError("Please enter other Gem Material.");
                } else if (TextUtils.isEmpty(estimatedValue)) {
                    etEstimatedValue.requestFocus();
                    etEstimatedValue.setError("Estimated value is required");
                }/* else if (TextUtils.isEmpty(diamondWeight)) {
                    etDiamondWeight.requestFocus();
                    etDiamondWeight.setError("Diamond weight is required");
                } */ else if (TextUtils.isEmpty(gemMaterialId)) {
                    Utils.showAlert("Please select Gem material", getActivity());
                } else if (imagePath.size() < 2) {
                    Utils.showAlert("Select at least one image", getActivity());
                } else if (!cbTerms.isChecked()) {
                    Utils.showAlert("You must agree to the terms and conditions before submitting!", getActivity());
                } else if (TextUtils.isEmpty(etdial.getText().toString())) {
                    Utils.showAlert("Dial is required", getActivity());
                } else {
                    btnNext.setClickable(false);
                    Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
                    // Use bounce interpolator with amplitude 0.2 and frequency 20
                    MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                    myAnim.setInterpolator(interpolator);
                    btnNext.startAnimation(myAnim);
                    MyConstants.orderData.clear();
                    MyConstants.orderData.put("ItemId", "2");
                    MyConstants.orderData.put("customerName", customerName);
                    MyConstants.orderData.put("caratWeight", diamondWeight);
                    MyConstants.orderData.put("estimatedValue", estimatedValue);
                    MyConstants.orderData.put("windId", windId);
                    MyConstants.orderData.put("wind", windStr);
                    MyConstants.orderData.put("materialType", materialTypeStr);
                    MyConstants.orderData.put("materialTypeId", materialTypeId);
                    MyConstants.orderData.put("materialTypeOther", otherMaterial);
                    MyConstants.orderData.put("brandName", brandName);
                    MyConstants.orderData.put("serialNumber", serialNumber);
                    MyConstants.orderData.put("dial", etdial.getText().toString());
                    MyConstants.orderData.put("modelName", modelName);
                    MyConstants.orderData.put("caseSize", caseSize);
                    MyConstants.orderData.put("isAllPartOriginal", originalParts);
                    MyConstants.orderData.put("imagesCount", String.valueOf(imagePath.size() - 1));
                    MyConstants.orderData.put("comment", comment);

                    MyConstants.orderData.put("gemMaterialId", gemMaterialId);
                    MyConstants.orderData.put("gemMaterialOther", gemMaterialStr);
                    if (cvOtherGemMaterial.getVisibility() == View.VISIBLE && !TextUtils.isEmpty(etOtherGemMaterial.getText().toString().trim())) {
                        MyConstants.orderData.put("gemMaterial", etOtherGemMaterial.getText().toString().trim());
                    } else {
                        MyConstants.orderData.put("gemMaterial", gemMaterialStr);
                    }

                    if (cMoreView.getVisibility() == View.VISIBLE && !TextUtils.isEmpty(etMoreColorValue.getText().toString().trim()))
                        MyConstants.orderData.put("color", etMoreColorValue.getText().toString().trim());
                    else
                        MyConstants.orderData.put("color", "");

                    if (gemMaterialStr.equalsIgnoreCase("none")) {
                        MyConstants.orderData.put("clearity_start", "0");
                        MyConstants.orderData.put("clearity_end", "0");
                        MyConstants.orderData.put("color_start", "0");
                        MyConstants.orderData.put("color_end", "0");

                    } else {
                        MyConstants.orderData.put("clearity_start", clearity_start);
                        MyConstants.orderData.put("clearity_end", clearity_end);
                        MyConstants.orderData.put("color_start", color_start);
                        MyConstants.orderData.put("color_end", color_end);
                    }


                    MyConstants.orderData.put("clearity_check", "0");
                    MyConstants.orderData.put("color_check", "0");

                    MyConstants.orderData.put("clearity_start_name", clearity_start_name);
                    MyConstants.orderData.put("clearity_end_name", clearity_end_name);
                    MyConstants.orderData.put("color_start_name", color_start_name);
                    MyConstants.orderData.put("color_end_name", color_end_name);

                    MyConstants.imagePath.clear();
                    MyConstants.imagePath = imagePath;
                    MyConstants.imagePath.remove(0);


                    if (!originalParts.equalsIgnoreCase("1")) {
                        ((HomeActivity) getActivity()).setIndex(9, TAG_WATCHES_AUTHENTIC_PARTS);
                        ((HomeActivity) getActivity()).replaceFragment(null);
                    } else {
                        int image = R.drawable.band_bezel_dial_watch;

                        Bitmap bm = BitmapFactory.decodeResource(getResources(), image);
                        storeImage(bm);

                        MyConstants.orderData.put("issueImage", issueImagePath);

                        ((HomeActivity) getActivity()).setIndex(4, TAG_ORDER_DETAIL);
                        ((HomeActivity) getActivity()).replaceFragment(null);
                    }
                }

            /*new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 100ms
                    ((HomeActivity) getActivity()).setIndex(9, TAG_WATCHES_AUTHENTIC_PARTS);
                    ((HomeActivity) getActivity()).replaceFragment(null);
                }
            }, 1000);*/
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d(TAG,
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();

            issueImagePath = pictureFile.getAbsolutePath();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }

    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath() + "/Federal_Appraisal/Images");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmssSSS").format(new Date());
        File mediaFile;
        String mImageName = "Watch_" + timeStamp + ".png";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    public void selectImage() {
        try {
            final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
            //final CharSequence[] options = {"Take Photo"};
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
            builder.setTitle("Select Option");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        dialog.dismiss();

                        File f = new File(Environment.getExternalStorageDirectory() + "/Federal_Appraisal/Images");
                        if (!f.exists()) {
                            f.mkdirs();
                        }
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File file = new File(Environment.getExternalStorageDirectory(), "/Federal_Appraisal/Images" + System.currentTimeMillis() + ".jpg");
                        selectedUri = Uri.fromFile(file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                        startActivityForResult(intent, PICK_IMAGE_CAMERA);

                    }
                    if (options[item].equals("Choose From Gallery")) {
                        dialog.dismiss();
                        Intent i = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, PICK_IMAGE_GALLERY);
                    }
                }
            });
            builder.show();

        } catch (Exception e) {
            Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PICK_IMAGE_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //openCameraIntent();
                    selectImage();
                } else {
                    Toast.makeText(getActivity(), "The app was not allowed to write in your storage", Toast.LENGTH_LONG).show();
                }
            }
            case PICK_IMAGE_GALLERY: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //openCameraIntent();
                    selectImage();
                } else {
                    Toast.makeText(getActivity(), "The app was not allowed to write in your storage", Toast.LENGTH_LONG).show();
                }
            }
        }
    }


//    public void loadImageFromGallery() {
//        // Create intent to Open Image applications like Gallery, Google Photos
//        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        // Start the Intent
//        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        try {
//            // When an Image is picked
//            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
//                    && null != data) {
//                // Get the Image from data
//
//                Uri selectedImage = data.getData();
//                String[] filePathColumn = {MediaStore.Images.Media.DATA};
//
//                // Get the cursor
//                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
//                        filePathColumn, null, null, null);
//                // Move to first row
//                int columnIndex = 0;
//                if (cursor != null) {
//                    cursor.moveToFirst();
//                    columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//                    filePathFirst = cursor.getString(columnIndex);
//                    System.out.println(filePathFirst);
//                    //imagePath.add(filePathFirst);
//                    cursor.close();
//
//                    //mentionQueryAdapter.notifyDataSetChanged();
//                    //drQuestionnaire3Adapter.notifyDataSetChanged();
//                }
//
//                doCropDP(filePathFirst);
//
//            }
//
//            if (requestCode == RESULT_CROP_DP) {
//                if (resultCode == RESULT_OK) {
//                    if (!TextUtils.isEmpty(filePathDp)) {
//
//                        Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePathDp,100,100);
//                        System.out.println("selectedBitmap:::::" + selectedBitmap);
//
//                        for (int i = 0; i < imagePath.size(); i++) {
//                            System.out.println("image list::"+ imagePath.get(i));
//                        }
//
//                        // Set The Bitmap Data To ImageView
//                        imagePath.add(filePathDp);
//                        //ivUser.setImageBitmap(selectedBitmap);
//                        //ivPageDp.setScaleType(ImageView.ScaleType.FIT_XY);
//                        watchesAdapter.notifyDataSetChanged();
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public String getPath(Uri uri) {
        File myFile = new File(uri.getPath());
        myFile.getAbsolutePath();
        return myFile.getAbsolutePath();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_CAMERA) {
            filePathFirst = getPath(selectedUri);
            System.out.println("image path::::" + filePathFirst);
            System.out.println("uri:::" + selectedUri);

            try {
                //  BitmapFactory.decodeFile(filePathFirst);
                imagePath.add(filePathFirst);
                watchesAdapter.notifyDataSetChanged();

                for (int i = 0; i < imagePath.size(); i++) {
                    System.out.println("image list::" + imagePath.get(i));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (PICK_IMAGE_GALLERY == 2 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            filePathFirst = cursor.getString(columnIndex);
            Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePathFirst, 100, 100);
            imagePath.add(filePathFirst);
            // ivDoc.setImageBitmap(BitmapFactory.decodeFile(imgPath));
            watchesAdapter.notifyDataSetChanged();

            for (int i = 0; i < imagePath.size(); i++) {
                System.out.println("image list::" + imagePath.get(i));
            }
            cursor.close();
        }
    }

    private void doCropDP(String picPath) {
        try {

            Intent cropIntent = new Intent("com.android.camera.action.CROP");

            File f = new File(picPath);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");

            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", false);

            File sdCardDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/Federal_Appraisal/Images");

            if (!sdCardDirectory.exists()) {
                sdCardDirectory.mkdirs();
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                    .format(new Date());

            String nw = "Image_" + timeStamp + ".jpeg";

            File image = new File(sdCardDirectory, nw);

            //uploadPathDp = sdCardDirectory + "/" + nw;
            filePathDp = new File(sdCardDirectory, nw).getAbsolutePath();
            System.out.println("UploadPath:::" + filePathDp);

            try {
                image.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }

            Uri uri = Uri.fromFile(image);

            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP_DP);
        } catch (ActivityNotFoundException anfe) {
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
