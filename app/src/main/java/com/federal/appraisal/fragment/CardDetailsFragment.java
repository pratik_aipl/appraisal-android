package com.federal.appraisal.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.federal.appraisal.R;
import com.federal.appraisal.adapter.CardDetailsAdpater;

public class CardDetailsFragment extends Fragment {

    public static TabLayout tabDetails;
    ViewPager viewPager;
    public static CardDetailsAdpater cardDetailsAdpater;
    String orderId, appraisalCost, shipping, shippingMethod;
    float total;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_card_details, container, false);

        Bundle bundle = getArguments();
        if (bundle != null) {
            orderId = bundle.getString("order_id");
            total = Float.parseFloat(bundle.getString("total"));
            appraisalCost = bundle.getString("appraisal_cost");
            shipping = bundle.getString("shipping");
            shippingMethod = bundle.getString("shipping_method");
        }

        viewPager = view.findViewById(R.id.pager_card_details);
        tabDetails = view.findViewById(R.id.tab_card_detail);

        SetupViewPager(viewPager);
        viewPager.setOffscreenPageLimit(2);
        tabDetails.setupWithViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0)
                    tabDetails.getTabAt(1).setText("Add New");
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return view;
    }

    public void SetupViewPager(ViewPager viewPager) {

        Bundle bundle = new Bundle();
        bundle.putString("total", String.valueOf(total));
        bundle.putString("order_id", orderId);
        bundle.putString("appraisal_cost", String.valueOf(appraisalCost));
        bundle.putString("shipping", String.valueOf(shipping));
        bundle.putString("shipping_method", shippingMethod);

        SavedCardList savedCardList = new SavedCardList();
        savedCardList.setArguments(bundle);
        CheckOutFragment checkOutFragment = new CheckOutFragment();
        checkOutFragment.setArguments(bundle);
        cardDetailsAdpater = new CardDetailsAdpater(getFragmentManager());
        cardDetailsAdpater.addFragment(savedCardList, "Saved");
        cardDetailsAdpater.addFragment(checkOutFragment, "Add New");

        viewPager.setAdapter(cardDetailsAdpater);

    }

}
