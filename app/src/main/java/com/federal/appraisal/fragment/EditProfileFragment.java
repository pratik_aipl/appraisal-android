package com.federal.appraisal.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.federal.appraisal.BuildConfig;
import com.federal.appraisal.R;
import com.federal.appraisal.activity.HomeActivity;
import com.federal.appraisal.animation.MyBounceInterpolator;
import com.federal.appraisal.others.App;
import com.federal.appraisal.others.Internet;
import com.federal.appraisal.ws.AsyncTaskListner;
import com.federal.appraisal.ws.CallRequest;
import com.federal.appraisal.ws.Constant;
import com.federal.appraisal.ws.MyConstants;
import com.federal.appraisal.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileFragment extends Fragment implements AsyncTaskListner {


    private EditText etFirstName, etLastName, etEmail, etOldPassword, etNewPassword, etConfirmPassword;
    private RadioGroup rgNotifications;
    private RadioButton rbYes, rbNo;
    private Button btnUpdate;
    private String notification = "1";
    private SharedPreferences sharedpreferences;

    public EditProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);

        try {
            ((HomeActivity) getActivity()).setFragmentTitle(7);
            ((HomeActivity) getActivity()).setIndex(7, HomeActivity.TAG_EDIT_PROFILE);

            sharedpreferences = getActivity().getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

            etFirstName = view.findViewById(R.id.etFirstName);
            etLastName = view.findViewById(R.id.etLastName);
            etEmail = view.findViewById(R.id.etEmail);
            etOldPassword = view.findViewById(R.id.etOldPassword);
            etNewPassword = view.findViewById(R.id.etNewPassword);
            etConfirmPassword = view.findViewById(R.id.etConfirmPassword);
            rgNotifications = view.findViewById(R.id.rgNotification);
            rbYes = view.findViewById(R.id.rbYes);
            rbNo = view.findViewById(R.id.rbNo);
            btnUpdate = view.findViewById(R.id.btnUpdate);

            getUserProfile();

            rgNotifications.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                    RadioButton checkedRadioButton = (RadioButton) radioGroup.findViewById(checkedId);

                    if (checkedRadioButton == rbYes) {
                        notification = "1";
                    } else {
                        notification = "0";
                    }
                }
            });

            btnUpdate.setClickable(true);
            btnUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String firstName = etFirstName.getText().toString().trim();
                    String lastName = etLastName.getText().toString().trim();
                    String email = etEmail.getText().toString().trim();
                    String oldPassword = etOldPassword.getText().toString().trim();
                    String newPassword = etNewPassword.getText().toString().trim();
                    String confirmPassword = etConfirmPassword.getText().toString().trim();

                    if (TextUtils.isEmpty(firstName)) {
                        etFirstName.requestFocus();
                        etFirstName.setError("First name is required");
                    } else if (TextUtils.isEmpty(lastName)) {
                        etLastName.requestFocus();
                        etLastName.setError("Last name is required");
                    } else if (!Utils.isValidEmail(email)) {
                        etEmail.requestFocus();
                        etEmail.setError("Email is not valid");
                    } else {
                        btnUpdate.setClickable(false);

                        final Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);

                        // Use bounce interpolator with amplitude 0.2 and frequency 20
                        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                        myAnim.setInterpolator(interpolator);

                        btnUpdate.startAnimation(myAnim);

                        if (TextUtils.isEmpty(oldPassword)) {
                            oldPassword = "";
                        }
                        if (TextUtils.isEmpty(newPassword)) {
                            newPassword = "";
                        }
                        if (TextUtils.isEmpty(confirmPassword)) {
                            confirmPassword = "";
                        }

                        editUserProfile(firstName, lastName, email, oldPassword, newPassword, confirmPassword);
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    private void editUserProfile(String firstName, String lastName, String email, String oldPassword, String newPassword, String confirmPassword) {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
            btnUpdate.setClickable(true);
        } else {

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "profile");
            map.put("header", "");
            map.put("token", App.user.getToken());
            map.put("user_id", App.user.getUserID());
            map.put("first_name", firstName);
            map.put("last_name", lastName);
            map.put("email", email);
            map.put("notification", notification);
            map.put("old_password", oldPassword);
            map.put("new_password", newPassword);
            map.put("confirm_new_password", confirmPassword);

            new CallRequest(EditProfileFragment.this).editUserProfile(map);
        }
    }

    private void getUserProfile() {
        if (!Internet.isAvailable(getActivity())) {
            Internet.showAlertDialog(getActivity(), "Error!", "Required Internet Connection", false);
        } else {

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "profile?");
            map.put("header", "");
            map.put("token", App.user.getToken());
            map.put("user_id", App.user.getUserID());

            new CallRequest(EditProfileFragment.this).getUserProfile(map);
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case getUserProfile:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {
                                //Utils.showToast(mainObj.getString("message"), LoginActivity.this);

                                JSONObject resultObj = mainObj.getJSONObject("data");
                                String first_name = resultObj.getString("first_name");
                                String last_name = resultObj.getString("last_name");
                                String email = resultObj.getString("email");
                                String notification = resultObj.getString("notification");

                                etFirstName.setText(first_name);
                                etLastName.setText(last_name);
                                etEmail.setText(email);

                                if (notification.equalsIgnoreCase("1")) {
                                    rbYes.setChecked(true);
                                } else {
                                    rbNo.setChecked(true);
                                }

                            } else {
                                Utils.showToast(mainObj.getString("message"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case editUserProfile:
                        btnUpdate.setClickable(true);
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {
                                Utils.showToast("Profile updated successfully", getActivity());

                                SharedPreferences.Editor editor = sharedpreferences.edit();

                                String name = etFirstName.getText().toString().trim() + " " + etLastName.getText().toString().trim();

                                editor.putString(MyConstants.USER_EMAIL, etEmail.getText().toString().trim());
                                editor.putString(MyConstants.FIRST_NAME, etFirstName.getText().toString().trim());
                                editor.putString(MyConstants.LAST_NAME, etLastName.getText().toString().trim());
                                editor.putString(MyConstants.NAME, name);
                                editor.commit();

                                ((HomeActivity) getActivity()).loadNavHeader();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Do something after 100ms
                                        getActivity().getSupportFragmentManager().popBackStack();
                                    }
                                }, 1000);


                                /*JSONObject resultObj = mainObj.getJSONObject("data");
                                String first_name = resultObj.getString("first_name");
                                String last_name = resultObj.getString("last_name");
                                String email = resultObj.getString("email");
                                String notification = resultObj.getString("notification");

                                etFirstName.setText(first_name);
                                etLastName.setText(last_name);
                                etEmail.setText(email);

                                if (notification.equalsIgnoreCase("1")){
                                    rbYes.setSelected(true);
                                } else {
                                    rbNo.setSelected(true);
                                }*/

                            } else {
                                Utils.showToast(mainObj.getString("message"), getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
