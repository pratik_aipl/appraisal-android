package com.federal.appraisal.ws;

public class Constant {
    public enum POST_TYPE {
        GET, POST, POST_WITH_IMAGE;
    }


    public enum TABS {
        DR_LIST, HISTORY, PROFILE, MESSAGES, HEALTH, NOTIFICATIONS

    }

    public enum REQUESTS {
        login, logout, getUserProfile, editUserProfile, addSupport, forgotPassword, getAllData, getShippingMethod, addOrder, getAppraisalList,
        orderPayment, getCommentList, addComment, getSavedCard, addUserCard, removeSavedCard
    }

}