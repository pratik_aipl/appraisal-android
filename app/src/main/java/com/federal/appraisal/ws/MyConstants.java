package com.federal.appraisal.ws;

import com.federal.appraisal.BuildConfig;
import com.federal.appraisal.model.AllMaterial;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyConstants {

    //  http://federal.blenzabi.com/api/v1/web_services/

    // live URLs
    public static final String MAIN_URL_OLD = "https://federal.blenzabi.com/api/v1/web_services/";
    public static final String BASE_URL_OLD = MAIN_URL_OLD + "api/v1/web_services/";
    public static final String MAIN_URL = "https://faaausa.com/api/v1/web_services/";
    //    public static final String BASE_URL = MAIN_URL + "beta/api/v1/web_services/";
    //    public static final String BASE_URL = MAIN_URL + "api/v1/web_services/";
    public static final String BASE_URL = BASE_URL_OLD;

    // for  Stripe
    //0--->Test key
    //1--->Production key
    public static String PAYMENT_CONFIG_ENVIRONMENT = "0";

    // for  Stripe
    //0--->Test key
    //1--->Production key
    public static String STRIPE_PUBLISH_KEY_LIVE = "pk_live_n9dZgqsWUMjSJjx6SB5GEl4Q";
    //    public static String STRIPE_PUBLISH_KEY_LIVE = "pk_live_qcRmzopjw213yPDc3gl5K6zx";
    public static String STRIPE_PUBLISH_KEY_TEST = "pk_test_d1VHmq3QUwqKIKyrTVkOGozA";
    //    public static String STRIPE_SECRET_KEY_LIVE = "sk_live_2IhapWHx4Le2SEJsYVTBpBcx";
    public static String STRIPE_SECRET_KEY_TEST = "sk_test_8tWp3SdPGDrj3cNSnU0Ysefe";
    public static String STRIPE_SECRET_KEY_LIVE = "sk_live_hwRAqWmkEMEdk2F73HfUJK3c";

    public static String DEVICE_ID;
    public static String ONESIGNAL_ID;

    public static AllMaterial allMaterial;
    public static HashMap<String, String> orderData = new HashMap<>();
    public static List<String> imagePath = new ArrayList<>();
    public static boolean paymentSuccess = false;
    public static String errorPayment;
    public static String certificateNumber;
    public static String totalAmount;
    public static String orderType;
    public static String shapeId;
    public static int shapePos;
    public static int category;
    public static String commentImage;
    public static String commentUploadImage;

    public static final String NAME = "NAME";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String PROFILE_PIC = "profile_pic";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String USER_ID = "user_id";
    public static final String USER_EMAIL = "user_email";
    public static final String TOKEN = "token";
    public static final String ROLE_ID = "role_id";
    public static final String ROLE_NAME = "role_name";
    public static final String STRIPE_ID = "stripe_id";

    public static final String PREF = "Federal_Appraisal";

}
