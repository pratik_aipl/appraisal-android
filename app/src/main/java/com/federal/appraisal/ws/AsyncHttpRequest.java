package com.federal.appraisal.ws;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;

import com.federal.appraisal.R;
import com.federal.appraisal.activity.LoginActivity;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Sagar Sojitra on 3/8/2016.
 */
public class AsyncHttpRequest extends AsyncTask<Map<String, String>, Void, String> {

    public Fragment ft;
    public Constant.REQUESTS request;
    public Map<String, String> map;
    public AsyncTaskListner aListner;
    public Constant.POST_TYPE post_type;
    public String chatId = "";
    public boolean header = false;
    public String token, user_id;
    public Context ct;
    public SharedPreferences sharedpreferences;
    public SharedPreferences.Editor editor;

    public AsyncHttpRequest(Fragment ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ft = ft;
        this.ct = ft.getActivity();
        this.request = request;
        this.map = map;
        this.aListner = (AsyncTaskListner) ft;
        this.post_type = post_type;
   //     this.map.put("unused", System.currentTimeMillis() + "");

        sharedpreferences = ct.getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        if (map.containsKey("chatId")) {
            chatId = map.get("chatId");
            map.remove("chatId");
        }
        if (Utils.isNetworkAvailable(ft.getActivity())) {

            if (!map.containsKey("show")) {
                Utils.showSimpleSpinProgressDialog(ft.getActivity(), "Please Wait..");
            }

            if (map.containsKey("header")) {
                header = true;
                token = map.get("token");
                user_id = map.get("user_id");
            }


            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showToast("No Internet, Please try again later", ft.getActivity());
        }
    }

    public AsyncHttpRequest(Context ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ct = ft;
        this.request = request;
        this.map = map;
        this.aListner = (AsyncTaskListner) ft;
        this.post_type = post_type;

        sharedpreferences = ct.getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

//        if (!map.containsKey("ip")) {
//            this.map.put("unused", System.currentTimeMillis() + "");
//        } else {
//            map.remove("ip");
//        }

        if (map.containsKey("chatId")) {
            chatId = map.get("chatId");
            map.remove("chatId");
        }


        if (Utils.isNetworkAvailable(ct)) {
            if (!map.containsKey("show")) {
                Utils.showSimpleSpinProgressDialog(ft, "Please Wait..");
            }

            if (map.containsKey("header")) {
                header = true;
                token = map.get("token");
                user_id = map.get("user_id");
            }

            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showToast("No Internet, Please try again later", ct);
        }
    }

    @Override
    protected String doInBackground(Map<String, String>... params) {
        return null;
    }

    public DefaultHttpClient HostVerifier() {
        HostnameVerifier hostnameVerifier = SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

        DefaultHttpClient client = new DefaultHttpClient();

        SchemeRegistry registry = new SchemeRegistry();
        SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
        socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
        registry.register(new Scheme("https", socketFactory, 443));
        SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
        DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());

// Set verifier
        HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
        return httpClient;
    }

    public DefaultHttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }



    public boolean matchKeysForImages(String key) {

        if(Pattern.compile(Pattern.quote("image"), Pattern.CASE_INSENSITIVE).matcher(key).find()){
            return  true;
        }

        if (key.contains("image") ||
                key.equalsIgnoreCase("profile_picture") ||
                key.equalsIgnoreCase("photo_id") ||
                key.equalsIgnoreCase("Image") ||
                key.equalsIgnoreCase("profile_pic") ||
                key.equalsIgnoreCase("ProfilePic")) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * Method to write ascii text characters to file on SD card. Note that you must add a
     * WRITE_EXTERNAL_STORAGE permission to the manifest file or this method will throw
     * a FileNotFound Exception because you won't have write permission.
     */



    class MyRequest extends AsyncTask<Map<String, String>, Integer, String>

    {
        MyCustomMultiPartEntity reqEntity;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected String doInBackground(Map<String, String>... map) {
            System.out.println("::urls ::" + map[0].get("url"));

            final String basicAuth = "Basic " + Base64.encodeToString("admin:API@FEDERAL!#$WEB$".getBytes(), Base64.NO_WRAP);

            String responseBody = "";
            try {
                switch (post_type) {
                    case GET:
                        String tempData = "";
                        DefaultHttpClient httpClient = getNewHttpClient();
                        String query = map[0].get("url");
                        System.out.println();
                        tempData += "\n\n\n\n URL : " + map[0].get("url");

                        map[0].remove("url");


                        List<String> values = new ArrayList<String>(map[0].values());


                        List<String> keys = new ArrayList<String>(map[0].keySet());

                        for (int i = 0; i < values.size(); i++) {
                            System.out.println();
                            System.out.println(keys.get(i) + "====>" + values.get(i));
                            query = query + keys.get(i) + "=" + values.get(i);
                            tempData += "\n" + keys.get(i) + "====>" + values.get(i);
                            if (i < values.size() - 1) {
                                query += "&";
                            }
                        }


                        System.out.println("URL" + "====> " + query);
                        System.out.println("BasicAuth ::: "+ basicAuth);
                        HttpGet httpGet = new HttpGet(query);
                        httpGet.setHeader("Authorization", basicAuth);
                        httpGet.addHeader("X-FEDERAL-API-KEY", "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss");
                        if (header) {
                            httpGet.addHeader("X-FEDERAL-LOGIN-TOKEN", token);
                            httpGet.addHeader("USER-ID", user_id);
                        }
                        HttpResponse httpResponse = httpClient.execute(httpGet);

                        int respCode = httpResponse.getStatusLine().getStatusCode();
                        HttpEntity httpEntity = httpResponse.getEntity();


                        System.out.println("Response code:::"+ respCode);
                        if (respCode == 401) {
                            return "401";
                        } else {
                            responseBody = EntityUtils.toString(httpEntity);
                            System.out.println("Response" + "====>" + responseBody);
                            tempData += "\n" + "Response" + "====>" + responseBody;
                            //writeToSDFile(tempData);
                            return responseBody;
                        }

                    case POST:
                        tempData = "";
                        System.out.println("new Requested URL >> " + map[0].get("url"));
                        tempData += "\n\n\n\n URL : " + map[0].get("url");
                        HttpPost postRequest = new HttpPost(map[0].get("url"));
                        postRequest.setHeader("Authorization", basicAuth);
                        postRequest.addHeader("X-FEDERAL-API-KEY", "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss");
                        if (header) {
                            postRequest.addHeader("X-FEDERAL-LOGIN-TOKEN", token);
                            postRequest.addHeader("USER-ID", user_id);
                        }
                        httpClient = getNewHttpClient();
                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                        for (String key : map[0].keySet()) {
                            System.out.println(key + "====>" + map[0].get(key));
                            tempData += "\n" + key + "====>" + map[0].get(key);
                            nameValuePairs.add(new BasicNameValuePair(key, map[0].get(key)));
                        }
                        postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                        System.out.println("Final URI::" + postRequest.getEntity().toString());
                        HttpResponse response = httpClient.execute(postRequest);

                      //  writeToSDFile(tempData);

                        respCode = response.getStatusLine().getStatusCode();

                        System.out.println("Response code:::"+ respCode);

                        if (respCode == 401) {

                            return "401";
                        } else {

                            httpEntity = response.getEntity();
                            responseBody = EntityUtils.toString(httpEntity);
                            tempData += "\n" + "Response" + "====>" + responseBody;
                            return responseBody;
                        }
                        //
                        // return responseBody;


                    case POST_WITH_IMAGE:

                        String charset = "UTF-8";
                        try {
                            httpClient = getNewHttpClient();
                            postRequest = new HttpPost(map[0].get("url"));
                            postRequest.setHeader("Authorization", basicAuth);
                            postRequest.addHeader("X-FEDERAL-API-KEY", "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss");
                            if (header) {
                                postRequest.addHeader("X-FEDERAL-LOGIN-TOKEN", token);
                                postRequest.addHeader("USER-ID", user_id);
                            }
                            reqEntity = new MyCustomMultiPartEntity(new MyCustomMultiPartEntity.ProgressListener() {
                                @Override
                                public void transferred(long num) {
                                }
                            });
                            BasicHttpContext localContext = new BasicHttpContext();
                            for (String key : map[0].keySet()) {
                                System.out.println();
                                System.out.println(key + "====>" + map[0].get(key));
                                if (matchKeysForImages(key)) {
                                    if (map[0].get(key) != null) {
                                        if (map[0].get(key).length() > 1) {
                                            System.out.println("in inner if");
                                            String type = "image/png";
                                            File f = new File(map[0].get(key));
                                            FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                            reqEntity.addPart(key, fbody);
                                        }
                                    }
                                } else {
                                    if (map[0].get(key) == null) {
                                        reqEntity.addPart(key, new StringBody(""));
                                    } else {
                                        reqEntity.addPart(key, new StringBody(map[0].get(key)));
                                    }
                                }
                            }
                            postRequest.setEntity(reqEntity);
                            HttpResponse responses = httpClient.execute(postRequest, localContext);
                            BufferedReader reader = new BufferedReader(new InputStreamReader(responses.getEntity().getContent(),"UTF-8"));

                            respCode = responses.getStatusLine().getStatusCode();

                            System.out.println("Response code:::"+ respCode);

                            if (respCode == 401) {

                                return "401";
                            } else {

                                String sResponse;
                                while ((sResponse = reader.readLine()) != null) {
                                    responseBody = responseBody + sResponse;
                                }
                                System.out.println("Response ::" + responseBody);
                                return responseBody;
                            }

                        } catch (IOException e) {
                            Log.d("ExPostActivity", e.getMessage());
                        }
                }

            } catch (Exception e) {
                e.printStackTrace();
                aListner.onTaskCompleted(null, request);
            }
            return null;

        }

        @Override
        protected void onPostExecute(String result) {
            try {
                if (result.equals("401")) {

                    System.out.println("logout:::::::::::::::");

                    editor = sharedpreferences.edit();
                    editor.clear();
                    editor.commit();

                    Intent intent = new Intent(ct, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ct.startActivity(intent);

                } else {
                    if (!chatId.isEmpty()) {
                        aListner.onProgressComplete(chatId, result, request);
                    } else {
                        aListner.onTaskCompleted(result, request);
                    }
                }
                super.onPostExecute(result);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}
