package com.federal.appraisal.ws;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.federal.appraisal.others.App;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;

public class CallRequest {

    public App app;
    public Context ct;
    public Fragment ft;
    public static String version = "1";

    public CallRequest(Fragment ft) {
        app = App.getInstance();
        this.ft = ft;
        this.ct = ft.getActivity();
    }

    public CallRequest(Context ct) {
        this.ct = ct;
        app = App.getInstance();
    }

    public CallRequest() {

    }

    public void submitTest() {

        String array = " [" +
                "{" +
                "AnswerdQueOptionID : 12;" +
                "MarkedQueOptionID : 0;" +
                "TestDTLID : 3301;" +
                "}," +
                "{" +
                "AnswerdQueOptionID : 80;" +
                "MarkedQueOptionID : 1;" +
                "TestDTLID : 3302;" +
                "}" +
                "];";

        Map<String, String> map = new HashMap<String, String>();

        map.put("url", "http://blenzabi.com/takeoff/api/v1/web_services/submit_test");
        map.put("header", "");
        map.put("token", "cwgkc0o8wk40wcowkkwcgwook4so8wgw");
        map.put("user_id", "8");
        map.put("test_header_id", "35");
        map.put("detail_array", array);
        new AsyncHttpRequest(ct, Constant.REQUESTS.addComment, Constant.POST_TYPE.POST, map);
    }

    public void login(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.login, Constant.POST_TYPE.POST, map);
    }

    public void logout(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.logout, Constant.POST_TYPE.POST, map);
    }

    public void getUserProfile(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getUserProfile, Constant.POST_TYPE.GET, map);
    }

    public void editUserProfile(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.editUserProfile, Constant.POST_TYPE.POST, map);
    }

    public void addSupport(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.addSupport, Constant.POST_TYPE.POST, map);
    }

    public void forgotPassword(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.forgotPassword, Constant.POST_TYPE.POST, map);
    }

    public void getAllData(Map<String, String> map) {
        new AsyncHttpRequest(ct, Constant.REQUESTS.getAllData, Constant.POST_TYPE.GET, map);
    }

    public void getShippingMethod(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getShippingMethod, Constant.POST_TYPE.GET, map);
    }

    public void getAppraisalList(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getAppraisalList, Constant.POST_TYPE.GET, map);
    }

    public void getCommentList(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getCommentList, Constant.POST_TYPE.GET, map);
    }

    public void addComment(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.addComment, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void addOrder(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.addOrder, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void orderPayment(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.orderPayment, Constant.POST_TYPE.POST, map);
    }

    public void getSavedCard(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.getSavedCard, Constant.POST_TYPE.GET, map);
    }

    public void addUserCard(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.addUserCard, Constant.POST_TYPE.POST, map);
    }

    public void removeSavedCard(Map<String, String> map) {
        new AsyncHttpRequest(ft, Constant.REQUESTS.removeSavedCard, Constant.POST_TYPE.POST, map);
    }
}