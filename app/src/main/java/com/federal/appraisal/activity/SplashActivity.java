package com.federal.appraisal.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.federal.appraisal.R;
import com.federal.appraisal.custom_views.CustomTextView;
import com.federal.appraisal.ws.MyConstants;
import com.onesignal.OneSignal;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.Calendar;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = "SplashActivity";
    private static int SPLASH_TIME_OUT = 1500;
    protected RxPermissions rxPermissions;
    CustomTextView mYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        mYear = findViewById(R.id.mYear);
        rxPermissions = new RxPermissions(this);
        /*OneSignal.idsAvailable( new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null)
                    MyConstants.ONESIGNAL_ID = userId;
                Log.d("debug", "User:" + userId);
            *//*    if (registrationId != null)
                    MyConstants.DEVICE_ID = registrationId;*//*
                Log.d("debug", "registrationId:" + registrationId);
            }
        });


        System.out.println("one sinal id:::"+MyConstants.ONESIGNAL_ID);*/
        int year = Calendar.getInstance().get(Calendar.YEAR);
        Log.d(TAG, "onCreate: " + year);
        mYear.setText(getString(R.string.copyright_d, year));
        MyConstants.DEVICE_ID = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .subscribe(granted -> {
                    if (granted) {
                        getCompleted();
                    } else {
                        // Oops permission denied
                    }
                });
    }

    private void getCompleted() {
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

}
