package com.federal.appraisal.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.anthonycr.grant.PermissionsManager;
import com.anthonycr.grant.PermissionsResultAction;
import com.federal.appraisal.BuildConfig;
import com.federal.appraisal.R;
import com.federal.appraisal.animation.MyBounceInterpolator;
import com.federal.appraisal.others.App;
import com.federal.appraisal.others.Internet;
import com.federal.appraisal.ws.AsyncTaskListner;
import com.federal.appraisal.ws.CallRequest;
import com.federal.appraisal.ws.Constant;
import com.federal.appraisal.ws.MyConstants;
import com.federal.appraisal.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements AsyncTaskListner {

    Button btnLogin;
    EditText etEmail, etPassword;
    TextView tvForgotPassword;
    TextInputLayout inputLayoutEmail, inputLayoutPassword;
    SharedPreferences sharedpreferences;

    public final static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public void checkAllPermissions() {

        PermissionsManager.getInstance().requestAllManifestPermissionsIfNecessary(this,
                new PermissionsResultAction() {
                    @Override
                    public void onGranted() {
                        // Proceed with initialization
                        System.out.println("Granted");
                    }

                    @Override
                    public void onDenied(String permission) {
                        // Notify the user that you need all of the permissions
                        System.out.println("denied");
                        checkAllPermissions();
                    }
                });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            ActivityCompat.finishAffinity(LoginActivity.this);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAllPermissions();
        }

        btnLogin = findViewById(R.id.btnLogin);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        inputLayoutEmail = findViewById(R.id.input_layout_email);
        inputLayoutPassword = findViewById(R.id.input_layout_password);

        if (BuildConfig.DEBUG) {
            if (BuildConfig.FLAVOR == "local")
                etEmail.setText("christine@blenzabi.com");
            else
                etEmail.setText("federalaausa@gmail.com");
            etPassword.setText("123456");
        }
        btnLogin.setClickable(true);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();

                if (TextUtils.isEmpty(email) || !isValidEmail(email)) {
                    etEmail.requestFocus();
                    etEmail.setError("Please enter valid email");
                } else if (TextUtils.isEmpty(password)) {
                    etPassword.requestFocus();
                    etPassword.setError("Please enter password");
                } else {

                    btnLogin.setClickable(false);

                    final Animation myAnim = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.bounce);

                    // Use bounce interpolator with amplitude 0.2 and frequency 20
                    MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                    myAnim.setInterpolator(interpolator);

                    btnLogin.startAnimation(myAnim);

                    doLogin(email, password);

                    /*new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                        }
                    }, 1000);*/
                }
            }
        });

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });

        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    btnLogin.performClick();
                    handled = true;
                }
                return handled;
            }
        });
    }

    private void doLogin(String email, String password) {
        if (!Internet.isAvailable(LoginActivity.this)) {
            Internet.showAlertDialog(LoginActivity.this, "Error!", "Required Internet Connection", false);
            btnLogin.setClickable(true);
        } else {

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "login");
            map.put("login", email);
            map.put("password", password);
            map.put("device_id", MyConstants.DEVICE_ID);
            map.put("token_id", MyConstants.ONESIGNAL_ID);

            new CallRequest(LoginActivity.this).login(map);
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case login:
                        btnLogin.setClickable(true);
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {
                                //Utils.showToast(mainObj.getString("message"), LoginActivity.this);

                                JSONObject resultObj = mainObj.getJSONObject("data");
                                setUserData(resultObj);

                            } else {
                                Utils.showToast(mainObj.getString("message"), LoginActivity.this);
                            }
                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                        break;
                }
            }else {
                btnLogin.setClickable(true);
            }
        } catch (Exception e) {
            btnLogin.setClickable(true);
            e.printStackTrace();
        }
    }

    private void setUserData(JSONObject resultObj) {

        try {
            JSONObject user = resultObj.getJSONObject("user");

            String user_id = resultObj.getString("user_id");
            String login_token = resultObj.getString("login_token");

            String role_id = user.getString("role_id");
            String first_name = user.getString("first_name");
            String last_name = user.getString("last_name");
            String email = user.getString("email");
            String RoleName = user.getString("RoleName");

            String name = first_name + " " + last_name;


            SharedPreferences.Editor editor = sharedpreferences.edit();

            editor.putString(MyConstants.USER_EMAIL, email);
            editor.putString(MyConstants.USER_ID, user_id);
            editor.putString(MyConstants.TOKEN, login_token);
            editor.putString(MyConstants.ROLE_ID, role_id);
            editor.putString(MyConstants.ROLE_NAME, RoleName);
            editor.putString(MyConstants.FIRST_NAME, first_name);
            editor.putString(MyConstants.LAST_NAME, last_name);
            editor.putBoolean(MyConstants.IS_LOGGED_IN, true);
            editor.putString(MyConstants.NAME, name);

            editor.commit();


            App.user.setUserID(user_id);
            App.user.setName(name);
            App.user.setFirstName(first_name);
            App.user.setLastName(last_name);
            App.user.setUserEmail(email);
            App.user.setToken(login_token);

            System.out.println("data saved successfully");

            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            ActivityCompat.finishAffinity(LoginActivity.this);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
