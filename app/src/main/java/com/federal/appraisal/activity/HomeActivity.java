package com.federal.appraisal.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.federal.appraisal.BuildConfig;
import com.federal.appraisal.R;
import com.federal.appraisal.fragment.CardDetailsFragment;
import com.federal.appraisal.fragment.CheckOutFragment;
import com.federal.appraisal.fragment.CommentsFragment;
import com.federal.appraisal.fragment.EditProfileFragment;
import com.federal.appraisal.fragment.EngagementRingsFragment;
import com.federal.appraisal.fragment.FileUploadFragment;
import com.federal.appraisal.fragment.HomeFragment;
import com.federal.appraisal.fragment.JewelleryFragment;
import com.federal.appraisal.fragment.MyCertificatesFragment;
import com.federal.appraisal.fragment.OrderConfirmFragment;
import com.federal.appraisal.fragment.OrderDetailFragment;
import com.federal.appraisal.fragment.PaymentFragment;
import com.federal.appraisal.fragment.StonesAuthenticPartsFragment;
import com.federal.appraisal.fragment.SupportFragment;
import com.federal.appraisal.fragment.WatchesAuthenticPartsFragment;
import com.federal.appraisal.fragment.WatchesFragment;
import com.federal.appraisal.fragment.WebLinkFragment;
import com.federal.appraisal.model.AllMaterial;
import com.federal.appraisal.others.App;
import com.federal.appraisal.others.Internet;
import com.federal.appraisal.ws.AsyncTaskListner;
import com.federal.appraisal.ws.CallRequest;
import com.federal.appraisal.ws.Constant;
import com.federal.appraisal.ws.MyConstants;
import com.federal.appraisal.ws.Utils;
import com.onesignal.OneSignal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeActivity extends AppCompatActivity implements AsyncTaskListner {
    private static final String TAG = "HomeActivity";
    public static final String TAG_HOME = "home";
    public static final String TAG_WEBLINKS = "weblinks";
    public static final String TAG_ORDER_DETAIL = "order_detail";
    public static final String TAG_PAYMENT = "payment";
    public static final String TAG_ORDER_CONFIRM = "order_confirm";
    public static final String TAG_EDIT_PROFILE = "edit_profile";
    public static final String TAG_WATCHES = "watches";
    public static final String TAG_WATCHES_AUTHENTIC_PARTS = "watches_authentic_parts";
    public static final String TAG_JEWELLERY = "jewellery";
    public static final String TAG_ENGAGEMENT_RINGS = "engagement_rings";
    public static final String TAG_STONE_AUTHENTIC_PARTS = "stone_authentic_parts";
    public static final String TAG_COMMENTS = "comments";
    public static final String TAG_FILE_UPLOAD = "file_upload";
    public static final String TAG_CHECK_OUT = "check_out";
    public static final String TAG_CARDDETAIL = "card_detail";
    public static final String TAG_CERTIFICATES = "certificates";
    public static final String TAG_SUPPORT = "support";
    // index to identify current nav menu item
    public static int navItemIndex = 0;
    public static String CURRENT_TAG = TAG_HOME;
    Toolbar toolbar;
    SharedPreferences sharedpreferences;
    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;
    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private ImageView ivUser;
    private TextView tvUserName, tvEmail,mVersion;
    private LinearLayout llLogOut;
    private SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));

        sharedpreferences = getSharedPreferences(MyConstants.PREF, Context.MODE_PRIVATE);

        if (sharedpreferences.getBoolean(MyConstants.IS_LOGGED_IN, false)) {
            App.user.setUserID(sharedpreferences.getString(MyConstants.USER_ID, ""));
            App.user.setName(sharedpreferences.getString(MyConstants.NAME, ""));
            App.user.setFirstName(sharedpreferences.getString(MyConstants.FIRST_NAME, ""));
            App.user.setLastName(sharedpreferences.getString(MyConstants.LAST_NAME, ""));
            App.user.setUserEmail(sharedpreferences.getString(MyConstants.USER_EMAIL, ""));
            App.user.setToken(sharedpreferences.getString(MyConstants.TOKEN, ""));
        }

        mHandler = new Handler();

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null)
                    MyConstants.ONESIGNAL_ID = userId;
                Log.d("debug", "User:" + userId);
            /*    if (registrationId != null)
                    MyConstants.DEVICE_ID = registrationId;*/
                Log.d("debug", "registrationId:" + registrationId);
            }
        });

        System.out.println("one sinal id:::" + MyConstants.ONESIGNAL_ID);

        MyConstants.DEVICE_ID = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navHeader = navigationView.getHeaderView(0);
        llLogOut = navigationView.findViewById(R.id.llLogOut);
        mVersion = navigationView.findViewById(R.id.mVersion);

        llLogOut.setOnClickListener(view -> {
            drawer.closeDrawers();
            new AlertDialog.Builder(HomeActivity.this)
                    .setTitle("Alert!")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage("Would you like to logout?")
                    .setPositiveButton("Yes", (dialog, which) -> {
                        dialog.dismiss();
                        doLogOut();
                    })
                    .setNegativeButton("No", (dialog, which) -> dialog.dismiss())
                    .show();
        });

        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        // load nav menu header data
        loadNavHeader();

        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
        }

        if (MyConstants.allMaterial == null) {
            getAllData();
        }

    }

    private void getAllData() {

        if (!Internet.isAvailable(HomeActivity.this)) {
            final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(HomeActivity.this).create();

            // Setting Dialog Title
            alertDialog.setTitle("Error!");

            // Setting Dialog Message
            alertDialog.setMessage("Required Internet Connection");

            // Setting alert dialog icon
            //    alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

            // Setting OK Button
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    getAllData();
                }
            });
            // Showing Alert Message
            alertDialog.show();
            //Internet.showAlertDialog(HomeActivity.this, "Error!", "Required Internet Connection", false);
        } else {

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "type?");
            map.put("header", "");
            map.put("token", App.user.getToken());
            map.put("user_id", App.user.getUserID());

            new CallRequest(HomeActivity.this).getAllData(map);
        }

    }

    private void doLogOut() {

        if (!Internet.isAvailable(HomeActivity.this)) {
            Internet.showAlertDialog(HomeActivity.this, "Error!", "Required Internet Connection", false);
        } else {

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "logout");
            map.put("header", "");
            map.put("token", App.user.getToken());
            map.put("user_id", App.user.getUserID());
            map.put("device_id", MyConstants.DEVICE_ID);
            map.put("token_id", MyConstants.ONESIGNAL_ID);

            new CallRequest(HomeActivity.this).logout(map);
        }

    }

    public void NavigationFocus(int navItemIndex) {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    public void loadNavHeader() {

        ivUser = navHeader.findViewById(R.id.ivUser);
        tvUserName = navHeader.findViewById(R.id.tvUserName);
        tvEmail = navHeader.findViewById(R.id.tvEmail);
        TextView tvVirsonCode = navHeader.findViewById(R.id.tvVirsonCode);
        String versionName = null;
        try {
            versionName = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        tvVirsonCode.setText("Version " + versionName);
        mVersion.setText("Version " + versionName);
        tvUserName.setText(sharedpreferences.getString(MyConstants.NAME, "Username"));
        tvEmail.setText(sharedpreferences.getString(MyConstants.USER_EMAIL, "abc@xyz.com"));

        navHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setIndex(7, TAG_EDIT_PROFILE);
                replaceFragment(null);

                drawer.closeDrawers();
            }
        });

        invalidateOptionsMenu();

    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    public void loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            //return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                if (navItemIndex != 0) {
                    fragmentTransaction.addToBackStack(null);
                }
                fragmentTransaction.commit();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    public void replaceFragment(Bundle bundle) {
        setToolbarTitle();
        Fragment fragment = getHomeFragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        shouldLoadHomeFragOnBackPress = true;
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText && !v.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                hideKeyboard(this);
        }
        return super.dispatchTouchEvent(ev);
    }

    public static void hideKeyboard(Activity activity) {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }
    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // home
                HomeFragment homeFragment = new HomeFragment();
                return homeFragment;
            case 1:
                // photos
                MyCertificatesFragment myCertificatesFragment = new MyCertificatesFragment();
                return myCertificatesFragment;
            case 2:
                // movies fragment
                SupportFragment supportFragment = new SupportFragment();
                return supportFragment;

            case 3:
                WebLinkFragment webLinkFragment = new WebLinkFragment();
                return webLinkFragment;

            case 4:
                OrderDetailFragment orderDetailFragment = new OrderDetailFragment();
                return orderDetailFragment;

            case 5:
                PaymentFragment paymentFragment = new PaymentFragment();
                return paymentFragment;

            case 6:
                OrderConfirmFragment orderConfirmFragment = new OrderConfirmFragment();
                return orderConfirmFragment;

            case 7:
                EditProfileFragment editProfileFragment = new EditProfileFragment();
                return editProfileFragment;

            case 8:
                WatchesFragment watchesFragment = new WatchesFragment();
                return watchesFragment;

            case 9:
                WatchesAuthenticPartsFragment watchesAuthenticPartsFragment = new WatchesAuthenticPartsFragment();
                return watchesAuthenticPartsFragment;

            case 10:
                JewelleryFragment jewelleryFragment = new JewelleryFragment();
                return jewelleryFragment;

            case 11:
                EngagementRingsFragment engagementRingsFragment = new EngagementRingsFragment();
                return engagementRingsFragment;

            case 12:
                StonesAuthenticPartsFragment stonesAuthenticPartsFragment = new StonesAuthenticPartsFragment();
                return stonesAuthenticPartsFragment;

            case 13:
                CommentsFragment commentsFragment = new CommentsFragment();
                return commentsFragment;

            case 14:
                FileUploadFragment fileUploadFragment = new FileUploadFragment();
                return fileUploadFragment;

            case 15:
//                CheckOutFragment checkOutFragment = new CheckOutFragment();
//                return checkOutFragment;
                CardDetailsFragment cardDetailsFragment = new CardDetailsFragment();
                return cardDetailsFragment;

            default:
                return new HomeFragment();
        }
    }

    public void setIndex(int navItemIndex, String currentTag) {
        this.navItemIndex = navItemIndex;
        CURRENT_TAG = currentTag;
    }

    public void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    public void setFragmentTitle(int navItemIndex) {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_home:
                        shouldLoadHomeFragOnBackPress = true;
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                        break;
                    case R.id.nav_certificates:
                        shouldLoadHomeFragOnBackPress = true;
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_CERTIFICATES;
                        break;
                    case R.id.nav_support:
                        shouldLoadHomeFragOnBackPress = true;
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_SUPPORT;
                        break;

                    default:
                        navItemIndex = 0;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (inputMethodManager != null) {
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);

                try {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (inputMethodManager != null) {
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };


        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home

            if (MyConstants.paymentSuccess) {
                MyConstants.paymentSuccess = false;
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();
                clearBackStack();
                return;
            }
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
                return;
            }
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();
                return;
            }

            super.onBackPressed();

        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();

            } else {

                if (navItemIndex != 0) {
                    navItemIndex = 0;
                    CURRENT_TAG = TAG_HOME;
                    loadHomeFragment();
                    return;
                }

                super.onBackPressed();
            }
        }


    }

   /* @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case logout:
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {
                                editor = sharedpreferences.edit();
                                editor.clear();
                                editor.commit();
                                startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                                ActivityCompat.finishAffinity(HomeActivity.this);

                            } else {
                                Utils.showToast(mainObj.getString("message"), HomeActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case getAllData:
                        try {
                            List<HashMap<String, String>> itemTypeList, shapeList, clarityList, colorList, windList, materialTypeList, materialJewelleryTypeList, materialWatchTypeList, gemMaterialWatchTypeList, jewelleryTypeList;
                            itemTypeList = new ArrayList<>();
                            shapeList = new ArrayList<>();
                            clarityList = new ArrayList<>();
                            colorList = new ArrayList<>();
                            windList = new ArrayList<>();
                            materialTypeList = new ArrayList<>();
                            materialJewelleryTypeList = new ArrayList<>();
                            gemMaterialWatchTypeList = new ArrayList<>();
                            materialWatchTypeList = new ArrayList<>();
                            jewelleryTypeList = new ArrayList<>();

                            JSONObject mainObj = new JSONObject(result);
                            Log.d(TAG, "onTaskCompleted: "+mainObj.toString());
                            if (mainObj.getBoolean("status")) {
                                JSONObject resultObj = mainObj.getJSONObject("data");
                                // ItemType
                                Log.d(TAG, "onTaskCompleted: " + resultObj.toString());
                                JSONArray ItemType = resultObj.getJSONArray("ItemType");
                                for (int i = 0; i < ItemType.length(); i++) {
                                    JSONObject object = ItemType.getJSONObject(i);

                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("ItemTypeID", object.getString("ItemTypeID"));
                                    map.put("ItemTypeName", object.getString("ItemTypeName"));
                                    map.put("AppraisalCost", object.getString("AppraisalCost"));

                                    itemTypeList.add(map);
                                }

                                // shape
                                JSONArray Shape = resultObj.getJSONArray("Shape");
                                for (int i = 0; i < Shape.length(); i++) {
                                    JSONObject object = Shape.getJSONObject(i);

                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("ShapeID", object.getString("ShapeID"));
                                    map.put("ShapeName", object.getString("ShapeName"));
                                    map.put("ShapeImageName", object.getString("ShapeImageName"));
                                    map.put("FirstImage", object.getString("FirstImage"));
                                    map.put("SecondImage", object.getString("SecondImage"));
                                    map.put("ShapeImagePath", object.getString("ShapeImagePath"));

                                    shapeList.add(map);
                                }

                                // clarity
                                JSONArray Clarity = resultObj.getJSONArray("Clarity");
                                for (int i = 0; i < Clarity.length(); i++) {
                                    JSONObject object = Clarity.getJSONObject(i);

                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("ClarityID", object.getString("ClarityID"));
                                    map.put("ClarityName", object.getString("ClarityName"));


                                    clarityList.add(map);
                                }

                                // color
                                JSONArray Color = resultObj.getJSONArray("Color");
                                for (int i = 0; i < Color.length(); i++) {
                                    JSONObject object = Color.getJSONObject(i);
                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("ColorID", object.getString("ColorID"));
                                    map.put("ColorName", object.getString("ColorName"));
                                    colorList.add(map);
                                }

                                // wind
                                JSONArray Wind = resultObj.getJSONArray("Wind");
                                for (int i = 0; i < Wind.length(); i++) {
                                    JSONObject object = Wind.getJSONObject(i);

                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("WindTypeID", object.getString("WindTypeID"));
                                    map.put("WindType", object.getString("WindType"));

                                    windList.add(map);
                                }

                                // material type
                                JSONArray MaterialType = resultObj.getJSONArray("MaterialType");
                                for (int i = 0; i < MaterialType.length(); i++) {
                                    JSONObject object = MaterialType.getJSONObject(i);

                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("MaterialTypeID", object.getString("MaterialTypeID"));
                                    map.put("MaterialType", object.getString("MaterialType"));

                                    materialTypeList.add(map);
                                }

                                // material jewellery type
                                JSONArray MaterialJewelaryType = resultObj.getJSONArray("MaterialJewelaryType");
                                for (int i = 0; i < MaterialJewelaryType.length(); i++) {
                                    JSONObject object = MaterialJewelaryType.getJSONObject(i);

                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("MaterialTypeID", object.getString("MaterialTypeID"));
                                    map.put("MaterialType", object.getString("MaterialType"));

                                    materialJewelleryTypeList.add(map);
                                }

                                // material watch type
                                JSONArray MaterialWatchType = resultObj.getJSONArray("MaterialWatchType");
                                for (int i = 0; i < MaterialWatchType.length(); i++) {
                                    JSONObject object = MaterialWatchType.getJSONObject(i);

                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("MaterialTypeID", object.getString("MaterialTypeID"));
                                    map.put("MaterialType", object.getString("MaterialType"));

                                    materialWatchTypeList.add(map);
                                }

                                // jewellery type
                                JSONArray JewelaryType = resultObj.getJSONArray("JewelaryType");
                                for (int i = 0; i < JewelaryType.length(); i++) {
                                    JSONObject object = JewelaryType.getJSONObject(i);

                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("JewelarystyleTypeID", object.getString("JewelarystyleTypeID"));
                                    map.put("JewelarystyleType", object.getString("JewelarystyleType"));

                                    jewelleryTypeList.add(map);
                                }
                                // GemMaterialWatchType type
                                JSONArray gemMaterial = resultObj.getJSONArray("GemMaterialWatchType");
                                Log.d(TAG, "GemMaterialWatchType: "+gemMaterial.toString());
                                for (int i = 0; i < gemMaterial .length(); i++) {
                                    JSONObject object = gemMaterial .getJSONObject(i);

                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("MaterialTypeID", object.getString("MaterialTypeID"));
                                    map.put("MaterialType", object.getString("MaterialType"));
                                    gemMaterialWatchTypeList.add(map);
                                }

                                MyConstants.allMaterial = new AllMaterial();
                                MyConstants.allMaterial.setItemType(itemTypeList);
                                MyConstants.allMaterial.setShape(shapeList);
                                MyConstants.allMaterial.setClarity(clarityList);
                                MyConstants.allMaterial.setColor(colorList);
                                MyConstants.allMaterial.setWind(windList);
                                MyConstants.allMaterial.setMaterialType(materialTypeList);
                                MyConstants.allMaterial.setMaterialJewelaryType(materialJewelleryTypeList);
                                MyConstants.allMaterial.setGemMaterialWatchType(gemMaterialWatchTypeList);
                                MyConstants.allMaterial.setMaterialWatchType(materialWatchTypeList);
                                MyConstants.allMaterial.setJewelaryType(jewelleryTypeList);

                            } else {
                                Utils.showToast(mainObj.getString("message"), HomeActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    public void clearBackStack() {

        FragmentManager.BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(
                0);
        getSupportFragmentManager().popBackStack(entry.getId(),
                FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getSupportFragmentManager().executePendingTransactions();

    }
}
