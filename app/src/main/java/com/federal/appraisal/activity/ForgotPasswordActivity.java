package com.federal.appraisal.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.federal.appraisal.BuildConfig;
import com.federal.appraisal.R;
import com.federal.appraisal.animation.MyBounceInterpolator;
import com.federal.appraisal.others.Internet;
import com.federal.appraisal.ws.AsyncTaskListner;
import com.federal.appraisal.ws.CallRequest;
import com.federal.appraisal.ws.Constant;
import com.federal.appraisal.ws.MyConstants;
import com.federal.appraisal.ws.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPasswordActivity extends AppCompatActivity implements AsyncTaskListner{

    EditText etEmail;
    Button btnResetPassword;
    TextView tvBackToLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        etEmail = findViewById(R.id.etEmail);
        tvBackToLogin = findViewById(R.id.tvBackToLogin);
        btnResetPassword = findViewById(R.id.btnResetPassword);

        tvBackToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnResetPassword.setEnabled(true);
        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = etEmail.getText().toString().trim();

                if (!Utils.isValidEmail(email)) {
                    etEmail.requestFocus();
                    etEmail.setError("Email is not valid");
                } else {
                    btnResetPassword.setClickable(false);

                    final Animation myAnim = AnimationUtils.loadAnimation(ForgotPasswordActivity.this, R.anim.bounce);

                    // Use bounce interpolator with amplitude 0.2 and frequency 20
                    MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                    myAnim.setInterpolator(interpolator);

                    btnResetPassword.startAnimation(myAnim);

                    resetPassword(email);
                }
            }
        });
    }

    private void resetPassword(String email) {
        if (!Internet.isAvailable(ForgotPasswordActivity.this)) {
            Internet.showAlertDialog(ForgotPasswordActivity.this, "Error!", "Required Internet Connection", false);
            btnResetPassword.setClickable(true);
        } else {

            Map<String, String> map = new HashMap<String, String>();
            map.put("url", BuildConfig.BASE_URL + "forgot_password");
            map.put("email", email);
            map.put("device_id", MyConstants.DEVICE_ID);
            new CallRequest(ForgotPasswordActivity.this).forgotPassword(map);
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.removeSimpleSpinProgressDialog();
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case forgotPassword:
                        btnResetPassword.setClickable(true);
                        try {
                            JSONObject mainObj = new JSONObject(result);
                            if (mainObj.getBoolean("status")) {
                                Utils.showToast(mainObj.getString("message"), ForgotPasswordActivity.this);
                                onBackPressed();
                            } else {
                                Utils.showToast(mainObj.getString("message"), ForgotPasswordActivity.this);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progres) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}
